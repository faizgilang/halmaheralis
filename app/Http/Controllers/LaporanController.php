<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\RegisterExport;
use App\Exports\SPT3Export;
use App\Exports\ReagenExport;

class LaporanController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('laporan.laporan');
    }

    public function laporan(Request $request)
    {
        if ($request->laporan === '1') {
            return Excel::download(new RegisterExport($request->from, $request->to), 'Register.xlsx');
        } elseif ($request->laporan === '2') {
            return Excel::download(new SPT3Export($request->from, $request->to), 'SPT3.xlsx');
        } elseif ($request->laporan === '3') {
            return Excel::download(new ReagenExport($request->from, $request->to), 'Reagen.xlsx');
        } else {
            return back();
        }
    }
}
