<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pasien;
use App\Pemeriksaan;
use App\Http\Requests\PasienAdd;

class PasienController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function search()
    {
        $data['pasien'] = Pasien::all();

        return view('pemeriksaan.find', $data);
    }

    public function find(Request $request)
    {
        $check = Pasien::where('no_cm', $request->no_cm)->first();
        $data['pasien'] = $check;
        $data['no_cm'] = $request->no_cm;

        if (!$check) {
            return view('pemeriksaan.new_add', $data);
        } else {
            return view('pemeriksaan.old_add', $data);
        }
    }

    public function index()
    {
        $data['pasien'] = Pasien::all();

        return view('pasien.index', $data);
    }

    public function history($id)
    {
        $data['pemeriksaan'] = Pemeriksaan::with('pasien')->where('pasien_id', $id)->get();

        return view('pasien.history', $data);
    }

    public function create()
    {
        return view('pasien.add');
    }

    public function save(PasienAdd $request)
    {
        $pasien = new Pasien();
        $pasien->no_cm = $request->no_cm;
        $pasien->nik = $request->nik;
        $pasien->nama = $request->nama;
        $pasien->is_new = $request->status;
        $pasien->jk = $request->jk;
        $pasien->alamat = $request->alamat;
        $pasien->tempat_lahir = $request->tempat;
        $pasien->tanggal_lahir = $request->tanggal;
        $pasien->goldar = $request->golongan;
        $pasien->bpjs = $request->bpjs;
        $pasien->save();

        return redirect('/kelola_pasien');
    }

    public function edit($id)
    {
        $data['pasien'] = Pasien::find($id);

        return view('pasien.edit', $data);
    }

    public function update($id, Request $request)
    {
        $pasien = Pasien::find($id);
        $pasien->no_cm = $request->no_cm;
        $pasien->nama = $request->nama;
        $pasien->is_new = $request->status;
        $pasien->nik = $request->nik;
        $pasien->jk = $request->jk;
        $pasien->alamat = $request->alamat;
        $pasien->tempat_lahir = $request->tempat;
        $pasien->tanggal_lahir = $request->tanggal;
        $pasien->goldar = $request->golongan;
        $pasien->bpjs = $request->bpjs;
        $pasien->save();

        return redirect('/kelola_pasien');
    }
}
