<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pemeriksaan;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $date = date('Y-m-d');
        $month = date('m');
        $year = date('Y');
        $data['day'] = Pemeriksaan::whereDate('tanggal',$date)->count();
        $data['month'] = Pemeriksaan::whereMonth('tanggal',$month)->count();
        $data['year'] = Pemeriksaan::whereYear('tanggal',$year)->count();
        return view('home',$data);
    }
}
