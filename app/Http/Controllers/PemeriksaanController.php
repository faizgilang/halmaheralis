<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pasien;
use App\Pemeriksaan;
use App\Http\Requests\PasienAdd;
use DB;

class PemeriksaanController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $data['pemeriksaan'] = Pemeriksaan::with('pasien')->orderBy('tanggal', 'desc')->get();

        return view('pemeriksaan.index', $data);
    }

    public function save(Request $request)
    {
        $checkPasien = Pasien::where('no_cm', $request->no_cm)->first();
        $check = DB::table('pemeriksaans')->where('pasien_id', $checkPasien)->count();
        if ($checkPasien->is_new == '0') {
            $check = 2;
        }
        $pemeriksaan = new Pemeriksaan();
        $pemeriksaan->is_first = $check > 0 ? '0' : '1';
        $pemeriksaan->tanggal = isset($request->date) ? $request->date : date('Y-m-d');
        $pemeriksaan->pasien_id = $checkPasien->id;
        $pemeriksaan->is_hamil = isset($request->is_hamil) ? '1' : '0';
        $pemeriksaan->goldar = $request->golongan;
        $pemeriksaan->debitur = $request->debitur;
        $pemeriksaan->hb = $request->hb;
        $pemeriksaan->hmt = $request->hmt;
        $pemeriksaan->leukosit = $request->leukosit;
        $pemeriksaan->plt = $request->plt;
        $pemeriksaan->widal_o = $request->widal_o;
        $pemeriksaan->widal_h = $request->widal_h;
        $pemeriksaan->led = $request->led;
        $pemeriksaan->diff = $request->diff;
        $pemeriksaan->hbsag = $request->hbsag;
        $pemeriksaan->gula = $request->gula;
        $pemeriksaan->chol = $request->chol;
        $pemeriksaan->trig = $request->trig;
        $pemeriksaan->ua = $request->ua;
        $pemeriksaan->hamil_protein = $request->hamil_protein;
        $pemeriksaan->hamil_reduksi = $request->hamil_reduksi;
        $pemeriksaan->bilirubin = $request->bilirubin;
        $pemeriksaan->gravindek = $request->gravindek;
        $pemeriksaan->urinalisa_protein = $request->urinalisa_protein;
        $pemeriksaan->urinalisa_sedimen = $request->urinalisa_sedimen;
        $pemeriksaan->hiv_1 = $request->hiv_1;
        $pemeriksaan->hiv_2 = $request->hiv_2;
        $pemeriksaan->hiv_3 = $request->hiv_3;
        $pemeriksaan->rpr = $request->rpr;
        $pemeriksaan->ims = $request->ims;
        $pemeriksaan->mikro_pengecatan = $request->mikro_pengecatan;
        $array = array(
            'pmn' => $request->pmn,
            'diplo' => $request->diplo,
            'candi' => $request->candi,
            'tric' => $request->tric,
        );

        $pemeriksaan->mikro_hasil = json_encode($array);
        $pemeriksaan->harga = $request->harga;
        $pemeriksaan->waktu = $request->waktu;
        $pemeriksaan->hb_stick = isset($request->hb_stick) ? '1' : '0';
        $pemeriksaan->gula_stick = isset($request->gula_stick) ? '1' : '0';
        $pemeriksaan->chol_stick = isset($request->chol_stick) ? '1' : '0';
        $pemeriksaan->ua_stick = isset($request->ua_stick) ? '1' : '0';
        $pemeriksaan->hiv_1_duplex = isset($request->hiv_1_duplex) ? '1' : '0';
        $pemeriksaan->hiv_2_duplex = isset($request->hiv_2_duplex) ? '1' : '0';
        $pemeriksaan->hiv_3_duplex = isset($request->hiv_3_duplex) ? '1' : '0';
        $pemeriksaan->save();

        return redirect('/kelola_pemeriksaan');
    }

    public function new_save(PasienAdd $request)
    {
        $pasien = new Pasien();
        $pasien->no_cm = $request->no_cm;
        $pasien->nik = $request->nik;
        $pasien->nama = $request->nama;
        $pasien->is_new = $request->status;
        $pasien->jk = $request->jk;
        $pasien->alamat = $request->alamat;
        $pasien->tempat_lahir = $request->tempat;
        $pasien->tanggal_lahir = $request->tanggal;
        $pasien->goldar = $request->golongan;
        $pasien->bpjs = $request->bpjs;
        $pasien->save();

        $pemeriksaan = new Pemeriksaan();
        $pemeriksaan->is_first = $request->status;
        $pemeriksaan->tanggal = isset($request->date) ? $request->date : date('Y-m-d');
        $pemeriksaan->pasien_id = $pasien->id;
        $pemeriksaan->is_hamil = isset($request->is_hamil) ? '1' : '0';
        $pemeriksaan->goldar = $request->golongan;
        $pemeriksaan->debitur = $request->debitur;
        $pemeriksaan->hb = $request->hb;
        $pemeriksaan->hmt = $request->hmt;
        $pemeriksaan->leukosit = $request->leukosit;
        $pemeriksaan->plt = $request->plt;
        $pemeriksaan->widal_o = $request->widal_o;
        $pemeriksaan->widal_h = $request->widal_h;
        $pemeriksaan->led = $request->led;
        $pemeriksaan->diff = $request->diff;
        $pemeriksaan->hbsag = $request->hbsag;
        $pemeriksaan->gula = $request->gula;
        $pemeriksaan->chol = $request->chol;
        $pemeriksaan->trig = $request->trig;
        $pemeriksaan->ua = $request->ua;
        $pemeriksaan->hamil_protein = $request->hamil_protein;
        $pemeriksaan->hamil_reduksi = $request->hamil_reduksi;
        $pemeriksaan->bilirubin = $request->bilirubin;
        $pemeriksaan->gravindek = $request->gravindek;
        $pemeriksaan->urinalisa_protein = $request->urinalisa_protein;
        $pemeriksaan->urinalisa_sedimen = $request->urinalisa_sedimen;
        $pemeriksaan->hiv_1 = $request->hiv_1;
        $pemeriksaan->hiv_2 = $request->hiv_2;
        $pemeriksaan->hiv_3 = $request->hiv_3;
        $pemeriksaan->rpr = $request->rpr;
        $pemeriksaan->ims = $request->ims;
        $pemeriksaan->mikro_pengecatan = $request->mikro_pengecatan;
        $array = array(
            'pmn' => $request->pmn,
            'diplo' => $request->diplo,
            'candi' => $request->candi,
            'tric' => $request->tric,
        );

        $pemeriksaan->mikro_hasil = json_encode($array);
        $pemeriksaan->harga = $request->harga;
        $pemeriksaan->waktu = $request->waktu;
        $pemeriksaan->hb_stick = isset($request->hb_stick) ? '1' : '0';
        $pemeriksaan->gula_stick = isset($request->gula_stick) ? '1' : '0';
        $pemeriksaan->chol_stick = isset($request->chol_stick) ? '1' : '0';
        $pemeriksaan->ua_stick = isset($request->ua_stick) ? '1' : '0';
        $pemeriksaan->hbsag_duplex = isset($request->hbsag_duplex) ? '1' : '0';
        $pemeriksaan->ims_duplex = isset($request->ims_duplex) ? '1' : '0';
        $pemeriksaan->hiv_1_duplex = isset($request->hiv_1_duplex) ? '1' : '0';
        $pemeriksaan->hiv_2_duplex = isset($request->hiv_2_duplex) ? '1' : '0';
        $pemeriksaan->hiv_3_duplex = isset($request->hiv_3_duplex) ? '1' : '0';
        $pemeriksaan->save();

        return redirect('/kelola_pemeriksaan');
    }

    public function create()
    {
        $data['pasien'] = Pasien::all();

        return view('pemeriksaan.add', $data);
    }

    public function delete($id)
    {
        $pemeriksaan = Pemeriksaan::find($id);
        $pemeriksaan->delete();

        return redirect('/kelola_pemeriksaan');
    }

    public function new_create()
    {
        $data['pasien'] = Pasien::all();

        return view('pemeriksaan.new_add', $data);
    }

    public function edit($id)
    {
        $data['pasien'] = Pasien::all();
        $data['pemeriksaan'] = Pemeriksaan::find($id);

        return view('pemeriksaan.edit', $data);
    }

    public function update(Request $request, $id)
    {
        $pemeriksaan = Pemeriksaan::find($id);
        $pemeriksaan->tanggal = isset($request->date) ? $request->date : date('Y-m-d');
        $pemeriksaan->is_hamil = isset($request->is_hamil) ? '1' : '0';
        $pemeriksaan->goldar = $request->golongan;
        $pemeriksaan->debitur = $request->debitur;
        $pemeriksaan->hb = $request->hb;
        $pemeriksaan->hmt = $request->hmt;
        $pemeriksaan->leukosit = $request->leukosit;
        $pemeriksaan->plt = $request->plt;
        $pemeriksaan->widal_o = $request->widal_o;
        $pemeriksaan->widal_h = $request->widal_h;
        $pemeriksaan->led = $request->led;
        $pemeriksaan->diff = $request->diff;
        $pemeriksaan->hbsag = $request->hbsag;
        $pemeriksaan->gula = $request->gula;
        $pemeriksaan->chol = $request->chol;
        $pemeriksaan->trig = $request->trig;
        $pemeriksaan->ua = $request->ua;
        $pemeriksaan->hamil_protein = $request->hamil_protein;
        $pemeriksaan->hamil_reduksi = $request->hamil_reduksi;
        $pemeriksaan->bilirubin = $request->bilirubin;
        $pemeriksaan->gravindek = $request->gravindek;
        $pemeriksaan->urinalisa_protein = $request->urinalisa_protein;
        $pemeriksaan->urinalisa_sedimen = $request->urinalisa_sedimen;
        $pemeriksaan->hiv_1 = $request->hiv_1;
        $pemeriksaan->hiv_2 = $request->hiv_2;
        $pemeriksaan->hiv_3 = $request->hiv_3;
        $pemeriksaan->rpr = $request->rpr;
        $pemeriksaan->ims = $request->ims;
        $pemeriksaan->mikro_pengecatan = $request->mikro_pengecatan;
        $array = array(
            'pmn' => $request->pmn,
            'diplo' => $request->diplo,
            'candi' => $request->candi,
            'tric' => $request->tric,
        );

        $pemeriksaan->mikro_hasil = json_encode($array);
        $pemeriksaan->harga = $request->harga;
        $pemeriksaan->waktu = $request->waktu;
        $pemeriksaan->hb_stick = isset($request->hb_stick) ? '1' : '0';
        $pemeriksaan->gula_stick = isset($request->gula_stick) ? '1' : '0';
        $pemeriksaan->chol_stick = isset($request->chol_stick) ? '1' : '0';
        $pemeriksaan->ua_stick = isset($request->ua_stick) ? '1' : '0';
        $pemeriksaan->hiv_1_duplex = isset($request->hiv_1_duplex) ? '1' : '0';
        $pemeriksaan->hiv_2_duplex = isset($request->hiv_2_duplex) ? '1' : '0';
        $pemeriksaan->hiv_3_duplex = isset($request->hiv_3_duplex) ? '1' : '0';

        $pemeriksaan->save();

        return redirect('/kelola_pemeriksaan');
    }
    public function detail(Request $request)
    {
        $pemeriksaan = Pemeriksaan::find($request->id);
        $data['detail'] = $pemeriksaan;
        echo json_encode($data, TRUE);
    }
}
