<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserAdd extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'role' => 'required',
            'password' => 'min:6',
        ];
    }
    public function messages()
    {
        return[
            'name.required' => 'Nama tidak boleh kosong',
            'email.unique' => 'Email harus unik',
            'email.required' => 'Nama tidak boleh kosong',
            'role.required' => 'Role tidak boleh kosong',
            'password.min' => 'Password harus lebih dari 6 karakter',
    ];
    }
}
