<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pasien extends Model
{
    //
    public function pemeriksaan(){
        return $this->hasMany('App\Pemeriksaan');
    }
}
