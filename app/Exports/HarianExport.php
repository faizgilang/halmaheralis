<?php

namespace App\Exports;

use App\Pemeriksaan;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithTitle;

class HarianExport implements FromView, ShouldAutoSize, WithEvents, WithTitle
{
    protected $from;
    protected $to;

    public function __construct($from, $to)
    {
        $this->from = $from;
        $this->to = $to;
    }

    public function title(): string
    {
        return 'Lembar Pencatatan Harian';
    }

    public function registerEvents(): array
    {
        return [
        AfterSheet::class => function (AfterSheet $event) {
            // All headers - set font size to 14
            $cellRange = 'A5:AJ6';
            $range = Pemeriksaan::with('pasien')
            ->whereDate('tanggal', '>=', $this->from)
            ->whereDate('tanggal', '<=', $this->to)
            ->count();
            $faiz = $range + 6;
            // Apply array of styles to B2:G8 cell range
            $styleHeader = [
                'alignment' => [
                    'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                    'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                ],
                'borders' => [
                    'allBorders' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
                        'color' => ['argb' => '00000000'],
                    ],
                ],
            ];
            $styleBody = [
                'borders' => [
                    'allBorders' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
                        'color' => ['argb' => '00000000'],
                    ],
                ],
            ];
            $event->sheet->getDelegate()->setCellValue('B1', 'Puskesmas Halmahera');
            $event->sheet->getDelegate()->setCellValue('B2', date('M', strtotime($this->to)));
            $event->sheet->getDelegate()->setCellValue('B3', date('Y', strtotime($this->to)));
            $event->sheet->getDelegate()->getStyle('B1:B3')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
            //header
            $event->sheet->getDelegate()->getStyle($cellRange)->applyFromArray($styleHeader);
            //body
            $event->sheet->getDelegate()->getStyle('A6:AJ'.$faiz)->applyFromArray($styleBody);

        // Set first row to height 20
        },
    ];
    }

    public function view(): View
    {
        return view('laporan.harian', ['pemeriksaan' => Pemeriksaan::with('pasien')
        ->whereDate('tanggal', '>=', $this->from)
        ->whereDate('tanggal', '<=', $this->to)
        ->orderBy('tanggal')
        ->get(), ]);
    }
}
