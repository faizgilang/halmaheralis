<?php

namespace App\Exports;

use App\Pemeriksaan;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;

class SPT3Export implements FromView, ShouldAutoSize, WithEvents, WithTitle
{
    protected $from;
    protected $to;

    public function __construct($from, $to)
    {
        $this->from = $from;
        $this->to = $to;
    }

    public function title(): string
    {
        return 'Laporan SP3';
    }

    public function registerEvents(): array
    {
        return [
        AfterSheet::class => function (AfterSheet $event) {
            $styleBody = [
                'borders' => [
                    'allBorders' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
                        'color' => ['argb' => '00000000'],
                    ],
                ],
            ];
            $hiv_l = 0;
            $hiv_p = 0;
            $hiv_l_pos = 0;
            $hiv_p_pos = 0;
            $hiv_hamil = 0;
            $hiv_hamil_pos = 0;
            $ims_l = 0;
            $ims_p = 0;
            $gram_l_pos = 0;
            $gram_p_pos = 0;
            $ims_l_pos = 0;
            $ims_p_pos = 0;
            $ims_hamil = 0;
            $ims_hamil_pos = 0;
            $gram = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
            ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
            ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
            ->where('pemeriksaans.mikro_pengecatan', '=', 'gram')
            ->get();
            foreach ($gram as $hiv) {
                if ($hiv->jk == '1' && json_decode($hiv->mikro_hasil)->pmn == '1') {
                    ++$gram_l_pos;
                } else {
                    ++$gram_p_pos;
                }
            }
            $hiv_1 = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
            ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
            ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
            ->whereNotNull('pemeriksaans.hiv_1')
            ->get();
            foreach ($hiv_1 as $hiv) {
                if ($hiv->jk == '1') {
                    ++$hiv_l;
                } else {
                    ++$hiv_p;
                    if ($hiv->is_hamil == '1') {
                        ++$hiv_hamil;
                    }
                }
            }
            $hiv_2 = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
            ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
            ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
            ->whereNotNull('pemeriksaans.hiv_3')
            ->get();
            foreach ($hiv_2 as $hiv) {
                if ($hiv->jk == '1') {
                    if ($hiv->hiv_1 == 'reaktif') {
                        ++$hiv_l_pos;
                    }
                } else {
                    if ($hiv->hiv_1 == 'reaktif') {
                        ++$hiv_p_pos;
                        if ($hiv->is_hamil == '1') {
                            ++$hiv_hamil_pos;
                        }
                    }
                }
            }
            $ims = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
            ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
            ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
            ->whereNotNull('pemeriksaans.ims')
            ->get();
            foreach ($ims as $hiv) {
                if ($hiv->jk == '1') {
                    ++$ims_l;
                    if ($hiv->ims == '1') {
                        ++$ims_l_pos;
                    }
                } else {
                    ++$ims_p;
                    if ($hiv->ims == '1') {
                        ++$ims_p_pos;
                        if ($hiv->is_hamil == '1') {
                            ++$ims_hamil_pos;
                        }
                    }
                    if ($hiv->is_hamil == '1') {
                        ++$ims_hamil;
                    }
                }
            }
            $event->sheet->getDelegate()->setCellValue('A3', 'F');
            $event->sheet->getDelegate()->mergeCells('A17:E17')->setCellValue('A17', 'Kode Puskesmas : P05');
            $event->sheet->getDelegate()->mergeCells('A18:E18')->setCellValue('A18', 'Nama Puskesmas : Halmahera');
            $event->sheet->getDelegate()->setCellValue('G17', 'Bulan');
            $event->sheet->getDelegate()->setCellValue('G18', 'Tahun');
            $event->sheet->getDelegate()->mergeCells('A16:E16')->setCellValue('A16', 'LAPORAN BULANAN UKM ESENSIAL 5 PENYAKIT MENULAR')->getStyle('A16')->getFont()->setBold(true);
            $event->sheet->getDelegate()->mergeCells('A20:B20')->setCellValue('A20', 'V-AIDS')->getStyle('A20')->getFont()->setBold(true);
            $event->sheet->getDelegate()->setCellValue('D20', 'L');
            $event->sheet->getDelegate()->setCellValue('E20', 'P');
            $event->sheet->getDelegate()->setCellValue('F20', 'JML');
            $event->sheet->getDelegate()->mergeCells('A22:B22')->setCellValue('A22', '1.Jumlah Orang dites HIV');
            $event->sheet->getDelegate()->setCellValue('D22', $hiv_l);
            $event->sheet->getDelegate()->setCellValue('E22', $hiv_p);
            $event->sheet->getDelegate()->setCellValue('F22', $hiv_p + $hiv_l);
            $event->sheet->getDelegate()->mergeCells('A23:B23')->setCellValue('A23', '2.Jumlah Orang dengan HIV Positif');
            $event->sheet->getDelegate()->setCellValue('D23', $hiv_l_pos);
            $event->sheet->getDelegate()->setCellValue('E23', $hiv_p_pos);
            $event->sheet->getDelegate()->setCellValue('F23', $hiv_p_pos + $hiv_l_pos);
            $event->sheet->getDelegate()->setCellValue('D24', 'JML');
            $event->sheet->getDelegate()->mergeCells('A25:B25')->setCellValue('A25', '3.Jumlah Ibu Hamil dites HIV');
            $event->sheet->getDelegate()->setCellValue('D25', $hiv_hamil);
            $event->sheet->getDelegate()->mergeCells('A26:B26')->setCellValue('A26', '4.Jumlah Ibu Hamil dengan HIV Positif');
            $event->sheet->getDelegate()->setCellValue('D26', $hiv_hamil_pos);

            $event->sheet->getDelegate()->mergeCells('A28:B28')->setCellValue('A28', 'Penyakit Kelamin')->getStyle('A28')->getFont()->setBold(true);
            $event->sheet->getDelegate()->setCellValue('D28', 'L');
            $event->sheet->getDelegate()->setCellValue('E28', 'P');
            $event->sheet->getDelegate()->setCellValue('F28', 'JML');
            $event->sheet->getDelegate()->mergeCells('A30:B30')->setCellValue('A30', '1.Jumlah Orang dengan Sifilis');
            $event->sheet->getDelegate()->setCellValue('D30', $ims_l_pos);
            $event->sheet->getDelegate()->setCellValue('E30', $ims_p_pos);
            $event->sheet->getDelegate()->setCellValue('F30', $ims_p_pos + $ims_l_pos);
            $event->sheet->getDelegate()->mergeCells('A31:B31')->setCellValue('A31', '2.Jumlah Orang dengan gram Positif');
            $event->sheet->getDelegate()->setCellValue('D31', $gram_l_pos);
            $event->sheet->getDelegate()->setCellValue('E31', $gram_p_pos);
            $event->sheet->getDelegate()->setCellValue('F31', $gram_p_pos + $gram_l_pos);
            $event->sheet->getDelegate()->setCellValue('D32', 'JML');
            $event->sheet->getDelegate()->mergeCells('A33:B33')->setCellValue('A33', '3.Jumlah Ibu Hamil dites Sifilis');
            $event->sheet->getDelegate()->setCellValue('D33', $ims_hamil);
            $event->sheet->getDelegate()->mergeCells('A34:B34')->setCellValue('A34', '4.Jumlah Ibu Hamil dengan Sifilis Positif');
            $event->sheet->getDelegate()->setCellValue('D34', $ims_hamil_pos);

            $event->sheet->getDelegate()->mergeCells('H17:I17')->setCellValue('H17', date('M', strtotime($this->to)));
            $event->sheet->getDelegate()->mergeCells('H18:I18')->setCellValue('H18', date('Y', strtotime($this->to)));

            $event->sheet->getDelegate()->mergeCells('A1:B1')->setCellValue('A1', date('M', strtotime($this->to)))->getStyle('A1')->getFont()->setBold(true);
            $event->sheet->getDelegate()->getStyle('A3:I4')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
            $event->sheet->getDelegate()->getStyle('B3:I4')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
            $event->sheet->getDelegate()->getStyle('A3')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_TOP);
            $event->sheet->getDelegate()->getStyle('A3:I14')->applyFromArray($styleBody);
            $event->sheet->getDelegate()->getStyle('H18')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
            $event->sheet->getDelegate()->getStyle('D22:F23')->applyFromArray($styleBody);
            $event->sheet->getDelegate()->getStyle('D25:D26')->applyFromArray($styleBody);
            $event->sheet->getDelegate()->getStyle('D30:F31')->applyFromArray($styleBody);
            $event->sheet->getDelegate()->getStyle('D33:D34')->applyFromArray($styleBody);
        },
];
    }

    public function view(): View
    {
        $baru_l1 = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '1')
        ->where('pasiens.jk', '=', '1')
        ->count();
        $baru_p11 = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_hamil', '=', '1')
        ->where('pemeriksaans.is_first', '=', '1')
        ->where('pasiens.jk', '=', '0')
        ->count();
        $baru_p21 = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_hamil', '=', '0')
        ->where('pemeriksaans.is_first', '=', '1')
        ->where('pasiens.jk', '=', '0')
        ->count();
        $lama_l1 = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '0')
        ->where('pasiens.jk', '=', '1')
        ->count();
        $lama_p11 = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_hamil', '=', '1')
        ->where('pemeriksaans.is_first', '=', '0')
        ->where('pasiens.jk', '=', '0')
        ->count();
        $lama_p21 = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_hamil', '=', '0')
        ->where('pemeriksaans.is_first', '=', '0')
        ->where('pasiens.jk', '=', '0')
        ->count();

        $baru_p12 = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_hamil', '=', '1')
        ->where('pemeriksaans.is_first', '=', '1')
        ->whereNotNull('pemeriksaans.hb')
        ->where('pasiens.jk', '=', '0')
        ->where(function ($a) {
            $a->orWhereNull('pemeriksaans.hmt')
            ->orWhereNull('pemeriksaans.leukosit')
            ->orWhereNull('pemeriksaans.plt');
        })
        ->count();
        $lama_p12 = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_hamil', '=', '1')
        ->where('pemeriksaans.is_first', '=', '0')
        ->whereNotNull('pemeriksaans.hb')
        ->where('pasiens.jk', '=', '0')
        ->where(function ($b) {
            $b->orWhereNull('pemeriksaans.hmt')
            ->orWhereNull('pemeriksaans.leukosit')
            ->orWhereNull('pemeriksaans.plt');
        })
        ->count();
        $baru_p13 = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_hamil', '=', '1')
        ->where('pemeriksaans.is_first', '=', '1')
        ->whereNotNull('pemeriksaans.hamil_protein')
        ->where('pasiens.jk', '=', '0')
        ->count();

        $lama_p13 = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_hamil', '=', '1')
        ->where('pemeriksaans.is_first', '=', '0')
        ->whereNotNull('pemeriksaans.hamil_protein')
        ->where('pasiens.jk', '=', '0')
        ->count();

        $baru_p14 = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_hamil', '=', '1')
        ->where('pemeriksaans.is_first', '=', '1')
        ->where('pemeriksaans.hamil_protein', '=', '0')
        ->where('pasiens.jk', '=', '0')
        ->count();

        $lama_p14 = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_hamil', '=', '1')
        ->where('pemeriksaans.is_first', '=', '0')
        ->where('pemeriksaans.hamil_protein', '=', '0')
        ->where('pasiens.jk', '=', '0')
        ->count();
        $baru_p15 = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '1')
        ->whereNotNull('pemeriksaans.gravindek')
        ->where('pasiens.jk', '=', '0')
        ->count();

        $lama_p15 = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '0')
        ->whereNotNull('pemeriksaans.gravindek')
        ->where('pasiens.jk', '=', '0')
        ->count();
        $baru_p16 = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '1')
        ->where('pemeriksaans.gravindek', '=', '0')
        ->where('pasiens.jk', '=', '0')
        ->count();

        $lama_p16 = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '0')
        ->where('pemeriksaans.gravindek', '=', '0')
        ->where('pasiens.jk', '=', '0')
        ->count();

        $baru_l7 = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '1')
        ->where('pasiens.jk', '=', '1')
        ->whereNotNull('pemeriksaans.hb')
        ->WhereNotNull('pemeriksaans.hmt')
        ->WhereNotNull('pemeriksaans.leukosit')
        ->WhereNotNull('pemeriksaans.plt')
        ->count();
        $baru_p27 = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '1')
        ->where('pasiens.jk', '=', '0')
        ->whereNotNull('pemeriksaans.hb')
        ->WhereNotNull('pemeriksaans.hmt')
        ->WhereNotNull('pemeriksaans.leukosit')
        ->WhereNotNull('pemeriksaans.plt')
        ->count();
        $lama_l7 = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '0')
        ->where('pasiens.jk', '=', '1')
        ->whereNotNull('pemeriksaans.hb')
        ->WhereNotNull('pemeriksaans.hmt')
        ->WhereNotNull('pemeriksaans.leukosit')
        ->WhereNotNull('pemeriksaans.plt')
        ->count();
        $lama_p27 = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '0')
        ->where('pasiens.jk', '=', '0')
        ->whereNotNull('pemeriksaans.hb')
        ->WhereNotNull('pemeriksaans.hmt')
        ->WhereNotNull('pemeriksaans.leukosit')
        ->WhereNotNull('pemeriksaans.plt')
        ->count();

        $data[] = [
            'layanan' => 'Jumlah Kunjungan Laboratorium',
            'baru_l' => $baru_l1,
            'baru_p' => $baru_p11 + $baru_p21,
            'baru_jumlah' => $baru_l1 + $baru_p11 + $baru_p21,
            'lama_l' => $lama_l1,
            'lama_p' => $lama_p11 + $lama_p21,
            'lama_jumlah' => $lama_l1 + $lama_p11 + $lama_p21,
            'total' => $baru_l1 + $baru_p11 + $baru_p21 + $lama_l1 + $lama_p11 + $lama_p21,
        ];
        $data[] = [
            'layanan' => 'Jumlah  pemeriksaan Hb pada ibu hamil',
            'baru_l' => 0,
            'baru_p' => $baru_p12,
            'baru_jumlah' => $baru_p12,
            'lama_l' => 0,
            'lama_p' => $lama_p12,
            'lama_jumlah' => $lama_p12,
            'total' => $baru_p12 + $lama_p12,
        ];
        $data[] = [
            'layanan' => 'Jumlah pemeriksaan protein urin pada ibu hamil',
            'baru_l' => 0,
            'baru_p' => $baru_p13,
            'baru_jumlah' => $baru_p13,
            'lama_l' => 0,
            'lama_p' => $lama_p13,
            'lama_jumlah' => $lama_p13,
            'total' => $baru_p13 + $lama_p13,
        ];

        // $data[] = [
        //     'layanan' => 'Jumlah pemeriksaan protein urin pada ibu hamil',
        //     'baru_l' => '',
        //     'baru_p' => $baru_p14,
        //     'baru_jumlah' => $baru_p14,
        //     'lama_l' => '',
        //     'lama_p' => $lama_p14,
        //     'lama_jumlah' => $lama_p14,
        //     'total' => $lama_p14+$baru_p14,
        // ];
        $data[] = [
            'layanan' => 'Jumlah pemeriksaan test kehamilan',
            'baru_l' => 0,
            'baru_p' => $baru_p15,
            'baru_jumlah' => $baru_p15,
            'lama_l' => 0,
            'lama_p' => $lama_p15,
            'lama_jumlah' => $lama_p15,
            'total' => $lama_p15 + $baru_p15,
        ];
        // $data[] = [
        //     'layanan' => 'Jumlah pemeriksaan test kehamilan',
        //     'hasil' => 'Negatif',
        //     'baru_l' => '',
        //     'baru_p1' => $baru_p16,
        //     'baru_p2' => '',
        //     'baru' => '',
        //     'baru_jumlah' => $baru_p16,
        //     'lama_l' => '',
        //     'lama_p1' => $lama_p16,
        //     'lama_p2' => '',
        //     'lama' => '',
        //     'lama_jumlah' => $lama_p16,
        //     'total' => '',
        // ];
        $gula = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->whereNotNull('pemeriksaans.gula')
        ->get();
        $kolesterol = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->whereNotNull('pemeriksaans.chol')
        ->get();
        $trig = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->whereNotNull('pemeriksaans.trig')
        ->get();
        $asam = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->whereNotNull('pemeriksaans.ua')
        ->get();
        $baru_l8 = 0;
        $baru_p28 = 0;
        $lama_l8 = 0;
        $lama_p28 = 0;
        foreach ($gula as $klinik) {
            if ($klinik->is_first == '1') {
                if ($klinik->jk == '1') {
                    ++$baru_l8;
                } else {
                    ++$baru_p28;
                }
            } else {
                if ($klinik->jk == '1') {
                    ++$lama_l8;
                } else {
                    ++$lama_p28;
                }
            }
        }
        foreach ($kolesterol as $klinik) {
            if ($klinik->is_first == '1') {
                if ($klinik->jk == '1') {
                    ++$baru_l8;
                } else {
                    ++$baru_p28;
                }
            } else {
                if ($klinik->jk == '1') {
                    ++$lama_l8;
                } else {
                    ++$lama_p28;
                }
            }
        }
        foreach ($trig as $klinik) {
            if ($klinik->is_first == '1') {
                if ($klinik->jk == '1') {
                    ++$baru_l8;
                } else {
                    ++$baru_p28;
                }
            } else {
                if ($klinik->jk == '1') {
                    ++$lama_l8;
                } else {
                    ++$lama_p28;
                }
            }
        }
        foreach ($asam as $klinik) {
            if ($klinik->is_first == '1') {
                if ($klinik->jk == '1') {
                    ++$baru_l8;
                } else {
                    ++$baru_p28;
                }
            } else {
                if ($klinik->jk == '1') {
                    ++$lama_l8;
                } else {
                    ++$lama_p28;
                }
            }
        }

        $hbsag = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->whereNotNull('pemeriksaans.hbsag')
        ->get();
        $widal_o = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->whereNotNull('pemeriksaans.widal_o')
        ->get();
        $widal_h = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->whereNotNull('pemeriksaans.widal_h')
        ->get();
        $goldar = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->whereNotNull('pemeriksaans.goldar')
        ->get();
        $hiv_1 = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->whereNotNull('pemeriksaans.hiv_1')
        ->get();
        $hiv_2 = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->whereNotNull('pemeriksaans.hiv_2')
        ->get();
        $hiv_3 = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->whereNotNull('pemeriksaans.hiv_3')
        ->get();
        $ims = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->whereNotNull('pemeriksaans.ims')
        ->get();
        $rpr = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->whereNotNull('pemeriksaans.rpr')
        ->get();
        $baru_l10 = 0;
        $baru_p210 = 0;
        $lama_l10 = 0;
        $lama_p210 = 0;
        foreach ($hbsag as $klinik) {
            if ($klinik->is_first == '1') {
                if ($klinik->jk == '1') {
                    ++$baru_l10;
                } else {
                    ++$baru_p210;
                }
            } else {
                if ($klinik->jk == '1') {
                    ++$lama_l10;
                } else {
                    ++$lama_p210;
                }
            }
        }
        foreach ($widal_o as $klinik) {
            if ($klinik->is_first == '1') {
                if ($klinik->jk == '1') {
                    ++$baru_l10;
                } else {
                    ++$baru_p210;
                }
            } else {
                if ($klinik->jk == '1') {
                    ++$lama_l10;
                } else {
                    ++$lama_p210;
                }
            }
        }
        foreach ($widal_h as $klinik) {
            if ($klinik->is_first == '1') {
                if ($klinik->jk == '1') {
                    ++$baru_l10;
                } else {
                    ++$baru_p210;
                }
            } else {
                if ($klinik->jk == '1') {
                    ++$lama_l10;
                } else {
                    ++$lama_p210;
                }
            }
        }
        foreach ($goldar as $klinik) {
            if ($klinik->is_first == '1') {
                if ($klinik->jk == '1') {
                    ++$baru_l10;
                } else {
                    ++$baru_p210;
                }
            } else {
                if ($klinik->jk == '1') {
                    ++$lama_l10;
                } else {
                    ++$lama_p210;
                }
            }
        }
        foreach ($hiv_1 as $klinik) {
            if ($klinik->is_first == '1') {
                if ($klinik->jk == '1') {
                    ++$baru_l10;
                } else {
                    ++$baru_p210;
                }
            } else {
                if ($klinik->jk == '1') {
                    ++$lama_l10;
                } else {
                    ++$lama_p210;
                }
            }
        }
        foreach ($hiv_2 as $klinik) {
            if ($klinik->is_first == '1') {
                if ($klinik->jk == '1') {
                    ++$baru_l10;
                } else {
                    ++$baru_p210;
                }
            } else {
                if ($klinik->jk == '1') {
                    ++$lama_l10;
                } else {
                    ++$lama_p210;
                }
            }
        }
        foreach ($hiv_3 as $klinik) {
            if ($klinik->is_first == '1') {
                if ($klinik->jk == '1') {
                    ++$baru_l10;
                } else {
                    ++$baru_p210;
                }
            } else {
                if ($klinik->jk == '1') {
                    ++$lama_l10;
                } else {
                    ++$lama_p210;
                }
            }
        }
        foreach ($ims as $klinik) {
            if ($klinik->is_first == '1') {
                if ($klinik->jk == '1') {
                    ++$baru_l10;
                } else {
                    ++$baru_p210;
                }
            } else {
                if ($klinik->jk == '1') {
                    ++$lama_l10;
                } else {
                    ++$lama_p210;
                }
            }
        }
        foreach ($rpr as $klinik) {
            if ($klinik->is_first == '1') {
                if ($klinik->jk == '1') {
                    ++$baru_l10;
                } else {
                    ++$baru_p210;
                }
            } else {
                if ($klinik->jk == '1') {
                    ++$lama_l10;
                } else {
                    ++$lama_p210;
                }
            }
        }

        $baru_l9 = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '1')
        ->where('pemeriksaans.mikro_pengecatan', '=', 'gram')
        ->where('pasiens.jk', '=', '1')
        ->count();
        $baru_p29 = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '1')
        ->where('pemeriksaans.mikro_pengecatan', '=', 'gram')
        ->where('pasiens.jk', '=', '0')
        ->count();
        $lama_l9 = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '0')
        ->where('pemeriksaans.mikro_pengecatan', '=', 'gram')
        ->where('pasiens.jk', '=', '1')
        ->count();

        $lama_p29 = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '0')
        ->where('pemeriksaans.mikro_pengecatan', '=', 'gram')
        ->where('pasiens.jk', '=', '0')
        ->count();
        $data[] = [
            'layanan' => 'Jumlah pemeriksaan hematologi',
            'baru_l' => $baru_l7,
            'baru_p' => $baru_p27,
            'baru_jumlah' => $baru_l7 + $baru_p27,
            'lama_l' => $lama_l7,
            'lama_p' => $lama_p27,
            'lama_jumlah' => $lama_l7 + $lama_p27,
            'total' => $baru_l7 + $baru_p27 + $lama_l7 + $lama_p27,
        ];
        $data[] = [
            'layanan' => 'Jumlah Kimia Klinik',
            'baru_l' => $baru_l8,
            'baru_p' => $baru_p28,
            'baru_jumlah' => $baru_l8 + $baru_p28,
            'lama_l' => $lama_l8,
            'lama_p' => $lama_p28,
            'lama_jumlah' => $lama_l8 + $lama_p28,
            'total' => $baru_l8 + $baru_p28 + $lama_l8 + $lama_p28,
        ];

        $baru_l12 = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '1')
        ->whereNotNull('pemeriksaans.urinalisa_protein')
        ->where('pasiens.jk', '=', '1')
        ->count();
        $baru_p212 = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '1')
        ->whereNotNull('pemeriksaans.urinalisa_protein')
        ->where('pasiens.jk', '=', '0')
        ->count();
        $lama_l12 = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '0')
        ->whereNotNull('pemeriksaans.urinalisa_protein')
        ->where('pasiens.jk', '=', '1')
        ->count();

        $lama_p212 = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '0')
        ->whereNotNull('pemeriksaans.urinalisa_protein')
        ->where('pasiens.jk', '=', '0')
        ->count();
        $data[] = [
            'layanan' => 'Jumlah pemeriksaan urinalisa',
            'baru_l' => $baru_l12,
            'baru_p' => $baru_p212,
            'baru_jumlah' => $baru_l12 + $baru_p212,
            'lama_l' => $lama_l12,
            'lama_p' => $lama_p212,
            'lama_jumlah' => $lama_l12 + $lama_p212,
            'total' => $baru_l12 + $baru_p212 + $lama_l12 + $lama_p212,
        ];
        $data[] = [
            'layanan' => 'Jumlah pemeriksaan mikrobiologi',
            'baru_l' => $baru_l9,
            'baru_p' => $baru_p29,
            'baru_jumlah' => $baru_l9 + $baru_p29,
            'lama_l' => $lama_l9,
            'lama_p' => $lama_p29,
            'lama_jumlah' => $lama_l9 + $lama_p29,
            'total' => $baru_l9 + $baru_p29 + $lama_l9 + $lama_p29,
        ];
        $data[] = [
            'layanan' => 'Jumlah pemeriksaan imunologi',
            'baru_l' => $baru_l10,
            'baru_p' => $baru_p210,
            'baru_jumlah' => $baru_l10 + $baru_p210,
            'lama_l' => $lama_l10,
            'lama_p' => $lama_p210,
            'lama_jumlah' => $lama_l10 + $lama_p210,
            'total' => $baru_l10 + $baru_p210 + $lama_l10 + $lama_p210,
        ];
        $data[] = [
            'layanan' => 'Jumlah pemeriksaan tinja',
            'baru_l' => 0,
            'baru_p' => 0,
            'baru_jumlah' => 0,
            'lama_l' => 0,
            'lama_p' => 0,
            'lama_jumlah' => 0,
            'total' => 0,
        ];

        return view('laporan.spt3', ['pemeriksaan' => $data]);
    }
}
