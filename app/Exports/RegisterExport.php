<?php

namespace App\Exports;
use App\Exports\HarianExport;
use App\Exports\BulananExport;
use App\Exports\ImunologiExport;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class RegisterExport implements WithMultipleSheets
{

    protected $from;
    protected $to;

    public function __construct($from, $to)
    {
        $this->from = $from;
        $this->to = $to;
    }


    /**
     * @return array
     */
    public function sheets(): array
    {
        $sheets = [];

        $sheets[] = new HarianExport($this->from, $this->to);
        $sheets[] = new BulananExport($this->from, $this->to);
        $sheets[] = new ImunologiExport($this->from, $this->to);

        return $sheets;
    }
}
