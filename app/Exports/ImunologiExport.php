<?php

namespace App\Exports;

use App\Pemeriksaan;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;

class ImunologiExport implements FromView, ShouldAutoSize, WithEvents, WithTitle
{
    protected $from;
    protected $to;

    public function __construct($from, $to)
    {
        $this->from = $from;
        $this->to = $to;
    }

    public function title(): string
    {
        return 'Laporan Imunologi';
    }

    public function registerEvents(): array
    {
        
        return [
        AfterSheet::class => function (AfterSheet $event) {
            $styleBody = [
                'borders' => [
                    'allBorders' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
                        'color' => ['argb' => '00000000'],
                         ],
                    ],
                ];
                $hbsag = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
                ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
                ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
                ->whereNotNull('pemeriksaans.hbsag')
                ->get();
                $widal_o = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
                ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
                ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
                ->whereNotNull('pemeriksaans.widal_o')
                ->get();
                $widal_h = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
                ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
                ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
                ->whereNotNull('pemeriksaans.widal_h')
                ->get();
                $goldar = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
                ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
                ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
                ->whereNotNull('pemeriksaans.goldar')
                ->get();
                $hiv_1 = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
                ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
                ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
                ->whereNotNull('pemeriksaans.hiv_1')
                ->get();
                $hiv_2 = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
                ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
                ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
                ->whereNotNull('pemeriksaans.hiv_2')
                ->get();
                $hiv_3 = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
                ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
                ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
                ->whereNotNull('pemeriksaans.hiv_3')
                ->get();
                $ims = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
                ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
                ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
                ->whereNotNull('pemeriksaans.ims')
                ->get();
                $rpr = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
                ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
                ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
                ->whereNotNull('pemeriksaans.rpr')
                ->get();
                $baru_l10 = 0;
                $baru_p210 = 0;
                $lama_l10 = 0;
                $lama_p210 = 0;
                foreach ($hbsag as $klinik) {
                    if ($klinik->is_first == '1') {
                        if ($klinik->jk == '1') {
                            ++$baru_l10;
                        } else {
                            ++$baru_p210;
                        }
                    } else {
                        if ($klinik->jk == '1') {
                            ++$lama_l10;
                        } else {
                            ++$lama_p210;
                        }
                    }
                }
                foreach ($widal_o as $klinik) {
                    if ($klinik->is_first == '1') {
                        if ($klinik->jk == '1') {
                            ++$baru_l10;
                        } else {
                            ++$baru_p210;
                        }
                    } else {
                        if ($klinik->jk == '1') {
                            ++$lama_l10;
                        } else {
                            ++$lama_p210;
                        }
                    }
                }
                foreach ($widal_h as $klinik) {
                    if ($klinik->is_first == '1') {
                        if ($klinik->jk == '1') {
                            ++$baru_l10;
                        } else {
                            ++$baru_p210;
                        }
                    } else {
                        if ($klinik->jk == '1') {
                            ++$lama_l10;
                        } else {
                            ++$lama_p210;
                        }
                    }
                }
                foreach ($goldar as $klinik) {
                    if ($klinik->is_first == '1') {
                        if ($klinik->jk == '1') {
                            ++$baru_l10;
                        } else {
                            ++$baru_p210;
                        }
                    } else {
                        if ($klinik->jk == '1') {
                            ++$lama_l10;
                        } else {
                            ++$lama_p210;
                        }
                    }
                }
                foreach ($hiv_1 as $klinik) {
                    if ($klinik->is_first == '1') {
                        if ($klinik->jk == '1') {
                            ++$baru_l10;
                        } else {
                            ++$baru_p210;
                        }
                    } else {
                        if ($klinik->jk == '1') {
                            ++$lama_l10;
                        } else {
                            ++$lama_p210;
                        }
                    }
                }
                foreach ($hiv_2 as $klinik) {
                    if ($klinik->is_first == '1') {
                        if ($klinik->jk == '1') {
                            ++$baru_l10;
                        } else {
                            ++$baru_p210;
                        }
                    } else {
                        if ($klinik->jk == '1') {
                            ++$lama_l10;
                        } else {
                            ++$lama_p210;
                        }
                    }
                }
                foreach ($hiv_3 as $klinik) {
                    if ($klinik->is_first == '1') {
                        if ($klinik->jk == '1') {
                            ++$baru_l10;
                        } else {
                            ++$baru_p210;
                        }
                    } else {
                        if ($klinik->jk == '1') {
                            ++$lama_l10;
                        } else {
                            ++$lama_p210;
                        }
                    }
                }
                foreach ($ims as $klinik) {
                    if ($klinik->is_first == '1') {
                        if ($klinik->jk == '1') {
                            ++$baru_l10;
                        } else {
                            ++$baru_p210;
                        }
                    } else {
                        if ($klinik->jk == '1') {
                            ++$lama_l10;
                        } else {
                            ++$lama_p210;
                        }
                    }
                }
                foreach ($rpr as $klinik) {
                    if ($klinik->is_first == '1') {
                        if ($klinik->jk == '1') {
                            ++$baru_l10;
                        } else {
                            ++$baru_p210;
                        }
                    } else {
                        if ($klinik->jk == '1') {
                            ++$lama_l10;
                        } else {
                            ++$lama_p210;
                        }
                    }
                }
                
            $event->sheet->getDelegate()->getStyle('A1:O32')->applyFromArray($styleBody);
            $event->sheet->getDelegate()->mergeCells('A3:A32')->setCellValue('A3','Jumlah Pemeriksaan Imunologi');
            $event->sheet->getDelegate()->getStyle('A1:O2')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
            $event->sheet->getDelegate()->getStyle('A3:B32')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
            
            $event->sheet->getDelegate()->mergeCells('B3:B4');
            $event->sheet->getDelegate()->mergeCells('I3:I4');
            $event->sheet->getDelegate()->mergeCells('N3:N4');
            $event->sheet->getDelegate()->mergeCells('O3:O4');
            
            $event->sheet->getDelegate()->mergeCells('B5:B8');
            $event->sheet->getDelegate()->mergeCells('I5:I8');
            $event->sheet->getDelegate()->mergeCells('N5:N8');
            $event->sheet->getDelegate()->mergeCells('O5:O8');
            
            $event->sheet->getDelegate()->mergeCells('B9:B12');
            $event->sheet->getDelegate()->mergeCells('I9:I12');
            $event->sheet->getDelegate()->mergeCells('N9:N12');
            $event->sheet->getDelegate()->mergeCells('O9:O12');
            
            $event->sheet->getDelegate()->mergeCells('B13:B16');
            $event->sheet->getDelegate()->mergeCells('I13:I16');
            $event->sheet->getDelegate()->mergeCells('N13:N16');
            $event->sheet->getDelegate()->mergeCells('O13:O16');
            
            $event->sheet->getDelegate()->mergeCells('B17:B25');
            $event->sheet->getDelegate()->mergeCells('I17:I19');
            $event->sheet->getDelegate()->mergeCells('N17:N19');
            $event->sheet->getDelegate()->mergeCells('O17:O19');
            $event->sheet->getDelegate()->mergeCells('I20:I22');
            $event->sheet->getDelegate()->mergeCells('N20:N22');
            $event->sheet->getDelegate()->mergeCells('O20:O22');
            $event->sheet->getDelegate()->mergeCells('I23:I25');
            $event->sheet->getDelegate()->mergeCells('N23:N25');
            $event->sheet->getDelegate()->mergeCells('O23:O25');
            
            $event->sheet->getDelegate()->mergeCells('B26:B27');
            $event->sheet->getDelegate()->mergeCells('I26:I27');
            $event->sheet->getDelegate()->mergeCells('N26:N27');
            $event->sheet->getDelegate()->mergeCells('O26:O27');
            
            $event->sheet->getDelegate()->mergeCells('B28:B32');
            $event->sheet->getDelegate()->mergeCells('I28:I32');
            $event->sheet->getDelegate()->mergeCells('N28:N32');
            $event->sheet->getDelegate()->mergeCells('O28:O32');
            
            $event->sheet->getDelegate()->mergeCells('C17:C19');
            $event->sheet->getDelegate()->mergeCells('C20:C22');
            $event->sheet->getDelegate()->mergeCells('C23:C25');
            
            $event->sheet->getDelegate()->getStyle('C17:C25')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
         
            $event->sheet->getDelegate()->getStyle('I3:I32')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
            $event->sheet->getDelegate()->getStyle('N3:O32')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
            $event->sheet->getDelegate()->setCellValue('N33', 'TOTAL');
            $event->sheet->getDelegate()->setCellValue('O33', $baru_l10 + $baru_p210 + $lama_l10 + $lama_p210);
        },
    ];
}

public function view(): View
{
    $hbasg_pos_l_lama = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
    ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
    ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
    ->where('pemeriksaans.is_first', '=', '0')
    ->where('pemeriksaans.hbsag', '1')
    ->where('pasiens.jk', '=', '1')
    ->count();
        $hbasg_pos_p_lama = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '0')
        ->where('pemeriksaans.hbsag', '1')
        ->where('pemeriksaans.is_hamil', '0')
        ->where('pasiens.jk', '=', '0')
        ->count();
        $hbasg_pos_h_lama = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '0')
        ->where('pemeriksaans.hbsag', '1')
        ->where('pemeriksaans.is_hamil', '1')
        ->where('pasiens.jk', '=', '0')
        ->count();
        $hbasg_neg_l_lama = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '0')
        ->where('pemeriksaans.hbsag', '0')
        ->where('pasiens.jk', '=', '1')
        ->count();
        $hbasg_neg_p_lama = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '0')
        ->where('pemeriksaans.hbsag', '0')
        ->where('pemeriksaans.is_hamil', '0')
        ->where('pasiens.jk', '=', '0')
        ->count();
        $hbasg_neg_h_lama = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '0')
        ->where('pemeriksaans.hbsag', '0')
        ->where('pemeriksaans.is_hamil', '1')
        ->where('pasiens.jk', '=', '0')
        ->count();

        $widal_o_neg_l_lama = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '0')
        ->where('pemeriksaans.widal_o', 'negatif')
        ->where('pasiens.jk', '=', '1')
        ->count();
        $widal_o_neg_p_lama = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '0')
        ->where('pemeriksaans.widal_o', 'negatif')
        ->where('pemeriksaans.is_hamil', '0')
        ->where('pasiens.jk', '=', '0')
        ->count();
        $widal_o_neg_h_lama = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '0')
        ->where('pemeriksaans.widal_o', 'negatif')
        ->where('pemeriksaans.is_hamil', '1')
        ->where('pasiens.jk', '=', '0')
        ->count();
        $widal_o_pos80_l_lama = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '0')
        ->where('pemeriksaans.widal_o', 'pos 1/80')
        ->where('pasiens.jk', '=', '1')
        ->count();
        $widal_o_pos80_p_lama = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '0')
        ->where('pemeriksaans.widal_o', 'pos 1/80')
        ->where('pemeriksaans.is_hamil', '0')
        ->where('pasiens.jk', '=', '0')
        ->count();
        $widal_o_pos80_h_lama = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '0')
        ->where('pemeriksaans.widal_o', 'pos 1/80')
        ->where('pemeriksaans.is_hamil', '1')
        ->where('pasiens.jk', '=', '0')
        ->count();
        $widal_o_pos160_l_lama = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '0')
        ->where('pemeriksaans.widal_o', 'pos 1/160')
        ->where('pasiens.jk', '=', '1')
        ->count();
        $widal_o_pos160_p_lama = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '0')
        ->where('pemeriksaans.widal_o', 'pos 1/160')
        ->where('pemeriksaans.is_hamil', '0')
        ->where('pasiens.jk', '=', '0')
        ->count();
        $widal_o_pos160_h_lama = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '0')
        ->where('pemeriksaans.widal_o', 'pos 1/160')
        ->where('pemeriksaans.is_hamil', '1')
        ->where('pasiens.jk', '=', '0')
        ->count();
        $widal_o_pos320_l_lama = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '0')
        ->where('pemeriksaans.widal_o', 'pos 1/320')
        ->where('pasiens.jk', '=', '1')
        ->count();
        $widal_o_pos320_p_lama = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '0')
        ->where('pemeriksaans.widal_o', 'pos 1/320')
        ->where('pemeriksaans.is_hamil', '0')
        ->where('pasiens.jk', '=', '0')
        ->count();
        $widal_o_pos320_h_lama = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '0')
        ->where('pemeriksaans.widal_o', 'pos 1/320')
        ->where('pemeriksaans.is_hamil', '1')
        ->where('pasiens.jk', '=', '0')
        ->count();

        $widal_h_neg_l_lama = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '0')
        ->where('pemeriksaans.widal_h', 'negatif')
        ->where('pasiens.jk', '=', '1')
        ->count();
        $widal_h_neg_p_lama = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '0')
        ->where('pemeriksaans.widal_h', 'negatif')
        ->where('pemeriksaans.is_hamil', '0')
        ->where('pasiens.jk', '=', '0')
        ->count();
        $widal_h_neg_h_lama = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '0')
        ->where('pemeriksaans.widal_h', 'negatif')
        ->where('pemeriksaans.is_hamil', '1')
        ->where('pasiens.jk', '=', '0')
        ->count();
        $widal_h_pos80_l_lama = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '0')
        ->where('pemeriksaans.widal_h', 'pos 1/80')
        ->where('pasiens.jk', '=', '1')
        ->count();
        $widal_h_pos80_p_lama = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '0')
        ->where('pemeriksaans.widal_h', 'pos 1/80')
        ->where('pemeriksaans.is_hamil', '0')
        ->where('pasiens.jk', '=', '0')
        ->count();
        $widal_h_pos80_h_lama = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '0')
        ->where('pemeriksaans.widal_h', 'pos 1/80')
        ->where('pemeriksaans.is_hamil', '1')
        ->where('pasiens.jk', '=', '0')
        ->count();
        $widal_h_pos160_l_lama = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '0')
        ->where('pemeriksaans.widal_h', 'pos 1/160')
        ->where('pasiens.jk', '=', '1')
        ->count();
        $widal_h_pos160_p_lama = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '0')
        ->where('pemeriksaans.widal_h', 'pos 1/160')
        ->where('pemeriksaans.is_hamil', '0')
        ->where('pasiens.jk', '=', '0')
        ->count();
        $widal_h_pos160_h_lama = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '0')
        ->where('pemeriksaans.widal_h', 'pos 1/160')
        ->where('pemeriksaans.is_hamil', '1')
        ->where('pasiens.jk', '=', '0')
        ->count();
        $widal_h_pos320_l_lama = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '0')
        ->where('pemeriksaans.widal_h', 'pos 1/320')
        ->where('pasiens.jk', '=', '1')
        ->count();
        $widal_h_pos320_p_lama = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '0')
        ->where('pemeriksaans.widal_h', 'pos 1/320')
        ->where('pemeriksaans.is_hamil', '0')
        ->where('pasiens.jk', '=', '0')
        ->count();
        $widal_h_pos320_h_lama = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '0')
        ->where('pemeriksaans.widal_h', 'pos 1/320')
        ->where('pemeriksaans.is_hamil', '1')
        ->where('pasiens.jk', '=', '0')
        ->count();

        $goldar_a_l_lama = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '0')
        ->where('pemeriksaans.goldar', 'A')
        ->where('pasiens.jk', '=', '1')
        ->count();
        $goldar_a_p_lama = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '0')
        ->where('pemeriksaans.goldar', 'A')
        ->where('pemeriksaans.is_hamil', '0')
        ->where('pasiens.jk', '=', '0')
        ->count();
        $goldar_a_h_lama = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '0')
        ->where('pemeriksaans.goldar', 'A')
        ->where('pemeriksaans.is_hamil', '1')
        ->where('pasiens.jk', '=', '0')
        ->count();
        $goldar_b_l_lama = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '0')
        ->where('pemeriksaans.goldar', 'b')
        ->where('pasiens.jk', '=', '1')
        ->count();
        $goldar_b_p_lama = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '0')
        ->where('pemeriksaans.goldar', 'B')
        ->where('pemeriksaans.is_hamil', '0')
        ->where('pasiens.jk', '=', '0')
        ->count();
        $goldar_b_h_lama = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '0')
        ->where('pemeriksaans.goldar', 'B')
        ->where('pemeriksaans.is_hamil', '1')
        ->where('pasiens.jk', '=', '0')
        ->count();
        $goldar_ab_l_lama = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '0')
        ->where('pemeriksaans.goldar', 'AB')
        ->where('pasiens.jk', '=', '1')
        ->count();
        $goldar_ab_p_lama = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '0')
        ->where('pemeriksaans.goldar', 'AB')
        ->where('pemeriksaans.is_hamil', '0')
        ->where('pasiens.jk', '=', '0')
        ->count();
        $goldar_ab_h_lama = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '0')
        ->where('pemeriksaans.goldar', 'AB')
        ->where('pemeriksaans.is_hamil', '1')
        ->where('pasiens.jk', '=', '0')
        ->count();
        $goldar_o_l_lama = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '0')
        ->where('pemeriksaans.goldar', 'O')
        ->where('pasiens.jk', '=', '1')
        ->count();
        $goldar_o_p_lama = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '0')
        ->where('pemeriksaans.goldar', 'O')
        ->where('pemeriksaans.is_hamil', '0')
        ->where('pasiens.jk', '=', '0')
        ->count();
        $goldar_o_h_lama = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '0')
        ->where('pemeriksaans.goldar', 'O')
        ->where('pemeriksaans.is_hamil', '1')
        ->where('pasiens.jk', '=', '0')
        ->count();

        $hiv_1_reak_l_lama = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '0')
        ->where('pemeriksaans.hiv_1', 'reaktif')
        ->where('pasiens.jk', '=', '1')
        ->count();
        $hiv_1_reak_p_lama = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '0')
        ->where('pemeriksaans.hiv_1', 'reaktif')
        ->where('pemeriksaans.is_hamil', '0')
        ->where('pasiens.jk', '=', '0')
        ->count();
        $hiv_1_reak_h_lama = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '0')
        ->where('pemeriksaans.hiv_1', 'reaktif')
        ->where('pemeriksaans.is_hamil', '1')
        ->where('pasiens.jk', '=', '0')
        ->count();
        $hiv_1_non_l_lama = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '0')
        ->where('pemeriksaans.hiv_1', 'non')
        ->where('pasiens.jk', '=', '1')
        ->count();
        $hiv_1_non_p_lama = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '0')
        ->where('pemeriksaans.hiv_1', 'non')
        ->where('pemeriksaans.is_hamil', '0')
        ->where('pasiens.jk', '=', '0')
        ->count();
        $hiv_1_non_h_lama = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '0')
        ->where('pemeriksaans.hiv_1', 'non')
        ->where('pemeriksaans.is_hamil', '1')
        ->where('pasiens.jk', '=', '0')
        ->count();
        $hiv_1_int_l_lama = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '0')
        ->where('pemeriksaans.hiv_1', 'indeterminate')
        ->where('pasiens.jk', '=', '1')
        ->count();
        $hiv_1_int_p_lama = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '0')
        ->where('pemeriksaans.hiv_1', 'indeterminate')
        ->where('pemeriksaans.is_hamil', '0')
        ->where('pasiens.jk', '=', '0')
        ->count();
        $hiv_1_int_h_lama = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '0')
        ->where('pemeriksaans.hiv_1', 'indeterminate')
        ->where('pemeriksaans.is_hamil', '1')
        ->where('pasiens.jk', '=', '0')
        ->count();

        $hiv_2_reak_l_lama = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '0')
        ->where('pemeriksaans.hiv_2', 'reaktif')
        ->where('pasiens.jk', '=', '1')
        ->count();
        $hiv_2_reak_p_lama = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '0')
        ->where('pemeriksaans.hiv_2', 'reaktif')
        ->where('pemeriksaans.is_hamil', '0')
        ->where('pasiens.jk', '=', '0')
        ->count();
        $hiv_2_reak_h_lama = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '0')
        ->where('pemeriksaans.hiv_2', 'reaktif')
        ->where('pemeriksaans.is_hamil', '1')
        ->where('pasiens.jk', '=', '0')
        ->count();
        $hiv_2_non_l_lama = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '0')
        ->where('pemeriksaans.hiv_2', 'non')
        ->where('pasiens.jk', '=', '1')
        ->count();
        $hiv_2_non_p_lama = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '0')
        ->where('pemeriksaans.hiv_2', 'non')
        ->where('pemeriksaans.is_hamil', '0')
        ->where('pasiens.jk', '=', '0')
        ->count();
        $hiv_2_non_h_lama = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '0')
        ->where('pemeriksaans.hiv_2', 'non')
        ->where('pemeriksaans.is_hamil', '1')
        ->where('pasiens.jk', '=', '0')
        ->count();
        $hiv_2_int_l_lama = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '0')
        ->where('pemeriksaans.hiv_2', 'indeterminate')
        ->where('pasiens.jk', '=', '1')
        ->count();
        $hiv_2_int_p_lama = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '0')
        ->where('pemeriksaans.hiv_2', 'indeterminate')
        ->where('pemeriksaans.is_hamil', '0')
        ->where('pasiens.jk', '=', '0')
        ->count();
        $hiv_2_int_h_lama = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '0')
        ->where('pemeriksaans.hiv_2', 'indeterminate')
        ->where('pemeriksaans.is_hamil', '1')
        ->where('pasiens.jk', '=', '0')
        ->count();

        $hiv_3_reak_l_lama = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '0')
        ->where('pemeriksaans.hiv_3', 'reaktif')
        ->where('pasiens.jk', '=', '1')
        ->count();
        $hiv_3_reak_p_lama = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '0')
        ->where('pemeriksaans.hiv_3', 'reaktif')
        ->where('pemeriksaans.is_hamil', '0')
        ->where('pasiens.jk', '=', '0')
        ->count();
        $hiv_3_reak_h_lama = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '0')
        ->where('pemeriksaans.hiv_3', 'reaktif')
        ->where('pemeriksaans.is_hamil', '1')
        ->where('pasiens.jk', '=', '0')
        ->count();
        $hiv_3_non_l_lama = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '0')
        ->where('pemeriksaans.hiv_3', 'non')
        ->where('pasiens.jk', '=', '1')
        ->count();
        $hiv_3_non_p_lama = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '0')
        ->where('pemeriksaans.hiv_3', 'non')
        ->where('pemeriksaans.is_hamil', '0')
        ->where('pasiens.jk', '=', '0')
        ->count();
        $hiv_3_non_h_lama = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '0')
        ->where('pemeriksaans.hiv_3', 'non')
        ->where('pemeriksaans.is_hamil', '1')
        ->where('pasiens.jk', '=', '0')
        ->count();
        $hiv_3_int_l_lama = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '0')
        ->where('pemeriksaans.hiv_3', 'indeterminate')
        ->where('pasiens.jk', '=', '1')
        ->count();
        $hiv_3_int_p_lama = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '0')
        ->where('pemeriksaans.hiv_3', 'indeterminate')
        ->where('pemeriksaans.is_hamil', '0')
        ->where('pasiens.jk', '=', '0')
        ->count();
        $hiv_3_int_h_lama = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '0')
        ->where('pemeriksaans.hiv_3', 'indeterminate')
        ->where('pemeriksaans.is_hamil', '1')
        ->where('pasiens.jk', '=', '0')
        ->count();

        $ims_pos_l_lama = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '0')
        ->where('pemeriksaans.ims', '1')
        ->where('pasiens.jk', '=', '1')
        ->count();
        $ims_pos_p_lama = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '0')
        ->where('pemeriksaans.ims', '1')
        ->where('pemeriksaans.is_hamil', '0')
        ->where('pasiens.jk', '=', '0')
        ->count();
        $ims_pos_h_lama = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '0')
        ->where('pemeriksaans.ims', '1')
        ->where('pemeriksaans.is_hamil', '1')
        ->where('pasiens.jk', '=', '0')
        ->count();
        $ims_neg_l_lama = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '0')
        ->where('pemeriksaans.ims', '0')
        ->where('pasiens.jk', '=', '1')
        ->count();
        $ims_neg_p_lama = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '0')
        ->where('pemeriksaans.ims', '0')
        ->where('pemeriksaans.is_hamil', '0')
        ->where('pasiens.jk', '=', '0')
        ->count();
        $ims_neg_h_lama = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '0')
        ->where('pemeriksaans.ims', '0')
        ->where('pemeriksaans.is_hamil', '1')
        ->where('pasiens.jk', '=', '0')
        ->count();

        $rpr_neg_l_lama = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '0')
        ->where('pemeriksaans.rpr', 'negatif')
        ->where('pasiens.jk', '=', '1')
        ->count();
        $rpr_neg_p_lama = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '0')
        ->where('pemeriksaans.rpr', 'negatif')
        ->where('pemeriksaans.is_hamil', '0')
        ->where('pasiens.jk', '=', '0')
        ->count();
        $rpr_neg_h_lama = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '0')
        ->where('pemeriksaans.rpr', 'negatif')
        ->where('pemeriksaans.is_hamil', '1')
        ->where('pasiens.jk', '=', '0')
        ->count();
        $rpr_pos2_l_lama = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '0')
        ->where('pemeriksaans.rpr', 'pos 1/2')
        ->where('pasiens.jk', '=', '1')
        ->count();
        $rpr_pos2_p_lama = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '0')
        ->where('pemeriksaans.rpr', 'pos 1/2')
        ->where('pemeriksaans.is_hamil', '0')
        ->where('pasiens.jk', '=', '0')
        ->count();
        $rpr_pos2_h_lama = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '0')
        ->where('pemeriksaans.rpr', 'pos 1/2')
        ->where('pemeriksaans.is_hamil', '1')
        ->where('pasiens.jk', '=', '0')
        ->count();
        $rpr_pos4_l_lama = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '0')
        ->where('pemeriksaans.rpr', 'pos 1/4')
        ->where('pasiens.jk', '=', '1')
        ->count();
        $rpr_pos4_p_lama = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '0')
        ->where('pemeriksaans.rpr', 'pos 1/4')
        ->where('pemeriksaans.is_hamil', '0')
        ->where('pasiens.jk', '=', '0')
        ->count();
        $rpr_pos4_h_lama = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '0')
        ->where('pemeriksaans.rpr', 'pos 1/4')
        ->where('pemeriksaans.is_hamil', '1')
        ->where('pasiens.jk', '=', '0')
        ->count();
        $rpr_pos8_l_lama = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '0')
        ->where('pemeriksaans.rpr', 'pos 1/8')
        ->where('pasiens.jk', '=', '1')
        ->count();
        $rpr_pos8_p_lama = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '0')
        ->where('pemeriksaans.rpr', 'pos 1/8')
        ->where('pemeriksaans.is_hamil', '0')
        ->where('pasiens.jk', '=', '0')
        ->count();
        $rpr_pos8_h_lama = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '0')
        ->where('pemeriksaans.rpr', 'pos 1/8')
        ->where('pemeriksaans.is_hamil', '1')
        ->where('pasiens.jk', '=', '0')
        ->count();
        $rpr_pos9_l_lama = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '0')
        ->where('pemeriksaans.rpr', 'pos >1/8')
        ->where('pasiens.jk', '=', '1')
        ->count();
        $rpr_pos9_p_lama = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '0')
        ->where('pemeriksaans.rpr', 'pos >1/8')
        ->where('pemeriksaans.is_hamil', '0')
        ->where('pasiens.jk', '=', '0')
        ->count();
        $rpr_pos9_h_lama = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '0')
        ->where('pemeriksaans.rpr', 'pos >1/8')
        ->where('pemeriksaans.is_hamil', '1')
        ->where('pasiens.jk', '=', '0')
        ->count();
        $hbasg_pos_l_baru = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '1')
        ->where('pemeriksaans.hbsag', '1')
        ->where('pasiens.jk', '=', '1')
        ->count();
        $hbasg_pos_p_baru = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '1')
        ->where('pemeriksaans.hbsag', '1')
        ->where('pemeriksaans.is_hamil', '0')
        ->where('pasiens.jk', '=', '0')
        ->count();
        $hbasg_pos_h_baru = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '1')
        ->where('pemeriksaans.hbsag', '1')
        ->where('pemeriksaans.is_hamil', '1')
        ->where('pasiens.jk', '=', '0')
        ->count();
        $hbasg_neg_l_baru = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '1')
        ->where('pemeriksaans.hbsag', '0')
        ->where('pasiens.jk', '=', '1')
        ->count();
        $hbasg_neg_p_baru = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '1')
        ->where('pemeriksaans.hbsag', '0')
        ->where('pemeriksaans.is_hamil', '0')
        ->where('pasiens.jk', '=', '0')
        ->count();
        $hbasg_neg_h_baru = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '1')
        ->where('pemeriksaans.hbsag', '0')
        ->where('pemeriksaans.is_hamil', '1')
        ->where('pasiens.jk', '=', '0')
        ->count();

        $widal_o_neg_l_baru = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '1')
        ->where('pemeriksaans.widal_o', 'negatif')
        ->where('pasiens.jk', '=', '1')
        ->count();
        $widal_o_neg_p_baru = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '1')
        ->where('pemeriksaans.widal_o', 'negatif')
        ->where('pemeriksaans.is_hamil', '0')
        ->where('pasiens.jk', '=', '0')
        ->count();
        $widal_o_neg_h_baru = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '1')
        ->where('pemeriksaans.widal_o', 'negatif')
        ->where('pemeriksaans.is_hamil', '1')
        ->where('pasiens.jk', '=', '0')
        ->count();
        $widal_o_pos80_l_baru = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '1')
        ->where('pemeriksaans.widal_o', 'pos 1/80')
        ->where('pasiens.jk', '=', '1')
        ->count();
        $widal_o_pos80_p_baru = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '1')
        ->where('pemeriksaans.widal_o', 'pos 1/80')
        ->where('pemeriksaans.is_hamil', '0')
        ->where('pasiens.jk', '=', '0')
        ->count();
        $widal_o_pos80_h_baru = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '1')
        ->where('pemeriksaans.widal_o', 'pos 1/80')
        ->where('pemeriksaans.is_hamil', '1')
        ->where('pasiens.jk', '=', '0')
        ->count();
        $widal_o_pos160_l_baru = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '1')
        ->where('pemeriksaans.widal_o', 'pos 1/160')
        ->where('pasiens.jk', '=', '1')
        ->count();
        $widal_o_pos160_p_baru = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '1')
        ->where('pemeriksaans.widal_o', 'pos 1/160')
        ->where('pemeriksaans.is_hamil', '0')
        ->where('pasiens.jk', '=', '0')
        ->count();
        $widal_o_pos160_h_baru = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '1')
        ->where('pemeriksaans.widal_o', 'pos 1/160')
        ->where('pemeriksaans.is_hamil', '1')
        ->where('pasiens.jk', '=', '0')
        ->count();
        $widal_o_pos320_l_baru = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '1')
        ->where('pemeriksaans.widal_o', 'pos 1/320')
        ->where('pasiens.jk', '=', '1')
        ->count();
        $widal_o_pos320_p_baru = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '1')
        ->where('pemeriksaans.widal_o', 'pos 1/320')
        ->where('pemeriksaans.is_hamil', '0')
        ->where('pasiens.jk', '=', '0')
        ->count();
        $widal_o_pos320_h_baru = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '1')
        ->where('pemeriksaans.widal_o', 'pos 1/320')
        ->where('pemeriksaans.is_hamil', '1')
        ->where('pasiens.jk', '=', '0')
        ->count();

        $widal_h_neg_l_baru = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '1')
        ->where('pemeriksaans.widal_h', 'negatif')
        ->where('pasiens.jk', '=', '1')
        ->count();
        $widal_h_neg_p_baru = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '1')
        ->where('pemeriksaans.widal_h', 'negatif')
        ->where('pemeriksaans.is_hamil', '0')
        ->where('pasiens.jk', '=', '0')
        ->count();
        $widal_h_neg_h_baru = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '1')
        ->where('pemeriksaans.widal_h', 'negatif')
        ->where('pemeriksaans.is_hamil', '1')
        ->where('pasiens.jk', '=', '0')
        ->count();
        $widal_h_pos80_l_baru = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '1')
        ->where('pemeriksaans.widal_h', 'pos 1/80')
        ->where('pasiens.jk', '=', '1')
        ->count();
        $widal_h_pos80_p_baru = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '1')
        ->where('pemeriksaans.widal_h', 'pos 1/80')
        ->where('pemeriksaans.is_hamil', '0')
        ->where('pasiens.jk', '=', '0')
        ->count();
        $widal_h_pos80_h_baru = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '1')
        ->where('pemeriksaans.widal_h', 'pos 1/80')
        ->where('pemeriksaans.is_hamil', '1')
        ->where('pasiens.jk', '=', '0')
        ->count();
        $widal_h_pos160_l_baru = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '1')
        ->where('pemeriksaans.widal_h', 'pos 1/160')
        ->where('pasiens.jk', '=', '1')
        ->count();
        $widal_h_pos160_p_baru = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '1')
        ->where('pemeriksaans.widal_h', 'pos 1/160')
        ->where('pemeriksaans.is_hamil', '0')
        ->where('pasiens.jk', '=', '0')
        ->count();
        $widal_h_pos160_h_baru = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '1')
        ->where('pemeriksaans.widal_h', 'pos 1/160')
        ->where('pemeriksaans.is_hamil', '1')
        ->where('pasiens.jk', '=', '0')
        ->count();
        $widal_h_pos320_l_baru = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '1')
        ->where('pemeriksaans.widal_h', 'pos 1/320')
        ->where('pasiens.jk', '=', '1')
        ->count();
        $widal_h_pos320_p_baru = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '1')
        ->where('pemeriksaans.widal_h', 'pos 1/320')
        ->where('pemeriksaans.is_hamil', '0')
        ->where('pasiens.jk', '=', '0')
        ->count();
        $widal_h_pos320_h_baru = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '1')
        ->where('pemeriksaans.widal_h', 'pos 1/320')
        ->where('pemeriksaans.is_hamil', '1')
        ->where('pasiens.jk', '=', '0')
        ->count();

        $goldar_a_l_baru = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '1')
        ->where('pemeriksaans.goldar', 'A')
        ->where('pasiens.jk', '=', '1')
        ->count();
        $goldar_a_p_baru = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '1')
        ->where('pemeriksaans.goldar', 'A')
        ->where('pemeriksaans.is_hamil', '0')
        ->where('pasiens.jk', '=', '0')
        ->count();
        $goldar_a_h_baru = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '1')
        ->where('pemeriksaans.goldar', 'A')
        ->where('pemeriksaans.is_hamil', '1')
        ->where('pasiens.jk', '=', '0')
        ->count();
        $goldar_b_l_baru = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '1')
        ->where('pemeriksaans.goldar', 'b')
        ->where('pasiens.jk', '=', '1')
        ->count();
        $goldar_b_p_baru = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '1')
        ->where('pemeriksaans.goldar', 'B')
        ->where('pemeriksaans.is_hamil', '0')
        ->where('pasiens.jk', '=', '0')
        ->count();
        $goldar_b_h_baru = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '1')
        ->where('pemeriksaans.goldar', 'B')
        ->where('pemeriksaans.is_hamil', '1')
        ->where('pasiens.jk', '=', '0')
        ->count();
        $goldar_ab_l_baru = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '1')
        ->where('pemeriksaans.goldar', 'AB')
        ->where('pasiens.jk', '=', '1')
        ->count();
        $goldar_ab_p_baru = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '1')
        ->where('pemeriksaans.goldar', 'AB')
        ->where('pemeriksaans.is_hamil', '0')
        ->where('pasiens.jk', '=', '0')
        ->count();
        $goldar_ab_h_baru = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '1')
        ->where('pemeriksaans.goldar', 'AB')
        ->where('pemeriksaans.is_hamil', '1')
        ->where('pasiens.jk', '=', '0')
        ->count();
        $goldar_o_l_baru = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '1')
        ->where('pemeriksaans.goldar', 'O')
        ->where('pasiens.jk', '=', '1')
        ->count();
        $goldar_o_p_baru = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '1')
        ->where('pemeriksaans.goldar', 'O')
        ->where('pemeriksaans.is_hamil', '0')
        ->where('pasiens.jk', '=', '0')
        ->count();
        $goldar_o_h_baru = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '1')
        ->where('pemeriksaans.goldar', 'O')
        ->where('pemeriksaans.is_hamil', '1')
        ->where('pasiens.jk', '=', '0')
        ->count();

        $hiv_1_reak_l_baru = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '1')
        ->where('pemeriksaans.hiv_1', 'reaktif')
        ->where('pasiens.jk', '=', '1')
        ->count();
        $hiv_1_reak_p_baru = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '1')
        ->where('pemeriksaans.hiv_1', 'reaktif')
        ->where('pemeriksaans.is_hamil', '0')
        ->where('pasiens.jk', '=', '0')
        ->count();
        $hiv_1_reak_h_baru = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '1')
        ->where('pemeriksaans.hiv_1', 'reaktif')
        ->where('pemeriksaans.is_hamil', '1')
        ->where('pasiens.jk', '=', '0')
        ->count();
        $hiv_1_non_l_baru = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '1')
        ->where('pemeriksaans.hiv_1', 'non')
        ->where('pasiens.jk', '=', '1')
        ->count();
        $hiv_1_non_p_baru = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '1')
        ->where('pemeriksaans.hiv_1', 'non')
        ->where('pemeriksaans.is_hamil', '0')
        ->where('pasiens.jk', '=', '0')
        ->count();
        $hiv_1_non_h_baru = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '1')
        ->where('pemeriksaans.hiv_1', 'non')
        ->where('pemeriksaans.is_hamil', '1')
        ->where('pasiens.jk', '=', '0')
        ->count();
        $hiv_1_int_l_baru = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '1')
        ->where('pemeriksaans.hiv_1', 'indeterminate')
        ->where('pasiens.jk', '=', '1')
        ->count();
        $hiv_1_int_p_baru = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '1')
        ->where('pemeriksaans.hiv_1', 'indeterminate')
        ->where('pemeriksaans.is_hamil', '0')
        ->where('pasiens.jk', '=', '0')
        ->count();
        $hiv_1_int_h_baru = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '1')
        ->where('pemeriksaans.hiv_1', 'indeterminate')
        ->where('pemeriksaans.is_hamil', '1')
        ->where('pasiens.jk', '=', '0')
        ->count();

        $hiv_2_reak_l_baru = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '1')
        ->where('pemeriksaans.hiv_2', 'reaktif')
        ->where('pasiens.jk', '=', '1')
        ->count();
        $hiv_2_reak_p_baru = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '1')
        ->where('pemeriksaans.hiv_2', 'reaktif')
        ->where('pemeriksaans.is_hamil', '0')
        ->where('pasiens.jk', '=', '0')
        ->count();
        $hiv_2_reak_h_baru = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '1')
        ->where('pemeriksaans.hiv_2', 'reaktif')
        ->where('pemeriksaans.is_hamil', '1')
        ->where('pasiens.jk', '=', '0')
        ->count();
        $hiv_2_non_l_baru = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '1')
        ->where('pemeriksaans.hiv_2', 'non')
        ->where('pasiens.jk', '=', '1')
        ->count();
        $hiv_2_non_p_baru = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '1')
        ->where('pemeriksaans.hiv_2', 'non')
        ->where('pemeriksaans.is_hamil', '0')
        ->where('pasiens.jk', '=', '0')
        ->count();
        $hiv_2_non_h_baru = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '1')
        ->where('pemeriksaans.hiv_2', 'non')
        ->where('pemeriksaans.is_hamil', '1')
        ->where('pasiens.jk', '=', '0')
        ->count();
        $hiv_2_int_l_baru = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '1')
        ->where('pemeriksaans.hiv_2', 'indeterminate')
        ->where('pasiens.jk', '=', '1')
        ->count();
        $hiv_2_int_p_baru = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '1')
        ->where('pemeriksaans.hiv_2', 'indeterminate')
        ->where('pemeriksaans.is_hamil', '0')
        ->where('pasiens.jk', '=', '0')
        ->count();
        $hiv_2_int_h_baru = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '1')
        ->where('pemeriksaans.hiv_2', 'indeterminate')
        ->where('pemeriksaans.is_hamil', '1')
        ->where('pasiens.jk', '=', '0')
        ->count();

        $hiv_3_reak_l_baru = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '1')
        ->where('pemeriksaans.hiv_3', 'reaktif')
        ->where('pasiens.jk', '=', '1')
        ->count();
        $hiv_3_reak_p_baru = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '1')
        ->where('pemeriksaans.hiv_3', 'reaktif')
        ->where('pemeriksaans.is_hamil', '0')
        ->where('pasiens.jk', '=', '0')
        ->count();
        $hiv_3_reak_h_baru = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '1')
        ->where('pemeriksaans.hiv_3', 'reaktif')
        ->where('pemeriksaans.is_hamil', '1')
        ->where('pasiens.jk', '=', '0')
        ->count();
        $hiv_3_non_l_baru = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '1')
        ->where('pemeriksaans.hiv_3', 'non')
        ->where('pasiens.jk', '=', '1')
        ->count();
        $hiv_3_non_p_baru = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '1')
        ->where('pemeriksaans.hiv_3', 'non')
        ->where('pemeriksaans.is_hamil', '0')
        ->where('pasiens.jk', '=', '0')
        ->count();
        $hiv_3_non_h_baru = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '1')
        ->where('pemeriksaans.hiv_3', 'non')
        ->where('pemeriksaans.is_hamil', '1')
        ->where('pasiens.jk', '=', '0')
        ->count();
        $hiv_3_int_l_baru = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '1')
        ->where('pemeriksaans.hiv_3', 'indeterminate')
        ->where('pasiens.jk', '=', '1')
        ->count();
        $hiv_3_int_p_baru = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '1')
        ->where('pemeriksaans.hiv_3', 'indeterminate')
        ->where('pemeriksaans.is_hamil', '0')
        ->where('pasiens.jk', '=', '0')
        ->count();
        $hiv_3_int_h_baru = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '1')
        ->where('pemeriksaans.hiv_3', 'indeterminate')
        ->where('pemeriksaans.is_hamil', '1')
        ->where('pasiens.jk', '=', '0')
        ->count();

        $ims_pos_l_baru = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '1')
        ->where('pemeriksaans.ims', '1')
        ->where('pasiens.jk', '=', '1')
        ->count();
        $ims_pos_p_baru = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '1')
        ->where('pemeriksaans.ims', '1')
        ->where('pemeriksaans.is_hamil', '0')
        ->where('pasiens.jk', '=', '0')
        ->count();
        $ims_pos_h_baru = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '1')
        ->where('pemeriksaans.ims', '1')
        ->where('pemeriksaans.is_hamil', '1')
        ->where('pasiens.jk', '=', '0')
        ->count();
        $ims_neg_l_baru = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '1')
        ->where('pemeriksaans.ims', '0')
        ->where('pasiens.jk', '=', '1')
        ->count();
        $ims_neg_p_baru = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '1')
        ->where('pemeriksaans.ims', '0')
        ->where('pemeriksaans.is_hamil', '0')
        ->where('pasiens.jk', '=', '0')
        ->count();
        $ims_neg_h_baru = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '1')
        ->where('pemeriksaans.ims', '0')
        ->where('pemeriksaans.is_hamil', '1')
        ->where('pasiens.jk', '=', '0')
        ->count();

        $rpr_neg_l_baru = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '1')
        ->where('pemeriksaans.rpr', 'negatif')
        ->where('pasiens.jk', '=', '1')
        ->count();
        $rpr_neg_p_baru = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '1')
        ->where('pemeriksaans.rpr', 'negatif')
        ->where('pemeriksaans.is_hamil', '0')
        ->where('pasiens.jk', '=', '0')
        ->count();
        $rpr_neg_h_baru = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '1')
        ->where('pemeriksaans.rpr', 'negatif')
        ->where('pemeriksaans.is_hamil', '1')
        ->where('pasiens.jk', '=', '0')
        ->count();
        $rpr_pos2_l_baru = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '1')
        ->where('pemeriksaans.rpr', 'pos 1/2')
        ->where('pasiens.jk', '=', '1')
        ->count();
        $rpr_pos2_p_baru = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '1')
        ->where('pemeriksaans.rpr', 'pos 1/2')
        ->where('pemeriksaans.is_hamil', '0')
        ->where('pasiens.jk', '=', '0')
        ->count();
        $rpr_pos2_h_baru = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '1')
        ->where('pemeriksaans.rpr', 'pos 1/2')
        ->where('pemeriksaans.is_hamil', '1')
        ->where('pasiens.jk', '=', '0')
        ->count();
        $rpr_pos4_l_baru = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '1')
        ->where('pemeriksaans.rpr', 'pos 1/4')
        ->where('pasiens.jk', '=', '1')
        ->count();
        $rpr_pos4_p_baru = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '1')
        ->where('pemeriksaans.rpr', 'pos 1/4')
        ->where('pemeriksaans.is_hamil', '0')
        ->where('pasiens.jk', '=', '0')
        ->count();
        $rpr_pos4_h_baru = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '1')
        ->where('pemeriksaans.rpr', 'pos 1/4')
        ->where('pemeriksaans.is_hamil', '1')
        ->where('pasiens.jk', '=', '0')
        ->count();
        $rpr_pos8_l_baru = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '1')
        ->where('pemeriksaans.rpr', 'pos 1/8')
        ->where('pasiens.jk', '=', '1')
        ->count();
        $rpr_pos8_p_baru = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '1')
        ->where('pemeriksaans.rpr', 'pos 1/8')
        ->where('pemeriksaans.is_hamil', '0')
        ->where('pasiens.jk', '=', '0')
        ->count();
        $rpr_pos8_h_baru = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '1')
        ->where('pemeriksaans.rpr', 'pos 1/8')
        ->where('pemeriksaans.is_hamil', '1')
        ->where('pasiens.jk', '=', '0')
        ->count();
        $rpr_pos9_l_baru = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '1')
        ->where('pemeriksaans.rpr', 'pos >1/8')
        ->where('pasiens.jk', '=', '1')
        ->count();
        $rpr_pos9_p_baru = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '1')
        ->where('pemeriksaans.rpr', 'pos >1/8')
        ->where('pemeriksaans.is_hamil', '0')
        ->where('pasiens.jk', '=', '0')
        ->count();
        $rpr_pos9_h_baru = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '1')
        ->where('pemeriksaans.rpr', 'pos >1/8')
        ->where('pemeriksaans.is_hamil', '1')
        ->where('pasiens.jk', '=', '0')
        ->count();

        $data[] = [
            'test' => 'Hbs Ag',
            'hasil_1' => 'Positif',
            'hasil_2' => '',
            'baru_l' => $hbasg_pos_l_baru,
            'baru_p' => $hbasg_pos_p_baru,
            'baru_h' => $hbasg_pos_h_baru,
            'baru' => $hbasg_pos_l_baru+$hbasg_pos_p_baru+$hbasg_pos_h_baru,
            'baru_jumlah' => $hbasg_neg_l_baru+$hbasg_neg_p_baru+$hbasg_neg_h_baru + $hbasg_pos_l_baru+$hbasg_pos_p_baru+$hbasg_pos_h_baru,
            'lama_l' => $hbasg_pos_l_lama,
            'lama_p' => $hbasg_pos_p_lama,
            'lama_h' => $hbasg_pos_h_lama,
            'lama' => $hbasg_pos_l_lama + $hbasg_pos_p_lama + $hbasg_pos_h_lama,
            'lama_jumlah' => $hbasg_pos_l_lama + $hbasg_pos_p_lama + $hbasg_pos_h_lama + $hbasg_neg_l_lama + $hbasg_neg_p_lama + $hbasg_neg_h_lama,
            'total' => $hbasg_neg_l_baru+$hbasg_neg_p_baru+$hbasg_neg_h_baru + $hbasg_pos_l_baru+$hbasg_pos_p_baru+$hbasg_pos_h_baru + $hbasg_pos_l_lama + $hbasg_pos_p_lama + $hbasg_pos_h_lama + $hbasg_neg_l_lama + $hbasg_neg_p_lama + $hbasg_neg_h_lama,
        ];

        $data[] = [
            'test' => 'Hbs Ag',
            'hasil_1' => 'Negatif',
            'hasil_2' => '',
            'baru_l' => $hbasg_neg_l_baru,
            'baru_p' => $hbasg_neg_p_baru,
            'baru_h' => $hbasg_neg_h_baru,
            'baru' => $hbasg_neg_l_baru+$hbasg_neg_p_baru+$hbasg_neg_h_baru,
            'baru_jumlah' => '',
            'lama_l' => $hbasg_neg_l_lama,
            'lama_p' => $hbasg_neg_p_lama,
            'lama_h' => $hbasg_neg_h_lama,
            'lama' => $hbasg_neg_l_lama + $hbasg_neg_p_lama + $hbasg_neg_h_lama,
            'lama_jumlah' => '',
            'total' => '',
        ];

        $data[] = [
            'test' => 'Widal O',
            'hasil_1' => 'Negatif',
            'hasil_2' => '',
            'baru_l' => $widal_o_neg_l_baru,
            'baru_p' => $widal_o_neg_p_baru,
            'baru_h' => $widal_o_neg_h_baru,
            'baru' => $widal_o_neg_l_baru+$widal_o_neg_p_baru+$widal_o_neg_h_baru,
            'baru_jumlah' => $widal_o_neg_l_baru+$widal_o_neg_p_baru+$widal_o_neg_h_baru+$widal_o_pos80_l_baru+$widal_o_pos80_p_baru+$widal_o_pos80_h_baru+$widal_o_pos160_l_baru+$widal_o_pos160_p_baru+$widal_o_pos160_h_baru+$widal_o_pos320_l_baru+$widal_o_pos320_p_baru+$widal_o_pos320_h_baru,
            'lama_l' => $widal_o_neg_l_lama,
            'lama_p' => $widal_o_neg_p_lama,
            'lama_h' => $widal_o_neg_h_lama,
            'lama' => $widal_o_neg_l_lama + $widal_o_neg_p_lama + $widal_o_neg_h_lama,
            'lama_jumlah' => $widal_o_neg_l_lama + $widal_o_neg_p_lama + $widal_o_neg_h_lama+$widal_o_pos80_l_lama + $widal_o_pos80_p_lama + $widal_o_pos80_h_lama+ $widal_o_pos160_l_lama + $widal_o_pos160_p_lama + $widal_o_pos160_h_lama+$widal_o_pos320_l_lama + $widal_o_pos320_p_lama + $widal_o_pos320_h_lama,
            'total' => $widal_o_neg_l_baru+$widal_o_neg_p_baru+$widal_o_neg_h_baru+$widal_o_pos80_l_baru+$widal_o_pos80_p_baru+$widal_o_pos80_h_baru+$widal_o_pos160_l_baru+$widal_o_pos160_p_baru+$widal_o_pos160_h_baru+$widal_o_pos320_l_baru+$widal_o_pos320_p_baru+$widal_o_pos320_h_baru+$widal_o_neg_l_lama + $widal_o_neg_p_lama + $widal_o_neg_h_lama+$widal_o_pos80_l_lama + $widal_o_pos80_p_lama + $widal_o_pos80_h_lama+ $widal_o_pos160_l_lama + $widal_o_pos160_p_lama + $widal_o_pos160_h_lama+$widal_o_pos320_l_lama + $widal_o_pos320_p_lama + $widal_o_pos320_h_lama,
        ];

        $data[] = [
            'test' => 'Widal O',
            'hasil_1' => 'Positif 1/80',
            'hasil_2' => '',
            'baru_l' => $widal_o_pos80_l_baru,
            'baru_p' => $widal_o_pos80_p_baru,
            'baru_h' => $widal_o_pos80_h_baru,
            'baru' => $widal_o_pos80_l_baru+$widal_o_pos80_p_baru+$widal_o_pos80_h_baru,
            'baru_jumlah' => '',
            'lama_l' => $widal_o_pos80_l_lama,
            'lama_p' => $widal_o_pos80_p_lama,
            'lama_h' => $widal_o_pos80_h_lama,
            'lama' => $widal_o_pos80_l_lama + $widal_o_pos80_p_lama + $widal_o_pos80_h_lama,
            'lama_jumlah' => '',
            'total' => '',
        ];

        $data[] = [
            'test' => 'Widal O',
            'hasil_1' => 'Positif 1/160',
            'hasil_2' => '',
            'baru_l' => $widal_o_pos160_l_baru,
            'baru_p' => $widal_o_pos160_p_baru,
            'baru_h' => $widal_o_pos160_h_baru,
            'baru' => $widal_o_pos160_l_baru+$widal_o_pos160_p_baru+$widal_o_pos160_h_baru,
            'baru_jumlah' => '',
            'lama_l' => $widal_o_pos160_l_lama,
            'lama_p' => $widal_o_pos160_p_lama,
            'lama_h' => $widal_o_pos160_h_lama,
            'lama' => $widal_o_pos160_l_lama + $widal_o_pos160_p_lama + $widal_o_pos160_h_lama,
            'lama_jumlah' => '',
            'total' => '',
        ];

        $data[] = [
            'test' => 'Widal O',
            'hasil_1' => 'Positif 1/320',
            'hasil_2' => '',
            'baru_l' => $widal_o_pos320_l_baru,
            'baru_p' => $widal_o_pos320_p_baru,
            'baru_h' => $widal_o_pos320_h_baru,
            'baru' => $widal_o_pos320_l_baru+$widal_o_pos320_p_baru+$widal_o_pos320_h_baru,
            'baru_jumlah' => '',
            'lama_l' => $widal_o_pos320_l_lama,
            'lama_p' => $widal_o_pos320_p_lama,
            'lama_h' => $widal_o_pos320_h_lama,
            'lama' => $widal_o_pos320_l_lama + $widal_o_pos320_p_lama + $widal_o_pos320_h_lama,
            'lama_jumlah' => '',
            'total' => '',
        ];

        $data[] = [
            'test' => 'Widal H',
            'hasil_1' => 'Negatif',
            'hasil_2' => '',
            'baru_l' => $widal_h_neg_l_baru,
            'baru_p' => $widal_h_neg_p_baru,
            'baru_h' => $widal_h_neg_h_baru,
            'baru' => $widal_h_neg_l_baru+$widal_h_neg_p_baru+$widal_h_neg_h_baru,
            'baru_jumlah' => $widal_h_neg_l_baru+$widal_h_neg_p_baru+$widal_h_neg_h_baru+$widal_h_pos80_l_baru+$widal_h_pos80_p_baru+$widal_h_pos80_h_baru+$widal_h_pos160_l_baru+$widal_h_pos160_p_baru+$widal_h_pos160_h_baru+$widal_h_pos320_l_baru+$widal_h_pos320_p_baru+$widal_h_pos320_h_baru,
            'lama_l' => $widal_h_neg_l_lama,
            'lama_p' => $widal_h_neg_p_lama,
            'lama_h' => $widal_h_neg_h_lama,
            'lama' => $widal_h_neg_l_lama + $widal_h_neg_p_lama + $widal_h_neg_h_lama,
            'lama_jumlah' => $widal_h_neg_l_lama + $widal_h_neg_p_lama + $widal_h_neg_h_lama+$widal_h_pos80_l_lama + $widal_h_pos80_p_lama + $widal_h_pos80_h_lama+ $widal_h_pos160_l_lama + $widal_h_pos160_p_lama + $widal_h_pos160_h_lama+$widal_h_pos320_l_lama + $widal_h_pos320_p_lama + $widal_h_pos320_h_lama,
            'total' => $widal_h_neg_l_baru+$widal_h_neg_p_baru+$widal_h_neg_h_baru+$widal_h_pos80_l_baru+$widal_h_pos80_p_baru+$widal_h_pos80_h_baru+$widal_h_pos160_l_baru+$widal_h_pos160_p_baru+$widal_h_pos160_h_baru+$widal_h_pos320_l_baru+$widal_h_pos320_p_baru+$widal_h_pos320_h_baru+$widal_h_neg_l_lama + $widal_h_neg_p_lama + $widal_h_neg_h_lama+$widal_h_pos80_l_lama + $widal_h_pos80_p_lama + $widal_h_pos80_h_lama+ $widal_h_pos160_l_lama + $widal_h_pos160_p_lama + $widal_h_pos160_h_lama+$widal_h_pos320_l_lama + $widal_h_pos320_p_lama + $widal_h_pos320_h_lama,
        ];

        $data[] = [
            'test' => 'Widal H',
            'hasil_1' => 'Positif 1/80',
            'hasil_2' => '',
            'baru_l' => $widal_h_pos80_l_baru,
            'baru_p' => $widal_h_pos80_p_baru,
            'baru_h' => $widal_h_pos80_h_baru,
            'baru' => $widal_h_pos80_l_baru+$widal_h_pos80_p_baru+$widal_h_pos80_h_baru,
            'baru_jumlah' => '',
            'lama_l' => $widal_h_pos80_l_lama,
            'lama_p' => $widal_h_pos80_p_lama,
            'lama_h' => $widal_h_pos80_h_lama,
            'lama' => $widal_h_pos80_l_lama + $widal_h_pos80_p_lama + $widal_h_pos80_h_lama,
            'lama_jumlah' => '',
            'total' => '',
        ];

        $data[] = [
            'test' => 'Widal H',
            'hasil_1' => 'Positif 1/160',
            'hasil_2' => '',
            'baru_l' => $widal_h_pos160_l_baru,
            'baru_p' => $widal_h_pos160_p_baru,
            'baru_h' => $widal_h_pos160_h_baru,
            'baru' => $widal_h_pos160_l_baru+$widal_h_pos160_p_baru+$widal_h_pos160_h_baru,
            'baru_jumlah' => '',
            'lama_l' => $widal_h_pos160_l_lama,
            'lama_p' => $widal_h_pos160_p_lama,
            'lama_h' => $widal_h_pos160_h_lama,
            'lama' => $widal_h_pos160_l_lama + $widal_h_pos160_p_lama + $widal_h_pos160_h_lama,
            'lama_jumlah' => '',
            'total' => '',
        ];

        $data[] = [
            'test' => 'Widal H',
            'hasil_1' => 'Positif 1/320',
            'hasil_2' => '',
            'baru_l' => $widal_h_pos320_l_baru,
            'baru_p' => $widal_h_pos320_p_baru,
            'baru_h' => $widal_h_pos320_h_baru,
            'baru' => $widal_h_pos320_l_baru+$widal_h_pos320_p_baru+$widal_h_pos320_h_baru,
            'baru_jumlah' => '',
            'lama_l' => $widal_h_pos320_l_lama,
            'lama_p' => $widal_h_pos320_p_lama,
            'lama_h' => $widal_h_pos320_h_lama,
            'lama' => $widal_h_pos320_l_lama + $widal_h_pos320_p_lama + $widal_h_pos320_h_lama,
            'lama_jumlah' => '',
            'total' => '',
        ];

        $data[] = [
            'test' => 'Gol Darah',
            'hasil_1' => 'A',
            'hasil_2' => '',
            'baru_l' => $goldar_a_l_baru,
            'baru_p' => $goldar_a_p_baru,
            'baru_h' => $goldar_a_h_baru,
            'baru' => $goldar_a_l_baru+$goldar_a_p_baru+$goldar_a_h_baru,
            'baru_jumlah' => $goldar_a_l_baru+$goldar_a_p_baru+$goldar_a_h_baru+$goldar_b_l_baru+$goldar_b_p_baru+$goldar_b_h_baru+$goldar_ab_l_baru+$goldar_ab_p_baru+$goldar_ab_h_baru+$goldar_o_l_baru+$goldar_o_p_baru+$goldar_o_h_baru,
            'lama_l' => $goldar_a_l_lama,
            'lama_p' => $goldar_a_p_lama,
            'lama_h' => $goldar_a_h_lama,
            'lama' => $goldar_a_l_lama + $goldar_a_p_lama + $goldar_a_h_lama,
            'lama_jumlah' => $goldar_a_l_lama + $goldar_a_p_lama + $goldar_a_h_lama+$goldar_b_l_lama + $goldar_b_p_lama + $goldar_b_h_lama+$goldar_ab_l_lama + $goldar_ab_p_lama + $goldar_ab_h_lama+$goldar_o_l_lama + $goldar_o_p_lama + $goldar_o_h_lama,
            'total' => $goldar_a_l_baru+$goldar_a_p_baru+$goldar_a_h_baru+$goldar_b_l_baru+$goldar_b_p_baru+$goldar_b_h_baru+$goldar_ab_l_baru+$goldar_ab_p_baru+$goldar_ab_h_baru+$goldar_o_l_baru+$goldar_o_p_baru+$goldar_o_h_baru+ $goldar_a_l_lama + $goldar_a_p_lama + $goldar_a_h_lama+$goldar_b_l_lama + $goldar_b_p_lama + $goldar_b_h_lama+$goldar_ab_l_lama + $goldar_ab_p_lama + $goldar_ab_h_lama+$goldar_o_l_lama + $goldar_o_p_lama + $goldar_o_h_lama,
        ];

        $data[] = [
            'test' => 'Gol Darah',
            'hasil_1' => 'B',
            'hasil_2' => '',
            'baru_l' => $goldar_b_l_baru,
            'baru_p' => $goldar_b_p_baru,
            'baru_h' => $goldar_b_h_baru,
            'baru' => $goldar_b_l_baru+$goldar_b_p_baru+$goldar_b_h_baru,
            'baru_jumlah' => '',
            'lama_l' => $goldar_b_l_lama,
            'lama_p' => $goldar_b_p_lama,
            'lama_h' => $goldar_b_h_lama,
            'lama' => $goldar_b_l_lama + $goldar_b_p_lama + $goldar_b_h_lama,
            'lama_jumlah' => '',
            'total' => '',
        ];

        $data[] = [
            'test' => 'Gol Darah',
            'hasil_1' => 'AB',
            'hasil_2' => '',
            'baru_l' => $goldar_ab_l_baru,
            'baru_p' => $goldar_ab_p_baru,
            'baru_h' => $goldar_ab_h_baru,
            'baru' => $goldar_ab_l_baru+$goldar_ab_p_baru+$goldar_ab_h_baru,
            'baru_jumlah' => '',
            'lama_l' => $goldar_ab_l_lama,
            'lama_p' => $goldar_ab_p_lama,
            'lama_h' => $goldar_ab_h_lama,
            'lama' => $goldar_ab_l_lama + $goldar_ab_p_lama + $goldar_ab_h_lama,
            'lama_jumlah' => '',
            'total' => '',
        ];

        $data[] = [
            'test' => 'Gol Darah',
            'hasil_1' => 'O',
            'hasil_2' => '',
            'baru_l' => $goldar_o_l_baru,
            'baru_p' => $goldar_o_p_baru,
            'baru_h' => $goldar_o_h_baru,
            'baru' => $goldar_o_l_baru+$goldar_o_p_baru+$goldar_o_h_baru,
            'baru_jumlah' => '',
            'lama_l' => $goldar_o_l_lama,
            'lama_p' => $goldar_o_p_lama,
            'lama_h' => $goldar_o_h_lama,
            'lama' => $goldar_o_l_lama + $goldar_o_p_lama + $goldar_o_h_lama,
            'lama_jumlah' => '',
            'total' => '',
        ];

        $data[] = [
            'test' => 'HIV',
            'hasil_1' => 'R1',
            'hasil_2' => 'Reaktif',
            'baru_l' => $hiv_1_reak_l_baru,
            'baru_p' => $hiv_1_reak_p_baru,
            'baru_h' => $hiv_1_reak_h_baru,
            'baru' => $hiv_1_reak_l_baru+$hiv_1_reak_p_baru+$hiv_1_reak_h_baru,
            'baru_jumlah' => $hiv_1_reak_l_baru+$hiv_1_reak_p_baru+$hiv_1_reak_h_baru+$hiv_1_non_l_baru+$hiv_1_non_p_baru+$hiv_1_non_h_baru+$hiv_1_int_l_baru+$hiv_1_int_p_baru+$hiv_1_int_h_baru,
            'lama_l' => $hiv_1_reak_l_lama,
            'lama_p' => $hiv_1_reak_p_lama,
            'lama_h' => $hiv_1_reak_h_lama,
            'lama' => $hiv_1_reak_l_lama + $hiv_1_reak_p_lama + $hiv_1_reak_h_lama,
            'lama_jumlah' => $hiv_1_reak_l_lama + $hiv_1_reak_p_lama + $hiv_1_reak_h_lama+$hiv_1_non_l_lama + $hiv_1_non_p_lama + $hiv_1_non_h_lama+$hiv_1_int_l_lama + $hiv_1_int_p_lama + $hiv_1_int_h_lama,
            'total' => $hiv_1_reak_l_baru+$hiv_1_reak_p_baru+$hiv_1_reak_h_baru+$hiv_1_non_l_baru+$hiv_1_non_p_baru+$hiv_1_non_h_baru+$hiv_1_int_l_baru+$hiv_1_int_p_baru+$hiv_1_int_h_baru+$hiv_1_reak_l_lama + $hiv_1_reak_p_lama + $hiv_1_reak_h_lama+$hiv_1_non_l_lama + $hiv_1_non_p_lama + $hiv_1_non_h_lama+$hiv_1_int_l_lama + $hiv_1_int_p_lama + $hiv_1_int_h_lama,
        ];

        $data[] = [
            'test' => 'HIV',
            'hasil_1' => 'R1',
            'hasil_2' => 'Non R',
            'baru_l' => $hiv_1_non_l_baru,
            'baru_p' => $hiv_1_non_p_baru,
            'baru_h' => $hiv_1_non_h_baru,
            'baru' => $hiv_1_non_l_baru+$hiv_1_non_p_baru+$hiv_1_non_h_baru,
            'baru_jumlah' => '',
            'lama_l' => $hiv_1_non_l_lama,
            'lama_p' => $hiv_1_non_p_lama,
            'lama_h' => $hiv_1_non_h_lama,
            'lama' => $hiv_1_non_l_lama + $hiv_1_non_p_lama + $hiv_1_non_h_lama,
            'lama_jumlah' => '',
            'total' => '',
        ];

        $data[] = [
            'test' => 'HIV',
            'hasil_1' => 'R1',
            'hasil_2' => 'Indeter',
            'baru_l' => $hiv_1_int_l_baru,
            'baru_p' => $hiv_1_int_p_baru,
            'baru_h' => $hiv_1_int_h_baru,
            'baru' => $hiv_1_int_l_baru+$hiv_1_int_p_baru+$hiv_1_int_h_baru,
            'baru_jumlah' => '',
            'lama_l' => $hiv_1_int_l_lama,
            'lama_p' => $hiv_1_int_p_lama,
            'lama_h' => $hiv_1_int_h_lama,
            'lama' => $hiv_1_int_l_lama + $hiv_1_int_p_lama + $hiv_1_int_h_lama,
            'lama_jumlah' => '',
            'total' => '',
        ];

        $data[] = [
            'test' => 'HIV',
            'hasil_1' => 'R2',
            'hasil_2' => 'Reaktif',
            'baru_l' => $hiv_2_reak_l_baru,
            'baru_p' => $hiv_2_reak_p_baru,
            'baru_h' => $hiv_2_reak_h_baru,
            'baru' => $hiv_2_reak_l_baru+$hiv_2_reak_p_baru+$hiv_2_reak_h_baru,
            'baru_jumlah' => $hiv_2_reak_l_baru+$hiv_2_reak_p_baru+$hiv_2_reak_h_baru+$hiv_2_non_l_baru+$hiv_2_non_p_baru+$hiv_2_non_h_baru+$hiv_2_int_l_baru+$hiv_2_int_p_baru+$hiv_2_int_h_baru,
            'lama_l' => $hiv_2_reak_l_lama,
            'lama_p' => $hiv_2_reak_p_lama,
            'lama_h' => $hiv_2_reak_h_lama,
            'lama' => $hiv_2_reak_l_lama + $hiv_2_reak_p_lama + $hiv_2_reak_h_lama,
            'lama_jumlah' => $hiv_2_reak_l_lama + $hiv_2_reak_p_lama + $hiv_2_reak_h_lama+$hiv_2_non_l_lama + $hiv_2_non_p_lama + $hiv_2_non_h_lama+$hiv_2_int_l_lama + $hiv_2_int_p_lama + $hiv_2_int_h_lama,
            'total' => $hiv_2_reak_l_baru+$hiv_2_reak_p_baru+$hiv_2_reak_h_baru+$hiv_2_non_l_baru+$hiv_2_non_p_baru+$hiv_2_non_h_baru+$hiv_2_int_l_baru+$hiv_2_int_p_baru+$hiv_2_int_h_baru+$hiv_2_reak_l_lama + $hiv_2_reak_p_lama + $hiv_2_reak_h_lama+$hiv_2_non_l_lama + $hiv_2_non_p_lama + $hiv_2_non_h_lama+$hiv_2_int_l_lama + $hiv_2_int_p_lama + $hiv_2_int_h_lama,
        ];

        $data[] = [
            'test' => 'HIV',
            'hasil_1' => 'R2',
            'hasil_2' => 'Non R',
            'baru_l' => $hiv_2_non_l_baru,
            'baru_p' => $hiv_2_non_p_baru,
            'baru_h' => $hiv_2_non_h_baru,
            'baru' => $hiv_2_non_l_baru+$hiv_2_non_p_baru+$hiv_2_non_h_baru,
            'baru_jumlah' => '',
            'lama_l' => $hiv_2_non_l_lama,
            'lama_p' => $hiv_2_non_p_lama,
            'lama_h' => $hiv_2_non_h_lama,
            'lama' => $hiv_2_non_l_lama + $hiv_2_non_p_lama + $hiv_2_non_h_lama,
            'lama_jumlah' => '',
            'total' => '',
        ];

        $data[] = [
            'test' => 'HIV',
            'hasil_1' => 'R2',
            'hasil_2' => 'Indeter',
            'baru_l' => $hiv_2_int_l_baru,
            'baru_p' => $hiv_2_int_p_baru,
            'baru_h' => $hiv_2_int_h_baru,
            'baru' => $hiv_2_int_l_baru+$hiv_2_int_p_baru+$hiv_2_int_h_baru,
            'baru_jumlah' => '',
            'lama_l' => $hiv_2_int_l_lama,
            'lama_p' => $hiv_2_int_p_lama,
            'lama_h' => $hiv_2_int_h_lama,
            'lama' => $hiv_2_int_l_lama + $hiv_2_int_p_lama + $hiv_2_int_h_lama,
            'lama_jumlah' => '',
            'total' => '',
        ];

        $data[] = [
            'test' => 'HIV',
            'hasil_1' => 'R3',
            'hasil_2' => 'Reaktif',
            'baru_l' => $hiv_3_reak_l_baru,
            'baru_p' => $hiv_3_reak_p_baru,
            'baru_h' => $hiv_3_reak_h_baru,
            'baru' => $hiv_3_reak_l_baru+$hiv_3_reak_p_baru+$hiv_3_reak_h_baru,
            'baru_jumlah' => $hiv_3_reak_l_baru+$hiv_3_reak_p_baru+$hiv_3_reak_h_baru+$hiv_3_non_l_baru+$hiv_3_non_p_baru+$hiv_3_non_h_baru+$hiv_3_int_l_baru+$hiv_3_int_p_baru+$hiv_3_int_h_baru,
            'lama_l' => $hiv_3_reak_l_lama,
            'lama_p' => $hiv_3_reak_p_lama,
            'lama_h' => $hiv_3_reak_h_lama,
            'lama' => $hiv_3_reak_l_lama + $hiv_3_reak_p_lama + $hiv_3_reak_h_lama,
            'lama_jumlah' => $hiv_3_reak_l_lama + $hiv_3_reak_p_lama + $hiv_3_reak_h_lama+$hiv_3_non_l_lama + $hiv_3_non_p_lama + $hiv_3_non_h_lama+$hiv_3_int_l_lama + $hiv_3_int_p_lama + $hiv_3_int_h_lama,
            'total' => $hiv_3_reak_l_baru+$hiv_3_reak_p_baru+$hiv_3_reak_h_baru+$hiv_3_non_l_baru+$hiv_3_non_p_baru+$hiv_3_non_h_baru+$hiv_3_int_l_baru+$hiv_3_int_p_baru+$hiv_3_int_h_baru+$hiv_3_reak_l_lama + $hiv_3_reak_p_lama + $hiv_3_reak_h_lama+$hiv_3_non_l_lama + $hiv_3_non_p_lama + $hiv_3_non_h_lama+$hiv_3_int_l_lama + $hiv_3_int_p_lama + $hiv_3_int_h_lama,
        ];

        $data[] = [
            'test' => 'HIV',
            'hasil_1' => 'R3',
            'hasil_2' => 'Non R',
            'baru_l' => $hiv_3_non_l_baru,
            'baru_p' => $hiv_3_non_p_baru,
            'baru_h' => $hiv_3_non_h_baru,
            'baru' => $hiv_3_non_l_baru+$hiv_3_non_p_baru+$hiv_3_non_h_baru,
            'baru_jumlah' => '',
            'lama_l' => $hiv_3_non_l_lama,
            'lama_p' => $hiv_3_non_p_lama,
            'lama_h' => $hiv_3_non_h_lama,
            'lama' => $hiv_3_non_l_lama + $hiv_3_non_p_lama + $hiv_3_non_h_lama,
            'lama_jumlah' => '',
            'total' => '',
        ];

        $data[] = [
            'test' => 'HIV',
            'hasil_1' => 'R3',
            'hasil_2' => 'Indeter',
            'baru_l' => $hiv_3_int_l_baru,
            'baru_p' => $hiv_3_int_p_baru,
            'baru_h' => $hiv_3_int_h_baru,
            'baru' => $hiv_3_int_l_baru+$hiv_3_int_p_baru+$hiv_3_int_h_baru,
            'baru_jumlah' => '',
            'lama_l' => $hiv_3_int_l_lama,
            'lama_p' => $hiv_3_int_p_lama,
            'lama_h' => $hiv_3_int_h_lama,
            'lama' => $hiv_3_int_l_lama + $hiv_3_int_p_lama + $hiv_3_int_h_lama,
            'lama_jumlah' => '',
            'total' => '',
        ];

        $data[] = [
            'test' => 'Sipilis Rapid',
            'hasil_1' => 'Positif',
            'hasil_2' => '',
            'baru_l' => $ims_pos_l_baru,
            'baru_p' => $ims_pos_p_baru,
            'baru_h' => $ims_pos_h_baru,
            'baru' => $ims_pos_l_baru+$ims_pos_p_baru+$ims_pos_h_baru,
            'baru_jumlah' => $ims_neg_l_baru+$ims_neg_p_baru+$ims_neg_h_baru + $ims_pos_l_baru+$ims_pos_p_baru+$ims_pos_h_baru,
            'lama_l' => $ims_pos_l_lama,
            'lama_p' => $ims_pos_p_lama,
            'lama_h' => $ims_pos_h_lama,
            'lama' => $ims_pos_l_lama + $ims_pos_p_lama + $ims_pos_h_lama,
            'lama_jumlah' => $ims_pos_l_lama + $ims_pos_p_lama + $ims_pos_h_lama + $ims_neg_l_lama + $ims_neg_p_lama + $ims_neg_h_lama,
            'total' => $ims_neg_l_baru+$ims_neg_p_baru+$ims_neg_h_baru + $ims_pos_l_baru+$ims_pos_p_baru+$ims_pos_h_baru + $ims_pos_l_lama + $ims_pos_p_lama + $ims_pos_h_lama + $ims_neg_l_lama + $ims_neg_p_lama + $ims_neg_h_lama,
        ];

        $data[] = [
            'test' => 'Sipilis Rapid',
            'hasil_1' => 'Negatif',
            'hasil_2' => '',
            'baru_l' => $ims_neg_l_baru,
            'baru_p' => $ims_neg_p_baru,
            'baru_h' => $ims_neg_h_baru,
            'baru' => $ims_neg_l_baru+$ims_neg_p_baru+$ims_neg_h_baru,
            'baru_jumlah' => '',
            'lama_l' => $ims_neg_l_lama,
            'lama_p' => $ims_neg_p_lama,
            'lama_h' => $ims_neg_h_lama,
            'lama' => $ims_neg_l_lama + $ims_neg_p_lama + $ims_neg_h_lama,
            'lama_jumlah' => '',
            'total' => '',
        ];

        $data[] = [
            'test' => 'RPR',
            'hasil_1' => 'Negatif',
            'hasil_2' => '',
            'baru_l' => $rpr_neg_l_baru,
            'baru_p' => $rpr_neg_p_baru,
            'baru_h' => $rpr_neg_h_baru,
            'baru' => $rpr_neg_l_baru+$rpr_neg_p_baru+$rpr_neg_h_baru,
            'baru_jumlah' => $rpr_neg_l_baru+$rpr_neg_p_baru+$rpr_neg_h_baru+$rpr_pos2_l_baru+$rpr_pos2_p_baru+$rpr_pos2_h_baru+$rpr_pos4_l_baru+$rpr_pos4_p_baru+$rpr_pos4_h_baru+$rpr_pos8_l_baru+$rpr_pos8_p_baru+$rpr_pos8_h_baru+$rpr_pos9_l_baru+$rpr_pos9_p_baru+$rpr_pos9_h_baru,
            'lama_l' => $rpr_neg_l_lama,
            'lama_p' => $rpr_neg_p_lama,
            'lama_h' => $rpr_neg_h_lama,
            'lama' => $rpr_neg_l_lama + $rpr_neg_p_lama + $rpr_neg_h_lama,
            'lama_jumlah' => $rpr_neg_l_lama + $rpr_neg_p_lama + $rpr_neg_h_lama+$rpr_pos2_l_lama + $rpr_pos2_p_lama + $rpr_pos2_h_lama+ $rpr_pos4_l_lama + $rpr_pos4_p_lama + $rpr_pos4_h_lama+$rpr_pos8_l_lama + $rpr_pos8_p_lama + $rpr_pos8_h_lama+$rpr_pos9_l_lama + $rpr_pos9_p_lama + $rpr_pos9_h_lama,
            'total' => $rpr_neg_l_baru+$rpr_neg_p_baru+$rpr_neg_h_baru+$rpr_pos2_l_baru+$rpr_pos2_p_baru+$rpr_pos2_h_baru+$rpr_pos4_l_baru+$rpr_pos4_p_baru+$rpr_pos4_h_baru+$rpr_pos8_l_baru+$rpr_pos8_p_baru+$rpr_pos8_h_baru+$rpr_neg_l_lama + $rpr_neg_p_lama + $rpr_neg_h_lama+$rpr_pos2_l_lama + $rpr_pos2_p_lama + $rpr_pos2_h_lama+ $rpr_pos4_l_lama + $rpr_pos4_p_lama + $rpr_pos4_h_lama+$rpr_pos8_l_lama + $rpr_pos8_p_lama + $rpr_pos8_h_lama+$rpr_pos9_l_lama + $rpr_pos9_p_lama + $rpr_pos9_h_lama+$rpr_pos9_l_baru+$rpr_pos9_p_baru+$rpr_pos9_h_baru,
        ];

        $data[] = [
            'test' => 'RPR',
            'hasil_1' => 'Positif 1/2',
            'hasil_2' => '',
            'baru_l' => $rpr_pos2_l_baru,
            'baru_p' => $rpr_pos2_p_baru,
            'baru_h' => $rpr_pos2_h_baru,
            'baru' => $rpr_pos2_l_baru+$rpr_pos2_p_baru+$rpr_pos2_h_baru,
            'baru_jumlah' => '',
            'lama_l' => $rpr_pos2_l_lama,
            'lama_p' => $rpr_pos2_p_lama,
            'lama_h' => $rpr_pos2_h_lama,
            'lama' => $rpr_pos2_l_lama + $rpr_pos2_p_lama + $rpr_pos2_h_lama,
            'lama_jumlah' => '',
            'total' => '',
        ];

        $data[] = [
            'test' => 'RPR',
            'hasil_1' => 'Positif 1/4',
            'hasil_2' => '',
            'baru_l' => $rpr_pos4_l_baru,
            'baru_p' => $rpr_pos4_p_baru,
            'baru_h' => $rpr_pos4_h_baru,
            'baru' => $rpr_pos4_l_baru+$rpr_pos4_p_baru+$rpr_pos4_h_baru,
            'baru_jumlah' => '',
            'lama_l' => $rpr_pos4_l_lama,
            'lama_p' => $rpr_pos4_p_lama,
            'lama_h' => $rpr_pos4_h_lama,
            'lama' => $rpr_pos4_l_lama + $rpr_pos4_p_lama + $rpr_pos4_h_lama,
            'lama_jumlah' => '',
            'total' => '',
        ];

        $data[] = [
            'test' => 'RPR',
            'hasil_1' => 'Positif 1/8',
            'hasil_2' => '',
            'baru_l' => $rpr_pos8_l_baru,
            'baru_p' => $rpr_pos8_p_baru,
            'baru_h' => $rpr_pos8_h_baru,
            'baru' => $rpr_pos8_l_baru+$rpr_pos8_p_baru+$rpr_pos8_h_baru,
            'baru_jumlah' => '',
            'lama_l' => $rpr_pos8_l_lama,
            'lama_p' => $rpr_pos8_p_lama,
            'lama_h' => $rpr_pos8_h_lama,
            'lama' => $rpr_pos8_l_lama + $rpr_pos8_p_lama + $rpr_pos8_h_lama,
            'lama_jumlah' => '',
            'total' => '',
        ];

        $data[] = [
            'test' => 'RPR',
            'hasil_1' => 'Positif >1/8',
            'hasil_2' => '',
            'baru_l' => $rpr_pos9_l_baru,
            'baru_p' => $rpr_pos9_p_baru,
            'baru_h' => $rpr_pos9_h_baru,
            'baru' => $rpr_pos9_l_baru+$rpr_pos9_p_baru+$rpr_pos9_h_baru,
            'baru_jumlah' => '',
            'lama_l' => $rpr_pos9_l_lama,
            'lama_p' => $rpr_pos9_p_lama,
            'lama_h' => $rpr_pos9_h_lama,
            'lama' => $rpr_pos9_l_lama + $rpr_pos9_p_lama + $rpr_pos9_h_lama,
            'lama_jumlah' => '',
            'total' => '',
        ];

        return view('laporan.imunologi', ['pemeriksaan' => $data]);
    }
}
