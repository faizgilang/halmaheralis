<?php

namespace App\Exports;

use App\Pemeriksaan;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;

class ReagenExport implements FromView, ShouldAutoSize, WithEvents, WithTitle
{
    protected $from;
    protected $to;

    public function __construct($from, $to)
    {
        $this->from = $from;
        $this->to = $to;
    }

    public function title(): string
    {
        return 'Laporan Penggunaan Reagen';
    }

    public function registerEvents(): array
    {
        return [
        AfterSheet::class => function (AfterSheet $event) {
            $styleBody = [
                'borders' => [
                    'allBorders' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
                        'color' => ['argb' => '00000000'],
                    ],
                ],
            ];
            $event->sheet->getDelegate()->getStyle('A1:C9')->applyFromArray($styleBody);
        },
];
    }

    public function view(): View
    {
        $hb = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.hb_stick', '=', '1')
        ->count();
        $gula = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.gula_stick', '=', '1')
        ->count();
        $chol = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.chol_stick', '=', '1')
        ->count();
        $ua = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.ua_stick', '=', '1')
        ->count();
        $ims = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->whereNotNull('pemeriksaans.ims')
        ->count();
        $hbsag = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->whereNotNull('pemeriksaans.hbsag')
        ->count();
        $ims_duplex = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.ims_duplex', '=', '1')
        ->count();
        $hbsag_duplex = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.hbsag_duplex', '=', '1')
        ->count();
        $hiv_1_duplex = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.hiv_1_duplex', '=', '1')
        ->count();
        $hiv_2_duplex = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.hiv_2_duplex', '=', '1')
        ->count();
        $hiv_3_duplex = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.hiv_3_duplex', '=', '1')
        ->count();
        $hiv_1 = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->whereNotNull('pemeriksaans.hiv_1')
        ->count();
        $hiv_2 = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->whereNotNull('pemeriksaans.hiv_2')
        ->count();
        $hiv_3 = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->whereNotNull('pemeriksaans.hiv_3')
        ->count();

        $data[] = [
            'layanan' => 'Stick HB',
            'total' => $hb,
        ];
        $data[] = [
            'layanan' => 'Stick Gula',
            'total' => $gula,
        ];
        $data[] = [
            'layanan' => 'Stick Kolesterol',
            'total' => $chol,
        ];
        $data[] = [
            'layanan' => 'Stick Asam Urat',
            'total' => $ua,
        ];
        $data[] = [
            'layanan' => 'Hbsag',
            'total' => $hbsag+$hbsag_duplex,
        ];
        $data[] = [
            'layanan' => 'IMS',
            'total' => $ims+$ims_duplex,
        ];
        $data[] = [
            'layanan' => 'SDHIV',
            'total' => $hiv_1 + $hiv_1_duplex,
        ];
        $data[] = [
            'layanan' => 'Advanced',
            'total' => $hiv_2 + $hiv_2_duplex,
        ];
        $data[] = [
            'layanan' => 'Vikia',
            'total' => $hiv_3 + $hiv_3_duplex,
        ];

        return view('laporan.Reagen', ['pemeriksaan' => $data]);
    }
}
