<?php

namespace App\Exports;

use App\Pemeriksaan;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;

class BulananExport implements FromView, ShouldAutoSize, WithEvents, WithTitle
{
    protected $from;
    protected $to;

    public function __construct($from, $to)
    {
        $this->from = $from;
        $this->to = $to;
    }

    public function title(): string
    {
        return 'Laporan Bulanan';
    }

    public function registerEvents(): array
    {
        return [
        AfterSheet::class => function (AfterSheet $event) {
            //protein hamil
            $baru_p13 = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_hamil', '=', '1')
        ->where('pemeriksaans.is_first', '=', '1')
        ->where('pemeriksaans.hamil_protein', '=', '1')
        ->where('pasiens.jk', '=', '0')
        ->count();

            $lama_p13 = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_hamil', '=', '1')
        ->where('pemeriksaans.is_first', '=', '0')
        ->where('pemeriksaans.hamil_protein', '=', '1')
        ->where('pasiens.jk', '=', '0')
        ->count();

            $baru_p14 = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_hamil', '=', '1')
        ->where('pemeriksaans.is_first', '=', '1')
        ->where('pemeriksaans.hamil_protein', '=', '0')
        ->where('pasiens.jk', '=', '0')
        ->count();

            $lama_p14 = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_hamil', '=', '1')
        ->where('pemeriksaans.is_first', '=', '0')
        ->where('pemeriksaans.hamil_protein', '=', '0')
        ->where('pasiens.jk', '=', '0')
        ->count();
            $event->sheet->getDelegate()->mergeCells('A6:A7')->setCellValue('A6', 'Jumlah pemeriksaan protein urin pada ibu hamil');
            $event->sheet->getDelegate()->mergeCells('G6:G7')->setCellValue('G6', $baru_p13 + $baru_p14);
            $event->sheet->getDelegate()->mergeCells('L6:L7')->setCellValue('L6', $lama_p13 + $lama_p14);
            $event->sheet->getDelegate()->mergeCells('M6:M7')->setCellValue('M6', $baru_p13 + $baru_p14 + $lama_p13 + $lama_p14);

            // gravindek
            $baru_p15 = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
            ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
            ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
            ->where('pemeriksaans.is_first', '=', '1')
            ->where('pemeriksaans.gravindek', '=', '1')
            ->where('pasiens.jk', '=', '0')
            ->count();

            $lama_p15 = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
            ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
            ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
            ->where('pemeriksaans.is_first', '=', '0')
            ->where('pemeriksaans.gravindek', '=', '1')
            ->where('pasiens.jk', '=', '0')
            ->count();
            $baru_p16 = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
            ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '1')
        ->where('pemeriksaans.gravindek', '=', '0')
        ->where('pasiens.jk', '=', '0')
        ->count();

            $lama_p16 = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '0')
        ->where('pemeriksaans.gravindek', '=', '0')
        ->where('pasiens.jk', '=', '0')
        ->count();
            $event->sheet->getDelegate()->mergeCells('A8:A9')->setCellValue('A8', 'Jumlah pemeriksaan test kehamilan');
            $event->sheet->getDelegate()->mergeCells('G8:G9')->setCellValue('G8', $baru_p15 + $baru_p16);
            $event->sheet->getDelegate()->mergeCells('L8:L9')->setCellValue('L8', $lama_p15 + $lama_p16);
            $event->sheet->getDelegate()->mergeCells('M8:M9')->setCellValue('M8', $baru_p15 + $baru_p16 + $lama_p15 + $lama_p16);

            //Glukosa
            $baru_l8 = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '1')
        ->whereNotNull('pemeriksaans.gula')
        ->where('pasiens.jk', '=', '1')
        ->count();
            $baru_p18 = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_hamil', '=', '1')
        ->where('pemeriksaans.is_first', '=', '1')
        ->whereNotNull('pemeriksaans.gula')
        ->where('pasiens.jk', '=', '0')
        ->count();
            $baru_p28 = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_hamil', '=', '0')
        ->where('pemeriksaans.is_first', '=', '1')
        ->whereNotNull('pemeriksaans.gula')
        ->where('pasiens.jk', '=', '0')
        ->count();
            $lama_l8 = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '0')
        ->whereNotNull('pemeriksaans.gula')
        ->where('pasiens.jk', '=', '1')
        ->count();
            $lama_p18 = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_hamil', '=', '1')
        ->where('pemeriksaans.is_first', '=', '0')
        ->whereNotNull('pemeriksaans.gula')
        ->where('pasiens.jk', '=', '0')
        ->count();
            $lama_p28 = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_hamil', '=', '0')
        ->where('pemeriksaans.is_first', '=', '0')
        ->whereNotNull('pemeriksaans.gula')
        ->where('pasiens.jk', '=', '0')
        ->count();
            //Kolesterol
            $baru_l9 = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '1')
        ->whereNotNull('pemeriksaans.chol')
        ->where('pasiens.jk', '=', '1')
        ->count();
            $baru_p19 = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_hamil', '=', '1')
        ->where('pemeriksaans.is_first', '=', '1')
        ->whereNotNull('pemeriksaans.chol')
        ->where('pasiens.jk', '=', '0')
        ->count();
            $baru_p29 = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_hamil', '=', '0')
        ->where('pemeriksaans.is_first', '=', '1')
        ->whereNotNull('pemeriksaans.chol')
        ->where('pasiens.jk', '=', '0')
        ->count();
            $lama_l9 = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '0')
        ->whereNotNull('pemeriksaans.chol')
        ->where('pasiens.jk', '=', '1')
        ->count();
            $lama_p19 = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_hamil', '=', '1')
        ->where('pemeriksaans.is_first', '=', '0')
        ->whereNotNull('pemeriksaans.chol')
        ->where('pasiens.jk', '=', '0')
        ->count();
            $lama_p29 = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_hamil', '=', '0')
        ->where('pemeriksaans.is_first', '=', '0')
        ->whereNotNull('pemeriksaans.chol')
        ->where('pasiens.jk', '=', '0')
        ->count();
            //Trigliserida
            $baru_l10 = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '1')
        ->whereNotNull('pemeriksaans.trig')
        ->where('pasiens.jk', '=', '1')
        ->count();
            $baru_p110 = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_hamil', '=', '1')
        ->where('pemeriksaans.is_first', '=', '1')
        ->whereNotNull('pemeriksaans.trig')
        ->where('pasiens.jk', '=', '0')
        ->count();
            $baru_p210 = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_hamil', '=', '0')
        ->where('pemeriksaans.is_first', '=', '1')
        ->whereNotNull('pemeriksaans.trig')
        ->where('pasiens.jk', '=', '0')
        ->count();
            $lama_l10 = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '0')
        ->whereNotNull('pemeriksaans.trig')
        ->where('pasiens.jk', '=', '1')
        ->count();
            $lama_p110 = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_hamil', '=', '1')
        ->where('pemeriksaans.is_first', '=', '0')
        ->whereNotNull('pemeriksaans.trig')
        ->where('pasiens.jk', '=', '0')
        ->count();
            $lama_p210 = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_hamil', '=', '0')
        ->where('pemeriksaans.is_first', '=', '0')
        ->whereNotNull('pemeriksaans.trig')
        ->where('pasiens.jk', '=', '0')
        ->count();
            //Asam Urat
            $baru_l11 = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '1')
        ->whereNotNull('pemeriksaans.ua')
        ->where('pasiens.jk', '=', '1')
        ->count();
            $baru_p111 = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_hamil', '=', '1')
        ->where('pemeriksaans.is_first', '=', '1')
        ->whereNotNull('pemeriksaans.ua')
        ->where('pasiens.jk', '=', '0')
        ->count();
            $baru_p211 = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_hamil', '=', '0')
        ->where('pemeriksaans.is_first', '=', '1')
        ->whereNotNull('pemeriksaans.ua')
        ->where('pasiens.jk', '=', '0')
        ->count();
            $lama_l11 = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '0')
        ->whereNotNull('pemeriksaans.ua')
        ->where('pasiens.jk', '=', '1')
        ->count();
            $lama_p111 = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_hamil', '=', '1')
        ->where('pemeriksaans.is_first', '=', '0')
        ->whereNotNull('pemeriksaans.ua')
        ->where('pasiens.jk', '=', '0')
        ->count();
            $lama_p211 = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_hamil', '=', '0')
        ->where('pemeriksaans.is_first', '=', '0')
        ->whereNotNull('pemeriksaans.ua')
        ->where('pasiens.jk', '=', '0')
        ->count();

            $event->sheet->getDelegate()->mergeCells('A11:A14')->setCellValue('A11', 'Jumlah pemeriksaan kimia klinik');
            $event->sheet->getDelegate()->setCellValue('B11', 'Glukosa');
            $event->sheet->getDelegate()->setCellValue('C11', $baru_l8);
            $event->sheet->getDelegate()->setCellValue('D11', $baru_p18);
            $event->sheet->getDelegate()->setCellValue('E11', $baru_p28);
            $event->sheet->getDelegate()->setCellValue('F11', $baru_l8 + $baru_p18 + $baru_p28);
            $event->sheet->getDelegate()->setCellValue('H11', $lama_l8);
            $event->sheet->getDelegate()->setCellValue('I11', $lama_p18);
            $event->sheet->getDelegate()->setCellValue('J11', $lama_p28);
            $event->sheet->getDelegate()->setCellValue('K11', $lama_l8 + $lama_p18 + $lama_p28);

            $event->sheet->getDelegate()->setCellValue('B12', 'Kolesterol');
            $event->sheet->getDelegate()->setCellValue('C12', $baru_l9);
            $event->sheet->getDelegate()->setCellValue('D12', $baru_p19);
            $event->sheet->getDelegate()->setCellValue('E12', $baru_p29);
            $event->sheet->getDelegate()->setCellValue('F12', $baru_l9 + $baru_p19 + $baru_p29);
            $event->sheet->getDelegate()->setCellValue('H12', $lama_l9);
            $event->sheet->getDelegate()->setCellValue('I12', $lama_p19);
            $event->sheet->getDelegate()->setCellValue('J12', $lama_p29);
            $event->sheet->getDelegate()->setCellValue('K12', $lama_l9 + $lama_p19 + $lama_p29);

            $event->sheet->getDelegate()->setCellValue('B13', 'Trigliserida');
            $event->sheet->getDelegate()->setCellValue('C13', $baru_l10);
            $event->sheet->getDelegate()->setCellValue('D13', $baru_p110);
            $event->sheet->getDelegate()->setCellValue('E13', $baru_p210);
            $event->sheet->getDelegate()->setCellValue('F13', $baru_l10 + $baru_p110 + $baru_p210);
            $event->sheet->getDelegate()->setCellValue('H13', $lama_l10);
            $event->sheet->getDelegate()->setCellValue('I13', $lama_p110);
            $event->sheet->getDelegate()->setCellValue('J13', $lama_p210);
            $event->sheet->getDelegate()->setCellValue('K13', $lama_l10 + $lama_p110 + $lama_p210);

            $event->sheet->getDelegate()->setCellValue('B14', 'Asam Urat');
            $event->sheet->getDelegate()->setCellValue('C14', $baru_l11);
            $event->sheet->getDelegate()->setCellValue('D14', $baru_p111);
            $event->sheet->getDelegate()->setCellValue('E14', $baru_p211);
            $event->sheet->getDelegate()->setCellValue('F14', $baru_l11 + $baru_p111 + $baru_p211);
            $event->sheet->getDelegate()->setCellValue('H14', $lama_l11);
            $event->sheet->getDelegate()->setCellValue('I14', $lama_p111);
            $event->sheet->getDelegate()->setCellValue('J14', $lama_p211);
            $event->sheet->getDelegate()->setCellValue('K14', $lama_l11 + $lama_p111 + $lama_p211);

            $event->sheet->getDelegate()->mergeCells('G11:G14')->setCellValue('G11', $baru_l8 + $baru_p18 + $baru_p28 + $baru_l9 + $baru_p19 + $baru_p29 + $baru_l10 + $baru_p110 + $baru_p210 + $baru_l11 + $baru_p111 + $baru_p211);
            $event->sheet->getDelegate()->mergeCells('L11:L14')->setCellValue('L11', $lama_l8 + $lama_p18 + $lama_p28 + $lama_l9 + $lama_p19 + $lama_p29 + $lama_l10 + $lama_p110 + $lama_p210 + $lama_l11 + $lama_p111 + $lama_p211);
            $event->sheet->getDelegate()->mergeCells('M11:M14')->setCellValue('M11', $lama_l8 + $lama_p18 + $lama_p28 + $lama_l9 + $lama_p19 + $lama_p29 + $lama_l10 + $lama_p110 + $lama_p210 + $lama_l11 + $lama_p111 + $lama_p211 + $baru_l8 + $baru_p18 + $baru_p28 + $baru_l9 + $baru_p19 + $baru_p29 + $baru_l10 + $baru_p110 + $baru_p210 + $baru_l11 + $baru_p111 + $baru_p211);
            //lengkap
            $baru_l12 = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '1')
        ->where('pemeriksaans.urinalisa_protein', '=', '1')
        ->where('pasiens.jk', '=', '1')
        ->count();
            $baru_p112 = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_hamil', '=', '1')
        ->where('pemeriksaans.is_first', '=', '1')
        ->where('pemeriksaans.urinalisa_protein', '=', '1')
        ->where('pasiens.jk', '=', '0')
        ->count();
            $baru_p212 = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_hamil', '=', '0')
        ->where('pemeriksaans.is_first', '=', '1')
        ->where('pemeriksaans.urinalisa_protein', '=', '1')
        ->where('pasiens.jk', '=', '0')
        ->count();
            $lama_l12 = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '0')
        ->where('pemeriksaans.urinalisa_protein', '=', '1')
        ->where('pasiens.jk', '=', '1')
        ->count();
            $lama_p112 = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_hamil', '=', '1')
        ->where('pemeriksaans.is_first', '=', '0')
        ->where('pemeriksaans.urinalisa_protein', '=', '1')
        ->where('pasiens.jk', '=', '0')
        ->count();
            $lama_p212 = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_hamil', '=', '0')
        ->where('pemeriksaans.is_first', '=', '0')
        ->where('pemeriksaans.urinalisa_protein', '=', '1')
        ->where('pasiens.jk', '=', '0')
        ->count();
            $event->sheet->getDelegate()->mergeCells('A15:A16')->setCellValue('A15', 'Jumlah pemeriksaan urinalisa');
            $event->sheet->getDelegate()->setCellValue('B15', 'Lengkap');
            $event->sheet->getDelegate()->setCellValue('C15', $baru_l12);
            $event->sheet->getDelegate()->setCellValue('D15', $baru_p112);
            $event->sheet->getDelegate()->setCellValue('E15', $baru_p212);
            $event->sheet->getDelegate()->setCellValue('F15', $baru_l12 + $baru_p112 + $baru_p212);
            $event->sheet->getDelegate()->setCellValue('H15', $lama_l12);
            $event->sheet->getDelegate()->setCellValue('I15', $lama_p112);
            $event->sheet->getDelegate()->setCellValue('J15', $lama_p212);
            $event->sheet->getDelegate()->setCellValue('K15', $lama_l12 + $lama_p112 + $lama_p212);
            //rutin
            $baru_l13 = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '1')
        ->where('pemeriksaans.urinalisa_protein', '=', '0')
        ->where('pasiens.jk', '=', '1')
        ->count();
            $baru_p113 = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_hamil', '=', '1')
        ->where('pemeriksaans.is_first', '=', '1')
        ->where('pemeriksaans.urinalisa_protein', '=', '0')
        ->where('pasiens.jk', '=', '0')
        ->count();
            $baru_p213 = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_hamil', '=', '0')
        ->where('pemeriksaans.is_first', '=', '1')
        ->where('pemeriksaans.urinalisa_protein', '=', '0')
        ->where('pasiens.jk', '=', '0')
        ->count();
            $lama_l13 = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '0')
        ->where('pemeriksaans.urinalisa_protein', '=', '0')
        ->where('pasiens.jk', '=', '1')
        ->count();
            $lama_p113 = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_hamil', '=', '1')
        ->where('pemeriksaans.is_first', '=', '0')
        ->where('pemeriksaans.urinalisa_protein', '=', '0')
        ->where('pasiens.jk', '=', '0')
        ->count();
            $lama_p213 = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_hamil', '=', '0')
        ->where('pemeriksaans.is_first', '=', '0')
        ->where('pemeriksaans.urinalisa_protein', '=', '0')
        ->where('pasiens.jk', '=', '0')
        ->count();
            $event->sheet->getDelegate()->setCellValue('B16', 'Rutin');
            $event->sheet->getDelegate()->setCellValue('C16', $baru_l13);
            $event->sheet->getDelegate()->setCellValue('D16', $baru_p113);
            $event->sheet->getDelegate()->setCellValue('E16', $baru_p213);
            $event->sheet->getDelegate()->setCellValue('F16', $baru_l13 + $baru_p113 + $baru_p213);
            $event->sheet->getDelegate()->setCellValue('H16', $lama_l13);
            $event->sheet->getDelegate()->setCellValue('I16', $lama_p113);
            $event->sheet->getDelegate()->setCellValue('J16', $lama_p213);
            $event->sheet->getDelegate()->setCellValue('K16', $lama_l13 + $lama_p113 + $lama_p213);

            $event->sheet->getDelegate()->mergeCells('G15:G16')->setCellValue('G15', $baru_l12 + $baru_p112 + $baru_p212 + $baru_l13 + $baru_p113 + $baru_p213);
            $event->sheet->getDelegate()->mergeCells('L15:L16')->setCellValue('L15', $lama_l12 + $lama_p112 + $lama_p212 + $lama_l13 + $lama_p113 + $lama_p213);
            $event->sheet->getDelegate()->mergeCells('M15:M16')->setCellValue('M15', $baru_l12 + $baru_p112 + $baru_p212 + $baru_l13 + $baru_p113 + $baru_p213 + $lama_l12 + $lama_p112 + $lama_p212 + $lama_l13 + $lama_p113 + $lama_p213);
            //bta
            $baru_l14 = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
            ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
            ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
            ->where('pemeriksaans.is_first', '=', '1')
            ->where('pemeriksaans.mikro_pengecatan', '=', 'bta')
            ->where('pasiens.jk', '=', '1')
            ->count();
            $baru_p114 = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_hamil', '=', '1')
        ->where('pemeriksaans.is_first', '=', '1')
        ->where('pemeriksaans.mikro_pengecatan', '=', 'bta')
        ->where('pasiens.jk', '=', '0')
        ->count();
            $baru_p214 = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_hamil', '=', '0')
        ->where('pemeriksaans.is_first', '=', '1')
        ->where('pemeriksaans.mikro_pengecatan', '=', 'bta')
        ->where('pasiens.jk', '=', '0')
        ->count();
            $lama_l14 = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '0')
        ->where('pemeriksaans.mikro_pengecatan', '=', 'bta')
        ->where('pasiens.jk', '=', '1')
        ->count();
            $lama_p114 = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_hamil', '=', '1')
        ->where('pemeriksaans.is_first', '=', '0')
        ->where('pemeriksaans.mikro_pengecatan', '=', 'bta')
        ->where('pasiens.jk', '=', '0')
        ->count();
            $lama_p214 = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_hamil', '=', '0')
        ->where('pemeriksaans.is_first', '=', '0')
        ->where('pemeriksaans.mikro_pengecatan', '=', 'bta')
        ->where('pasiens.jk', '=', '0')
        ->count();
            $event->sheet->getDelegate()->mergeCells('A17:A18')->setCellValue('A17', 'Jumlah pemeriksaan mikrobiologi dan parasitologi');
            $event->sheet->getDelegate()->setCellValue('B17', 'BTA');
            $event->sheet->getDelegate()->setCellValue('C17', $baru_l14);
            $event->sheet->getDelegate()->setCellValue('D17', $baru_p114);
            $event->sheet->getDelegate()->setCellValue('E17', $baru_p214);
            $event->sheet->getDelegate()->setCellValue('F17', $baru_l14 + $baru_p114 + $baru_p214);
            $event->sheet->getDelegate()->setCellValue('H17', $lama_l14);
            $event->sheet->getDelegate()->setCellValue('I17', $lama_p114);
            $event->sheet->getDelegate()->setCellValue('J17', $lama_p214);
            $event->sheet->getDelegate()->setCellValue('K17', $lama_l14 + $lama_p114 + $lama_p214);
            //gram
            $baru_l15 = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
            ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
            ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
            ->where('pemeriksaans.is_first', '=', '1')
            ->where('pemeriksaans.mikro_pengecatan', '=', 'gram')
            ->where('pasiens.jk', '=', '1')
            ->count();
            $baru_p115 = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_hamil', '=', '1')
        ->where('pemeriksaans.is_first', '=', '1')
        ->where('pemeriksaans.mikro_pengecatan', '=', 'gram')
        ->where('pasiens.jk', '=', '0')
        ->count();
            $baru_p215 = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_hamil', '=', '0')
        ->where('pemeriksaans.is_first', '=', '1')
        ->where('pemeriksaans.mikro_pengecatan', '=', 'gram')
        ->where('pasiens.jk', '=', '0')
        ->count();
            $lama_l15 = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '0')
        ->where('pemeriksaans.mikro_pengecatan', '=', 'gram')
        ->where('pasiens.jk', '=', '1')
        ->count();
            $lama_p115 = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_hamil', '=', '1')
        ->where('pemeriksaans.is_first', '=', '0')
        ->where('pemeriksaans.mikro_pengecatan', '=', 'gram')
        ->where('pasiens.jk', '=', '0')
        ->count();
            $lama_p215 = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_hamil', '=', '0')
        ->where('pemeriksaans.is_first', '=', '0')
        ->where('pemeriksaans.mikro_pengecatan', '=', 'gram')
        ->where('pasiens.jk', '=', '0')
        ->count();
            $event->sheet->getDelegate()->setCellValue('B18', 'Gram');
            $event->sheet->getDelegate()->setCellValue('C18', $baru_l15);
            $event->sheet->getDelegate()->setCellValue('D18', $baru_p115);
            $event->sheet->getDelegate()->setCellValue('E18', $baru_p215);
            $event->sheet->getDelegate()->setCellValue('F18', $baru_l15 + $baru_p115 + $baru_p215);
            $event->sheet->getDelegate()->setCellValue('H18', $lama_l15);
            $event->sheet->getDelegate()->setCellValue('I18', $lama_p115);
            $event->sheet->getDelegate()->setCellValue('J18', $lama_p215);
            $event->sheet->getDelegate()->setCellValue('K18', $lama_l15 + $lama_p115 + $lama_p215);

            $event->sheet->getDelegate()->mergeCells('G17:G18')->setCellValue('G17', $baru_l14 + $baru_p114 + $baru_p214 + $baru_l15 + $baru_p115 + $baru_p215);
            $event->sheet->getDelegate()->mergeCells('L17:L18')->setCellValue('L17', $lama_l14 + $lama_p114 + $lama_p214 + $lama_l15 + $lama_p115 + $lama_p215);
            $event->sheet->getDelegate()->mergeCells('M17:M18')->setCellValue('M17', $baru_l14 + $baru_p114 + $baru_p214 + $baru_l15 + $baru_p115 + $baru_p215 + $lama_l14 + $lama_p114 + $lama_p214 + $lama_l15 + $lama_p115 + $lama_p215);

            $event->sheet->getDelegate()->getStyle('A1:A18')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
            $event->sheet->getDelegate()->getStyle('A1:M2')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
            $event->sheet->getDelegate()->getStyle('A1:M18')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
            $styleBody = [
                'borders' => [
                    'allBorders' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
                        'color' => ['argb' => '00000000'],
                    ],
                ],
            ];
            $event->sheet->getDelegate()->getStyle('A1:M18')->applyFromArray($styleBody);
        },
];
    }

    public function view(): View
    {
        $baru_l1 = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '1')
        ->where('pasiens.jk', '=', '1')
        ->count();
        $baru_p11 = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_hamil', '=', '1')
        ->where('pemeriksaans.is_first', '=', '1')
        ->where('pasiens.jk', '=', '0')
        ->count();
        $baru_p21 = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_hamil', '=', '0')
        ->where('pemeriksaans.is_first', '=', '1')
        ->where('pasiens.jk', '=', '0')
        ->count();
        $lama_l1 = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '0')
        ->where('pasiens.jk', '=', '1')
        ->count();
        $lama_p11 = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_hamil', '=', '1')
        ->where('pemeriksaans.is_first', '=', '0')
        ->where('pasiens.jk', '=', '0')
        ->count();
        $lama_p21 = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_hamil', '=', '0')
        ->where('pemeriksaans.is_first', '=', '0')
        ->where('pasiens.jk', '=', '0')
        ->count();

        $baru_l2 = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '1')
        ->whereNotNull('pemeriksaans.hb')
        ->where('pasiens.jk', '=', '1')
        ->where(function ($a) {
            $a->orWhereNull('pemeriksaans.hmt')
            ->orWhereNull('pemeriksaans.leukosit')
            ->orWhereNull('pemeriksaans.plt');
        })
        ->count();
        $baru_p12 = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_hamil', '=', '1')
        ->where('pemeriksaans.is_first', '=', '1')
        ->whereNotNull('pemeriksaans.hb')
        ->where('pasiens.jk', '=', '0')
        ->where(function ($a) {
            $a->orWhereNull('pemeriksaans.hmt')
            ->orWhereNull('pemeriksaans.leukosit')
            ->orWhereNull('pemeriksaans.plt');
        })
        ->count();
        $baru_p22 = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_hamil', '=', '0')
        ->where('pemeriksaans.is_first', '=', '1')
        ->whereNotNull('pemeriksaans.hb')
        ->where('pasiens.jk', '=', '0')
        ->where(function ($a) {
            $a->orWhereNull('pemeriksaans.hmt')
            ->orWhereNull('pemeriksaans.leukosit')
            ->orWhereNull('pemeriksaans.plt');
        })
        ->count();
        $lama_l2 = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '0')
        ->whereNotNull('pemeriksaans.hb')
        ->where('pasiens.jk', '=', '1')
        ->where(function ($b) {
            $b->orWhereNull('pemeriksaans.hmt')
            ->orWhereNull('pemeriksaans.leukosit')
            ->orWhereNull('pemeriksaans.plt');
        })
        ->count();
        $lama_p12 = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_hamil', '=', '1')
        ->where('pemeriksaans.is_first', '=', '0')
        ->whereNotNull('pemeriksaans.hb')
        ->where('pasiens.jk', '=', '0')
        ->where(function ($b) {
            $b->orWhereNull('pemeriksaans.hmt')
            ->orWhereNull('pemeriksaans.leukosit')
            ->orWhereNull('pemeriksaans.plt');
        })
        ->count();
        $lama_p22 = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_hamil', '=', '0')
        ->where('pemeriksaans.is_first', '=', '0')
        ->whereNotNull('pemeriksaans.hb')
        ->where('pasiens.jk', '=', '0')
        ->where(function ($b) {
            $b->orWhereNull('pemeriksaans.hmt')
            ->orWhereNull('pemeriksaans.leukosit')
            ->orWhereNull('pemeriksaans.plt');
        })
        ->count();
        $baru_p13 = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_hamil', '=', '1')
        ->where('pemeriksaans.is_first', '=', '1')
        ->where('pemeriksaans.hamil_protein', '=', '1')
        ->where('pasiens.jk', '=', '0')
        ->count();

        $lama_p13 = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_hamil', '=', '1')
        ->where('pemeriksaans.is_first', '=', '0')
        ->where('pemeriksaans.hamil_protein', '=', '1')
        ->where('pasiens.jk', '=', '0')
        ->count();

        $baru_p14 = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_hamil', '=', '1')
        ->where('pemeriksaans.is_first', '=', '1')
        ->where('pemeriksaans.hamil_protein', '=', '0')
        ->where('pasiens.jk', '=', '0')
        ->count();

        $lama_p14 = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_hamil', '=', '1')
        ->where('pemeriksaans.is_first', '=', '0')
        ->where('pemeriksaans.hamil_protein', '=', '0')
        ->where('pasiens.jk', '=', '0')
        ->count();
        $baru_p15 = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '1')
        ->where('pemeriksaans.is_hamil', '=', '1')
        ->where('pemeriksaans.gravindek', '=', '1')
        ->where('pasiens.jk', '=', '0')
        ->count();
        $baru_p25 = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '1')
        ->where('pemeriksaans.is_hamil', '=', '0')
        ->where('pemeriksaans.gravindek', '=', '1')
        ->where('pasiens.jk', '=', '0')
        ->count();

        $lama_p15 = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '0')
        ->where('pemeriksaans.is_hamil', '=', '1')
        ->where('pemeriksaans.gravindek', '=', '1')
        ->where('pasiens.jk', '=', '0')
        ->count();
        $lama_p25 = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '0')
        ->where('pemeriksaans.is_hamil', '=', '0')
        ->where('pemeriksaans.gravindek', '=', '1')
        ->where('pasiens.jk', '=', '0')
        ->count();
        $baru_p16 = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_hamil', '=', '1')
        ->where('pemeriksaans.is_first', '=', '1')
        ->where('pemeriksaans.gravindek', '=', '0')
        ->where('pasiens.jk', '=', '0')
        ->count();
        $baru_p26 = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_hamil', '=', '0')
        ->where('pemeriksaans.is_first', '=', '1')
        ->where('pemeriksaans.gravindek', '=', '0')
        ->where('pasiens.jk', '=', '0')
        ->count();

        $lama_p16 = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_hamil', '=', '1')
        ->where('pemeriksaans.is_first', '=', '0')
        ->where('pemeriksaans.gravindek', '=', '0')
        ->where('pasiens.jk', '=', '0')
        ->count();
        $lama_p26 = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_hamil', '=', '0')
        ->where('pemeriksaans.is_first', '=', '0')
        ->where('pemeriksaans.gravindek', '=', '0')
        ->where('pasiens.jk', '=', '0')
        ->count();

        $baru_l7 = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '1')
        ->where('pasiens.jk', '=', '1')
        ->whereNotNull('pemeriksaans.hb')
        ->WhereNotNull('pemeriksaans.hmt')
        ->WhereNotNull('pemeriksaans.leukosit')
        ->WhereNotNull('pemeriksaans.plt')
        ->count();
        $baru_p17 = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '1')
        ->where('pemeriksaans.is_hamil', '=', '1')
        ->where('pasiens.jk', '=', '0')
        ->whereNotNull('pemeriksaans.hb')
        ->WhereNotNull('pemeriksaans.hmt')
        ->WhereNotNull('pemeriksaans.leukosit')
        ->WhereNotNull('pemeriksaans.plt')
        ->count();
        $baru_p27 = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '1')
        ->where('pemeriksaans.is_hamil', '=', '0')
        ->where('pasiens.jk', '=', '0')
        ->whereNotNull('pemeriksaans.hb')
        ->WhereNotNull('pemeriksaans.hmt')
        ->WhereNotNull('pemeriksaans.leukosit')
        ->WhereNotNull('pemeriksaans.plt')
        ->count();
        $lama_l7 = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '0')
        ->where('pasiens.jk', '=', '1')
        ->whereNotNull('pemeriksaans.hb')
        ->WhereNotNull('pemeriksaans.hmt')
        ->WhereNotNull('pemeriksaans.leukosit')
        ->WhereNotNull('pemeriksaans.plt')
        ->count();
        $lama_p17 = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '0')
        ->where('pemeriksaans.is_hamil', '=', '1')
        ->where('pasiens.jk', '=', '0')
        ->whereNotNull('pemeriksaans.hb')
        ->WhereNotNull('pemeriksaans.hmt')
        ->WhereNotNull('pemeriksaans.leukosit')
        ->WhereNotNull('pemeriksaans.plt')
        ->count();
        $lama_p27 = Pemeriksaan::join('pasiens', 'pasiens.id', '=', 'pemeriksaans.pasien_id')
        ->whereDate('pemeriksaans.tanggal', '>=', $this->from)
        ->whereDate('pemeriksaans.tanggal', '<=', $this->to)
        ->where('pemeriksaans.is_first', '=', '0')
        ->where('pasiens.jk', '=', '0')
        ->where('pemeriksaans.is_hamil', '=', '0')
        ->whereNotNull('pemeriksaans.hb')
        ->WhereNotNull('pemeriksaans.hmt')
        ->WhereNotNull('pemeriksaans.leukosit')
        ->WhereNotNull('pemeriksaans.plt')
        ->count();

        $data[] = [
            'layanan' => 'Jumlah Kunjungan Laboratorium',
            'hasil' => '',
            'baru_l' => $baru_l1,
            'baru_p1' => $baru_p11,
            'baru_p2' => $baru_p21,
            'baru' => '',
            'baru_jumlah' => $baru_l1 + $baru_p11 + $baru_p21,
            'lama_l' => $lama_l1,
            'lama_p1' => $lama_p11,
            'lama_p2' => $lama_p21,
                'lama' => '',
                'lama_jumlah' => $lama_l1 + $lama_p11 + $lama_p21,
                'total' => $baru_l1 + $baru_p11 + $baru_p21 + $lama_l1 + $lama_p11 + $lama_p21,
        ];
        $data[] = [
            'layanan' => 'Jumlah pemeriksaan Hb',
            'hasil' => '',
            'baru_l' => $baru_l2,
            'baru_p1' => $baru_p12,
            'baru_p2' => $baru_p22,
            'baru' => '',
            'baru_jumlah' => $baru_l2 + $baru_p12 + $baru_p22,
            'lama_l' => $lama_l2,
            'lama_p1' => $lama_p12,
            'lama_p2' => $lama_p22,
            'lama' => '',
            'lama_jumlah' => $lama_l2 + $lama_p12 + $lama_p22,
            'total' => $baru_l2 + $baru_p12 + $baru_p22 + $lama_l2 + $lama_p12 + $lama_p22,
        ];
        $data[] = [
            'layanan' => 'Jumlah pemeriksaan Hb pada ibu hamil',
            'hasil' => '',
            'baru_l' => '0',
            'baru_p1' => $baru_p12,
            'baru_p2' => '0',
            'baru' => '',
            'baru_jumlah' => $baru_p12,
            'lama_l' => '0',
            'lama_p1' => $lama_p12,
            'lama_p2' => '0',
            'lama' => '',
            'lama_jumlah' => $lama_p12,
            'total' => $baru_p12 + $lama_p12,
        ];
        $data[] = [
            'layanan' => 'Jumlah pemeriksaan protein urin pada ibu hamil',
            'hasil' => 'Positif',
            'baru_l' => '0',
            'baru_p1' => $baru_p13,
            'baru_p2' => '0',
            'baru' => '',
            'baru_jumlah' => $baru_p13,
            'lama_l' => '0',
            'lama_p1' => $lama_p13,
            'lama_p2' => '0',
            'lama' => '',
            'lama_jumlah' => $lama_p13,
            'total' => '',
        ];

        $data[] = [
            'layanan' => 'Jumlah pemeriksaan protein urin pada ibu hamil',
            'hasil' => 'Negatif',
            'baru_l' => '0',
            'baru_p1' => $baru_p14,
            'baru_p2' => '0',
            'baru' => '',
            'baru_jumlah' => $baru_p14,
            'lama_l' => '0',
            'lama_p1' => $lama_p14,
            'lama_p2' => '0',
            'lama' => '',
            'lama_jumlah' => $lama_p14,
            'total' => '',
        ];
        $data[] = [
            'layanan' => 'Jumlah pemeriksaan test kehamilan',
            'hasil' => 'Positif',
            'baru_l' => '0',
            'baru_p1' => $baru_p15,
            'baru_p2' => $baru_p25,
            'baru' => '',
            'baru_jumlah' => $baru_p15 + $baru_p25,
            'lama_l' => '0',
            'lama_p1' => $lama_p15,
            'lama_p2' => $lama_p25,
            'lama' => '',
            'lama_jumlah' => $lama_p15 + $lama_p25,
            'total' => '',
        ];
        $data[] = [
            'layanan' => 'Jumlah pemeriksaan test kehamilan',
            'hasil' => 'Negatif',
            'baru_l' => '0',
            'baru_p1' => $baru_p16,
            'baru_p2' => $baru_p26,
            'baru' => '',
            'baru_jumlah' => $baru_p16 + $baru_p26,
            'lama_l' => '0',
            'lama_p1' => $lama_p16,
            'lama_p2' => $lama_p26,
            'lama' => '',
            'lama_jumlah' => $lama_p16 + $lama_p26,
            'total' => '',
        ];
        $data[] = [
            'layanan' => 'Jumlah pemeriksaan hematologi',
            'hasil' => '',
            'baru_l' => $baru_l7,
            'baru_p1' => $baru_p17,
            'baru_p2' => $baru_p27,
            'baru' => '',
            'baru_jumlah' => $baru_p17 + $baru_l7 + $baru_p27,
            'lama_l' => $lama_l7,
            'lama_p1' => $lama_p17,
            'lama_p2' => $lama_p27,
            'lama' => '',
            'lama_jumlah' => $lama_p17 + $lama_l7 + $lama_p27,
            'total' => $baru_l7 + $baru_p27 + $lama_l7 + $lama_p27 + $lama_p17 + $baru_p17,
        ];

        return view('laporan.bulanan', ['pemeriksaan' => $data]);
    }
}
