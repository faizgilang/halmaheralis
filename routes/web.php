<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes([
    'register' => false, // Registration Routes...
    'reset' => true, // Password Reset Routes...
    'verify' => true, // Email Verification Routes...
  ]);

Route::post('/detail_pemeriksaan', 'PemeriksaanController@detail');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/laporan', 'LaporanController@index');
Route::post('/laporan', 'LaporanController@laporan');

Route::get('/kelola_pemeriksaan', 'PemeriksaanController@index');
Route::get('/tambah_pemeriksaan', 'PemeriksaanController@create');
Route::post('/tambah_pemeriksaan', 'PemeriksaanController@save');
Route::get('/tambah_pemeriksaan_baru', 'PemeriksaanController@new_create');
Route::post('/tambah_pemeriksaan_baru', 'PemeriksaanController@new_save');
Route::get('/delete_pemeriksaan/{id}', 'PemeriksaanController@delete');
Route::get('/edit_pemeriksaan/{id}', 'PemeriksaanController@edit');
Route::post('/edit_pemeriksaan/{id}', 'PemeriksaanController@update');

Route::get('/kelola_pasien', 'PasienController@index');
Route::get('/kelola_pasien/{id}', 'PasienController@history');
Route::get('/cari_pasien', 'PasienController@search');
Route::post('/cari_pasien', 'PasienController@find');
Route::get('/tambah_pasien', 'PasienController@create');
Route::post('/tambah_pasien', 'PasienController@save');
Route::get('/edit_pasien/{id}', 'PasienController@edit');
Route::post('/edit_pasien/{id}', 'PasienController@update');
