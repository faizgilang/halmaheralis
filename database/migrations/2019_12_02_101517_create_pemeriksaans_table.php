<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePemeriksaansTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('pemeriksaans', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('tanggal');
            $table->unsignedBigInteger('pasien_id');
            $table->enum('is_first', ['0', '1']);
            $table->enum('is_hamil', ['0', '1'])->nullable();
            $table->enum('goldar', ['A', 'B', 'AB', 'O'])->nullable();
            $table->enum('debitur', ['umum', 'bpjs', 'umum ri', 'bpjs ri'])->nullable();
            $table->decimal('hb', 10, 1)->nullable();
            $table->decimal('hmt', 10, 1)->nullable();
            $table->decimal('leukosit', 10, 1)->nullable();
            $table->bigInteger('plt')->nullable();
            $table->enum('widal_o', ['negatif', 'pos 1/80', 'pos 1/160', 'pos 1/320'])->nullable();
            $table->enum('widal_h', ['negatif', 'pos 1/80', 'pos 1/160', 'pos 1/320'])->nullable();
            $table->string('led')->nullable();
            $table->string('diff')->nullable();
            $table->enum('hbsag', ['0', '1'])->nullable();
            $table->bigInteger('gula')->nullable();
            $table->bigInteger('chol')->nullable();
            $table->bigInteger('trig')->nullable();
            $table->decimal('ua', 10, 1)->nullable();
            $table->enum('hamil_protein', ['0', '1'])->nullable();
            $table->enum('hamil_reduksi', ['0', '1', '2'])->nullable();
            $table->string('bilirubin')->nullable();
            $table->enum('gravindek', ['0', '1'])->nullable();
            $table->enum('urinalisa_protein', ['0', '1'])->nullable();
            $table->string('urinalisa_sedimen')->nullable();
            $table->enum('hiv_1', ['non', 'reaktif', 'indeterminate'])->nullable();
            $table->enum('hiv_2', ['non', 'reaktif', 'indeterminate'])->nullable();
            $table->enum('hiv_3', ['non', 'reaktif', 'indeterminate'])->nullable();
            $table->enum('rpr', ['negatif', 'pos 1/2', 'pos 1/4', 'pos 1/8', 'pos >1/8'])->nullable();
            $table->enum('ims', ['0', '1'])->nullable();
            $table->enum('mikro_pengecatan', ['bta', 'gram'])->nullable();
            $table->string('mikro_hasil')->nullable();
            $table->bigInteger('harga')->nullable();
            $table->bigInteger('waktu')->nullable();
            $table->enum('hb_stick', ['0', '1'])->nullable();
            $table->enum('gula_stick', ['0', '1'])->nullable();
            $table->enum('chol_stick', ['0', '1'])->nullable();
            $table->enum('ua_stick', ['0', '1'])->nullable();
            $table->enum('hbsag_duplex', ['0', '1'])->nullable();
            $table->enum('ims_duplex', ['0', '1'])->nullable();
            $table->enum('hiv_1_duplex', ['0', '1'])->nullable();
            $table->enum('hiv_2_duplex', ['0', '1'])->nullable();
            $table->enum('hiv_3_duplex', ['0', '1'])->nullable();
            $table->timestamps();
        });
        Schema::table('pemeriksaans', function (Blueprint $table) {
            $table->foreign('pasien_id')->references('id')->on('pasiens')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('pemeriksaans');
    }
}
