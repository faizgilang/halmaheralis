<?php

use Illuminate\Database\Seeder;
use App\Pasien;

class PemeriksaanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    private $json;

    public function __construct()
    {
        // Decode JSON to Array
        try {
            $this->json = json_decode(file_get_contents(base_path('resources/json/pemeriksaan.json')), true);
        } catch (\Exception $e) {
            error_log('Error on ACLSeeder '.$e->getMessage());
        }
    }

    public function run()
    {
        try {
            DB::beginTransaction();

            $resultArray = [];
            $data = $this->json;
            foreach ($data as $key => $value) {
                $check = Pasien::where('no_cm', $value['no_cm'])->first();
                if (isset($value['tanggal'])) {
                    $rowInsert = [
                    'tanggal' => $value['tanggal'],
                    'pasien_id' => $check->id,
                    'is_first' => $value['is_first'],
                    'is_hamil' => $value['is_hamil'],
                    'goldar' => isset($value['goldar']) ? $value['goldar'] : null,
                    'debitur' => strtolower($value['debitur']),
                    'hb' => isset($value['hb']) ? $value['hb'] : null,
                    'hmt' => isset($value['hmt']) ? $value['hmt'] : null,
                    'leukosit' => isset($value['leukosit']) ? $value['leukosit'] : null,
                    'plt' => isset($value['plt']) ? $value['plt'] : null,
                    'widal_o' => isset($value['widal_o']) ? strtolower($value['widal_o']) : null,
                    'widal_h' => isset($value['widal_h']) ? strtolower($value['widal_h']) : null,
                    'led' => isset($value['led']) ? $value['led'] : null,
                    'diff' => isset($value['diff']) ? $value['diff'] : null,
                    'hbsag' => isset($value['hbsag']) ? strtolower($value['hbsag']) == 'negatif' ? '0' : '1' : null,
                    'gula' => isset($value['gula']) ? $value['gula'] : null,
                    'chol' => isset($value['chol']) ? $value['chol'] : null,
                    'trig' => isset($value['trig']) ? $value['trig'] : null,
                    'ua' => isset($value['ua']) ? $value['ua'] : null,
                    'hamil_protein' => isset($value['hamil_protein']) ? strtolower($value['hamil_protein']) == 'negatif' ? '0' : '1' : null,
                    'hamil_reduksi' => isset($value['hamil_reduksi']) ? strtolower($value['hamil_reduksi']) == 'negatif' ? '0' : strtolower($value['hamil_reduksi']) == 'positif 1' ? '1' : '0' : null,
                    'bilirubin' => isset($value['bilirubin']) ? $value['bilirubin'] : null,
                    'gravindek' => isset($value['gravindek']) ? strtolower($value['gravindek']) == 'negatif' ? '0' : '1' : null,
                    'urinalisa_protein' => isset($value['urinalisa_protein']) ? strtolower($value['urinalisa_protein']) == 'negatif' ? '0' : '1' : null,
                    'urinalisa_sedimen' => isset($value['urinalisa_sedimen']) ? $value['urinalisa_sedimen'] : null,
                    'hiv_1' => isset($value['hiv_1']) ? strtolower($value['hiv_1']) : null,
                    'hiv_2' => isset($value['hiv_2']) ? strtolower($value['hiv_2']) : null,
                    'hiv_3' => isset($value['hiv_3']) ? strtolower($value['hiv_3']) : null,
                    'rpr' => isset($value['rpr']) ? strtolower($value['rpr']) : null,
                    'ims' => isset($value['ims']) ? strtolower($value['ims']) == 'negatif' ? '0' : '1' : null,
                    'mikro_pengecatan' => isset($value['mikrobiologi_pengecatan']) ? strtolower($value['mikrobiologi_pengecatan']) : null,
                    'mikro_hasil' => isset($value['mikrobiologi_hasil']) ? strtolower($value['mikrobiologi_hasil']) : null,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ];
                }

                $resultArray[] = $rowInsert;
            }
            App\Pemeriksaan::insert($resultArray);

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }
}
