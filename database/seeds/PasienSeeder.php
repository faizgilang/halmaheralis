<?php

use Illuminate\Database\Seeder;

class PasienSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    private $json;

    public function __construct()
    {
        // Decode JSON to Array
        try {
            $this->json = json_decode(file_get_contents(base_path('resources/json/pasienOrig.json')), true);
        } catch (\Exception $e) {
            error_log('Error on ACLSeeder '.$e->getMessage());
        }
    }

    public function unique_multi_array($array, $key)
    {
        $temp_array = array();
        $i = 0;
        $key_array = array();

        foreach ($array as $val) {
            if (!in_array($val[$key], $key_array)) {
                $key_array[$i] = $val[$key];
                $temp_array[$i] = $val;
            }
            ++$i;
        }

        return $temp_array;
    }

    public function run()
    {
        try {
            DB::beginTransaction();

            $resultArray = [];
            $data = $this->json;
            foreach ($data as $key => $value) {
                if (isset($value['nama'])) {
                    $rowInsert = [
                    'nama' => $value['nama'],
                    'no_cm' => $value['no_cm'],
                    'alamat' => $value['alamat'],
                    'tanggal_lahir' => $value['tanggal_lahir'],
                    'jk' => $value['jk'],
                    'is_new' => isset($value['is_new']) ? $value['is_new'] == '1' ? '1' : '0' : '0',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ];
                }

                $resultArray[] = $rowInsert;
            }
            App\Pasien::insert($this->unique_multi_array($resultArray, 'no_cm'));

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }
}
