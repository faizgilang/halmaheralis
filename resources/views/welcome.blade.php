<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Halmahera	</title>
<meta name="description" content="">
<meta name="author" content="">

<!-- Favicons
    ================================================== -->
<link rel="shortcut icon" href="" type="image/x-icon">
<link rel="apple-touch-icon" href="img/apple-touch-icon.png">
<link rel="apple-touch-icon" sizes="72x72" href="img/apple-touch-icon-72x72.png">
<link rel="apple-touch-icon" sizes="114x114" href="img/apple-touch-icon-114x114.png">

<!-- Bootstrap -->
<link rel="stylesheet" type="text/css"  href="{{asset('homepage/css/bootstrap.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('homepage/fonts/font-awesome/css/font-awesome.css')}}">

<!-- Stylesheet
    ================================================== -->
<link rel="stylesheet" type="text/css"  href="{{asset('homepage/css/style.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('homepage/css/prettyPhoto.css')}}">
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,700,800,600,300" rel="stylesheet" type="text/css">
 <style>

        .panel-transparent {
            background: none;
        }

        .panel-transparent .panel-heading{
            background: rgba(122, 130, 136, 0)!important;
        }

        .panel-transparent .panel-body{
            background: rgba(46, 51, 56, 0.6)!important;
        }
        .x {
           font-family: "Rockwell ", "Rockwell Bold", monospace;
           
        }	
       </style>
<script type="text/javascript" src="{{asset('homepage/js/modernizr.custom.js')}}"></script>

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">
<!-- Navigation -->
<nav id="menu" class="navbar navbar-default navbar-fixed-top">
  <div class="container"> 
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
    </div>
    
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul align="right" class="col-lg-offset-8 col-lg-4">
	  @if (Auth::guest())
         <form method="POST" action="{{ route('login') }}">
                        @csrf
                        <div class="form-group">
                            <div class="col-lg-5">
                                <input style="background-color: transparent; color:#fff" id="email" placeholder="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-lg-5">
                                <input style="background-color: transparent; color:#fff" id="password" placeholder="Password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                       

                        <div class="form-group">
                            <div class="col-lg-2">
                                <button type="submit" class="btn btn-success">
                                    {{ __('Login') }}
                                </button>	
                            </div>
                        </div>
        </form>
					
       @else
       <a class="nav-link" href="/home">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Dashboard</span></a>
       @endif
      </ul>
    </div>
    <!-- /.navbar-collapse --> 
  </div>
  <!-- /.container-fluid --> 
</nav>
<!-- Header -->
<header id="header">
  <div class="intro text-center">
    <div class="overlay">
      <div class="container">
        <div class="row">
          <div class="intro-text">
            <p>Selamat Datang</p>
            <h2>Sistem Informasi Laboratorium  <span class="brand"></span></h2>
            <p>Puskesmas Halmahera</p>
        </div>
      </div>
    </div>
  </div>
</header>
<!-- Services Section -->

<script type="text/javascript" src="{{asset('homepage/js/jquery.1.11.1.js')}}"></script> 
<script type="text/javascript" src="{{asset('homepage/js/bootstrap.js')}}"></script> 
<script type="text/javascript" src="{{asset('homepage/js/SmoothScroll.js')}}"></script> 
<script type="text/javascript" src="{{asset('homepage/js/jquery.prettyPhoto.js')}}"></script> 
<script type="text/javascript" src="{{asset('homepage/js/jquery.isotope.js')}}"></script> 
<script type="text/javascript" src="{{asset('homepage/js/jqBootstrapValidation.js')}}"></script>  
<script type="text/javascript" src="{{asset('homepage/js/main.js')}}"></script>
</body>
</html>