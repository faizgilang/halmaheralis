@extends('layouts.theme')

@section('content')
<div class="card">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Tambah Data Pasien</h6>
    </div>
  <div class="card-body">
     <form method="post" action="{{ url()->current() }}">
     {{ csrf_field() }}
  <div class=" field_wrapper">
    <div class="col-lg-12">
        <div class="form-group">
          <label for="nama">Nama:</label>
          <input type="text" required name="nama" class="form-control" id="name">
          {!! $errors-> first('nama', '<strong class="text-danger">:message</strong>')!!}
        </div>
        <div class="form-group">
          <label for="no_cm">Nomer CM:</label>
          <input type="text" required name="no_cm" class="form-control" id="no_cm">
          {!! $errors-> first('no_cm', '<strong class="text-danger">:message</strong>')!!}
        </div>
        <div class="form-group">
          <label for="status">Status Pasien:</label>
          <select class="form-control" required name="status" id="status">
                        <option value="">Jenis Pasien</option>
                        <option value="1">Baru</option>
                        <option value="0">Lama</option>
          </select>
        </div>
        <div class="form-group">
          <label for="nik">NIK:</label>
          <input type="text" name="nik" class="form-control" id="nik">
        </div>
        <div class="form-group">
          <label for="bpjs">Nomer BPJS:</label>
          <input type="text" name="bpjs" class="form-control" id="bpjs">
        </div>
        <div class="form-group">
          <label for="jk">Jenis Kelamin:</label>
          <select class="form-control" required name="jk" id="jk">
                        <option value="">Pilih Jenis Kelamin</option>
                        <option value="1">Laki-Laki</option>
                        <option value="0">Perempuan</option>
          </select>
        </div>
        <div class="form-group">
          <label for="alamat">Alamat:</label>
          <input type="text" name="alamat" class="form-control" id="alamat">
        </div>
        <div class="form-group">
          <label for="tempat">Tempat Lahir:</label>
          <input type="text" name="tempat" class="form-control" id="tanggal">
        </div>
        <div class="form-group">
          <label for="tanggal">Tanggal Lahir:</label>
          <input type="date" name="tanggal" class="form-control" id="tanggal">
        </div>
        <div class="form-group">
          <label for="golongan">Jenis Golongan Darah:</label>
          <select class="form-control" name="golongan" id="golongan">
                        <option value="">Pilih Golongan Darah</option>
                        <option value="A">A</option>
                        <option value="B">B</option>
                        <option value="O">O</option>
                        <option value="AB">AB</option>
          </select>
        </div>
      </div>
  </div>
  <div class="col-md-12">
  <!-- <div class="row">
    <div class="offset-10 col-md-2">
      <a href="javascript:void(0);" class="add_button float-right btn btn-info btn-xs" title="Add field">Add items</a>
    </div> -->
  <!-- </div> -->
        <button type="submit" class="btn btn-primary">Submit</button>
      </form>
  </div>
  </div>
</div>
@endsection

@section('script')

<script>
$(document).ready(function(){
  var maxField = 999; //Input fields increment limitation
  var addButton = $('.add_button'); //Add button selector
  var wrapper = $('.field_wrapper'); //Input field wrapper
  var fieldHTML = '<div class="col-lg-12"><a href="javascript:void(0);" class="remove_button float-right btn btn-danger btn-xs"  title="Remove field">Remove<i class="fa fa-trash"></i></a><div class="form-group"><label for="email">Email address:</label><input type="number" min="13" class="form-control" id="email"></div><div class="form-group"><label for="pwd">Password:</label><input type="password" class="form-control" id="pwd"></div</div>'; //New input field html
  var x = 1; //Initial field counter is 1

  $(addButton).click(function(){ //Once add button is clicked
    if(x < maxField){ //Check maximum number of input fields
      x++; //Increment field counter
      $(wrapper).append(fieldHTML); // Add field html
    }
  });
  $(wrapper).on('click', '.remove_button', function(e){ //Once remove button is clicked
    e.preventDefault();
    $(this).parent('div').remove(); //Remove field html
    x--; //Decrement field counter
  });
});
</script>@endsection