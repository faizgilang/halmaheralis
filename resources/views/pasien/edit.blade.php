@extends('layouts.theme')

@section('content')
<div class="card">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Edit Data Pasien</h6>
    </div>
  <div class="card-body">
     <form method="post" action="{{ url()->current() }}">
     {{ csrf_field() }}
  <div class=" field_wrapper">
    <div class="col-lg-12">
        <div class="form-group">
          <label for="nama">Nama:</label>
          <input type="text" required name="nama" class="form-control" id="name" value="{{$pasien->nama}}">
          {!! $errors-> first('nama', '<strong class="text-danger">:message</strong>')!!}
        </div>
        <div class="form-group">
          <label for="no_cm">Nomer CM:</label>
          <input type="text" required name="no_cm" class="form-control" id="no_cm" value="{{$pasien->no_cm}}">
          {!! $errors-> first('no_cm', '<strong class="text-danger">:message</strong>')!!}
        </div>
        <div class="form-group">
          <label for="bpjs">Nomer BPJS:</label>
          <input type="text" name="bpjs" class="form-control" id="bpjs" value="{{$pasien->bpjs}}">
        </div>
        <div class="form-group">
          <label for="status">Status Pasien:</label>
          <select class="form-control" required name="status" id="status">
                        <option value="{{$pasien->is_new}}">{{$pasien->is_new == 1 ? "Baru" : "Lama" }}</option>
                        <option value="1">Baru</option>
                        <option value="0">Lama</option>
          </select>
        </div>
        <div class="form-group">
          <label for="nik">NIK:</label>
          <input type="text" name="nik" class="form-control" id="nik" value="{{$pasien->nik}}">
        </div>
        <div class="form-group">
          <label for="jk">Jenis Kelamin:</label>
          <select class="form-control" name="jk" id="jk">
                        @if($pasien->jk === "1")
                        <option value="1">Laki-Laki</option>
                        <option value="0">Perempuan</option>
                        @else
                        <option value="0">Perempuan</option>
                        <option value="1">Laki-Laki</option>
                        @endif
          </select>
        </div>
        <div class="form-group">
          <label for="alamat">Alamat:</label>
          <input type="text" name="alamat" class="form-control" id="alamat" value="{{$pasien->alamat}}"> 
        </div>
        <div class="form-group">
          <label for="tempat">Tempat Lahir:</label>
          <input type="text" name="tempat" class="form-control" id="tempat" value="{{$pasien->tempat_lahir}}">
        </div>
        <div class="form-group">
          <label for="tanggal">Tanggal Lahir:</label>
          <input type="date" name="tanggal" class="form-control" id="tanggal" value="{{$pasien->tanggal_lahir}}">
        </div>
        <div class="form-group">
          <label for="golongan">Jenis Golongan Darah:</label>
          <select class="form-control" name="golongan" id="golongan">
                        <option value="{{$pasien->goldar}}">{{$pasien->goldar}}</option>
                        <option value="A">A</option>
                        <option value="B">B</option>
                        <option value="O">O</option>
                        <option value="AB">AB</option>
          </select>
        </div>
    </div>
  </div>
  <div class="col-md-12">
        <button type="submit" class="btn btn-primary">Submit</button>
      </form>
  </div>
  </div>
</div>
@endsection
