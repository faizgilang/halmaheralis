@extends('layouts.theme')

@section('content')
<div class="card">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Riwayat Data Pemeriksaan</h6>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>No. CM</th>
                      <th>Nama</th>
                      <th>Debitur</th>
                      <th>Tanggal</th>
                      <th>Opsi</th>
                    </tr>
                  </thead>
                  <tbody>
                  @foreach($pemeriksaan as $index => $pemeriksaan)
                  <tr>
                  <td>{{$index+1}}</td>
                  <td>{{$pemeriksaan->pasien->no_cm}}</td>
                  <td>{{$pemeriksaan->pasien->nama}}</td>
                  <td>{{strtoupper($pemeriksaan->debitur)}}</td>
                  <td>{{$pemeriksaan->tanggal}}</td>
                  <td>
                  <a href="/edit_pemeriksaan/{{$pemeriksaan->id}}"><i class="fas fa-fw fa-edit"></i></a>
                  <a href="#exampleModal{{$index}}" data-toggle="modal" data-target="#exampleModal{{$index}}" date-id="{{ $index }}"><i class="fas fa-fw fa-file-signature"></i></a>
                  <a  href="{{ url('delete_pemeriksaan/' . $pemeriksaan->id) }}" onclick="return confirm('Yakin Data ini akan dihapus?')">
                    <i class="fa fa-fw fa-trash"></i>
                        </a>
                  </td>
                  </tr>
                  <div class="modal fade" id="exampleModal{{$index}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Hasil Pemeriksaan</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body row">
                      
                          @if($pemeriksaan->goldar)
                          <div class="col-lg-5">Golongan Darah</div>
                          <div class="col-lg-1">:</div>
                          <div class="col-lg-3">{{$pemeriksaan->goldar}}</div>
                          @endif

                          @if($pemeriksaan->hb)
                          <div class="col-lg-5">HB</div>
                          <div class="col-lg-1">:</div>
                          <div class="col-lg-3">{{$pemeriksaan->hb}}</div>
                          @endif

                          @if($pemeriksaan->hmt)
                          <div class="col-lg-5">HMT</div>
                          <div class="col-lg-1">:</div>
                          <div class="col-lg-3">{{$pemeriksaan->hmt}}</div>
                          @endif

                          @if($pemeriksaan->leukosit)
                          <div class="col-lg-5">Leukosit</div>
                          <div class="col-lg-1">:</div>
                          <div class="col-lg-3">{{$pemeriksaan->leukosit}}</div>
                          @endif

                          @if($pemeriksaan->plt)
                          <div class="col-lg-5">PLT</div>
                          <div class="col-lg-1">:</div>
                          <div class="col-lg-3">{{$pemeriksaan->plt}}</div>
                          @endif

                          @if($pemeriksaan->widal_o)
                          <div class="col-lg-5">Widal O</div>
                          <div class="col-lg-1">:</div>
                          <div class="col-lg-3">{{$pemeriksaan->widal_o}}</div>
                          @endif

                          @if($pemeriksaan->widal_h)
                          <div class="col-lg-5">Widal H</div>
                          <div class="col-lg-1">:</div>
                          <div class="col-lg-3">{{$pemeriksaan->widal_h}}</div>
                          @endif

                          @if($pemeriksaan->led)
                          <div class="col-lg-5">LED</div>
                          <div class="col-lg-1">:</div>
                          <div class="col-lg-3">{{$pemeriksaan->led}}</div>
                          @endif

                          @if($pemeriksaan->diff)
                          <div class="col-lg-5">Diff</div>
                          <div class="col-lg-1">:</div>
                          <div class="col-lg-3">{{$pemeriksaan->diff}}</div>
                          @endif
                          
                          @if($pemeriksaan->hbsag)
                          <div class="col-lg-5">HBSAG</div>
                          <div class="col-lg-1">:</div>
                          <div class="col-lg-3">{{isset($pemeriksaan->hbsag) ? $pemeriksaan->hbsag == "0" ? 'Negatif' : 'Positif' : ''}}</div>
                          @endif
                         
                          @if($pemeriksaan->gula)
                          <div class="col-lg-5">Gula</div>
                          <div class="col-lg-1">:</div>
                          <div class="col-lg-3">{{$pemeriksaan->gula}}</div>
                          @endif

                          @if($pemeriksaan->chol)
                          <div class="col-lg-5">Kolesterol</div>
                          <div class="col-lg-1">:</div>
                          <div class="col-lg-3">{{$pemeriksaan->chol}}</div>
                          @endif

                          @if($pemeriksaan->trig)
                          <div class="col-lg-5">Trig</div>
                          <div class="col-lg-1">:</div>
                          <div class="col-lg-3">{{$pemeriksaan->trig}}</div>
                          @endif

                          @if($pemeriksaan->ua)
                          <div class="col-lg-5">UA</div>
                          <div class="col-lg-1">:</div>
                          <div class="col-lg-3">{{$pemeriksaan->ua}}</div>
                          @endif

                          @if($pemeriksaan->hamil_protein)
                          <div class="col-lg-5">Hamil Protein</div>
                          <div class="col-lg-1">:</div>
                          <div class="col-lg-3">{{isset($pemeriksaan->hamil_protein) ? $pemeriksaan->hamil_protein == "0" ? 'Negatif' : 'Positif' : ''}}</div>
                          @endif
                          
                          @if($pemeriksaan->hamil_reduksi)
                          <div class="col-lg-5">Hamil Reduksi</div>
                          <div class="col-lg-1">:</div>
                          <div class="col-lg-3">{{isset($pemeriksaan->hamil_reduksi) ? $pemeriksaan->hamil_reduksi == "0" ? 'Negatif' : $pemeriksaan->hamil_reduksi == "1" ? 'Positif 1' : 'Positif 2' : ''}}</div>
                          @endif
                          
                          @if($pemeriksaan->urinalisa_protein)
                          <div class="col-lg-5">Urinalisa Protein</div>
                          <div class="col-lg-1">:</div>
                          <div class="col-lg-3">{{isset($pemeriksaan->urinalisa_protein) ? $pemeriksaan->urinalisa_protein == '0' ? "Rutin" : "Lengkap" : ''}}</div>
                          @endif
                          
                          @if($pemeriksaan->urinalisa_sedimen)
                          <div class="col-lg-5">Urinalisa Sedimen</div>
                          <div class="col-lg-1">:</div>
                          <div class="col-lg-3">{{$pemeriksaan->urinalisa_sedimen }}</div>
                          @endif
                          
                          @if($pemeriksaan->hiv_1)
                          <div class="col-lg-5">HIV R1</div>
                          <div class="col-lg-1">:</div>
                          <div class="col-lg-3">{{$pemeriksaan->hiv_1 }}</div>
                          @endif
                          
                          @if($pemeriksaan->hiv_2)
                          <div class="col-lg-5">HIV R2</div>
                          <div class="col-lg-1">:</div>
                          <div class="col-lg-3">{{$pemeriksaan->hiv_2 }}</div>
                          @endif
                          
                          @if($pemeriksaan->hiv_3)
                          <div class="col-lg-5">HIV R3</div>
                          <div class="col-lg-1">:</div>
                          <div class="col-lg-3">{{$pemeriksaan->hiv_3 }}</div>
                          @endif
                          
                          @if($pemeriksaan->rpr)
                          <div class="col-lg-5">RPR</div>
                          <div class="col-lg-1">:</div>
                          <div class="col-lg-3">{{$pemeriksaan->rpr}}</div>
                          @endif
                          
                          @if($pemeriksaan->ims)
                          <div class="col-lg-5">IMS</div>
                          <div class="col-lg-1">:</div>
                          <div class="col-lg-3">{{isset($pemeriksaan->ims) ? $pemeriksaan->ims == "0" ? "Negatif" : 'Positif' : ''}}</div>
                          @endif
                          
                          @if($pemeriksaan->mikro_pengecatan)
                          <div class="col-lg-5">Mikrobiologi Pengecatan</div>
                          <div class="col-lg-1">:</div>
                          <div class="col-lg-3">{{$pemeriksaan->mikro_pengecatan }}</div>
                          @endif
                          
                          @if(isset(json_decode($pemeriksaan->mikro_hasil)->pmn) || isset(json_decode($pemeriksaan->mikro_hasil)->diplo) || isset(json_decode($pemeriksaan->mikro_hasil)->candi) || isset(json_decode($pemeriksaan->mikro_hasil)->tric))
                          <div class="col-lg-5">Mikrobiologi Hasil</div>
                          <div class="col-lg-1">:</div>
                            @if(isset(json_decode($pemeriksaan->mikro_hasil)->pmn))
                            <div class="offset-6 col-lg-3">{{json_decode($pemeriksaan->mikro_hasil)->pmn == '1' ? "PMN +" : "PMN -" }}</div>
                            @endif
                            @if(isset(json_decode($pemeriksaan->mikro_hasil)->diplo))
                            <div class="offset-6 col-lg-3">{{json_decode($pemeriksaan->mikro_hasil)->diplo == '1' ? "Diplo +" : "Diplo -"}}</div>
                            @endif
                            @if(isset(json_decode($pemeriksaan->mikro_hasil)->candi))
                            <div class="offset-6 col-lg-3">{{json_decode($pemeriksaan->mikro_hasil)->candi == '1' ? "Candida +" : "Candida -"}}</div>
                            @endif
                            @if(isset(json_decode($pemeriksaan->mikro_hasil)->tric))
                            <div class="offset-6 col-lg-3">{{json_decode($pemeriksaan->mikro_hasil)->tric == '1' ? "Trich +" : "Trich -"}}</div>
                            @endif
                          @endif
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                      </div>
                    </div>
                  </div>
                </div>
                  @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          

@endsection

@section('script')
@endsection