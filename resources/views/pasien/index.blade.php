@extends('layouts.theme')

@section('content')
<div class="card">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Kelola Data Pasien</h6>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>No. CM</th>
                      <th>Nama</th>
                      <th>Jenis Kelamin</th>
                      <th>Opsi</th>
                    </tr>
                  </thead>
                  <tbody>
                  @foreach($pasien as $index => $pasien)
                  <tr>
                  <td>{{$index+1}}</td>
                  <td>{{$pasien->no_cm}}</td>
                  <td>{{$pasien->nama}}</td>
                  <td>{{$pasien->jk == "1" ? "Laki-Laki" : "Perempuan"}}</td>
                  <td>
                  <a href="/edit_pasien/{{$pasien->id}}"><i class="fas fa-fw fa-edit"></i></a>
                  <a href="/kelola_pasien/{{$pasien->id}}"><i class="fas fa-fw fa-file-medical"></i></a>
                  </td>
                  </tr>
                  @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
@endsection

@section('script')
@endsection