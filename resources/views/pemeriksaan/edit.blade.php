@extends('layouts.theme')

@section('content')
<div class="card">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Edit Data Pemeriksaan</h6>
    </div>
  <div class="card-body">
     <form method="post" action="{{ url()->current() }}">
     {{ csrf_field() }}
  <div class=" field_wrapper">
    <div class="col-lg-12">
    <div class="form-group row">
          <label class="col-lg-2 col-form-label" for="date">Tanggal</label>
          <div class="col-lg-10">
          <input type="date" name="date" value="{{$pemeriksaan->tanggal}}" class="form-control" id="date">
          </div>
        </div>
    <div class="form-group row">
          <div class="offset-2 col-lg-10">
          <input type="checkbox" name="is_hamil" class="" {{$pemeriksaan->is_hamil == "1" ? 'checked' : null}} id="is_hamil"> Hamil
          </div>
        </div>
    <div class="form-group row">
          <label class="col-lg-2 col-form-label" for="golongan">Golongan Darah:</label>
          <div class="col-lg-10">
          <select class="form-control" name="golongan" id="golongan">
                        <option value="{{$pemeriksaan->goldar}}">{{strtoupper($pemeriksaan->goldar)}}</option>
                        <option value="A">A</option>
                        <option value="B">B</option>
                        <option value="O">O</option>
                        <option value="AB">AB</option>
          </select>
          </div>
        </div>
        <div class="form-group row">
          <label class="col-lg-2 col-form-label" for="debitur">Debitur:</label>
          <div class="col-lg-10">
          <select class="form-control" name="debitur" id="debitur">
                        <option value="{{$pemeriksaan->debitur}}">{{strtoupper($pemeriksaan->debitur)}}</option>
                        <option value="umum">Umum</option>
                        <option value="bpjs">BPJS</option>
                        <option value="umum ri">Umum Ri</option>
                        <option value="bpjs ri">BPJS RI</option>
          </select>
          </div>
        </div>
        <div class="form-group row">
          <label class="col-lg-2 col-form-label" for="hb">HB:</label>
          <div class="col-lg-10">
          <input type="number" step="0.1" value="{{$pemeriksaan->hb}}" name="hb" class="form-control" id="hb">
          </div>
        </div>
        <div class="form-group row">
          <div class="offset-2 col-lg-10">
          <input type="checkbox" name="hb_stick"  class="" {{$pemeriksaan->hb_stick == "1" ? 'checked' : null}} id="hb_stick"> Stick
          </div>
        </div>
        <div class="form-group row">
          <label class="col-lg-2 col-form-label" for="hmt">HMT:</label>
          <div class="col-lg-10">
          <input type="number" step="0.1" value="{{$pemeriksaan->hmt}}" name="hmt" class="form-control" id="hmt">
          </div>
        </div>
        <div class="form-group row">
          <label class="col-lg-2 col-form-label" for="leukosit">Leukosit:</label>
          <div class="col-lg-10">
          <input type="number" step="0.1" value="{{$pemeriksaan->leukosit}}" name="leukosit" class="form-control" id="leukosit">
          </div>
        </div>
        <div class="form-group row">
          <label class="col-lg-2 col-form-label" for="plt">PLT:</label>
          <div class="col-lg-10">
          <input type="number" step="1" value="{{$pemeriksaan->plt}}" name="plt" class="form-control" id="plt">
          </div>
        </div>
          <label class="col-form-label">Widal:</label>
        <div class="form-group">
          <div class="form-group row">
            <label align="right" class="col-lg-2 col-form-label" for="widal_o">O:</label>
              <div class="col-lg-10">
                <select class="form-control"  name="widal_o" id="widal_o">
                              <option value="{{$pemeriksaan->widal_o}}">{{isset($pemeriksaan->widal_o) ? $pemeriksaan->widal_o : 'Widal O'}}</option>
                              <option value="negatif">Negatif</option>
                              <option value="pos 1/80">Positif 1/80</option>
                              <option value="pos 1/160">Positif 1/160</option>
                              <option value="pos 1/320">Positif 1/320</option>
                </select>
            </div>
          </div>
          <div class="form-group row">
            <label align="right" class="col-lg-2 col-form-label" for="widal_h">H:</label>
              <div class="col-lg-10">
                <select class="form-control" name="widal_h" id="widal_h">
                              <option value="{{$pemeriksaan->widal_h}}">{{isset($pemeriksaan->widal_h) ? $pemeriksaan->widal_h : 'Widal H'}}</option>
                              <option value="negatif">Negatif</option>
                              <option value="pos 1/80">Positif 1/80</option>
                              <option value="pos 1/160">Positif 1/160</option>
                              <option value="pos 1/320">Positif 1/320</option>
                </select>
            </div>
          </div>
        </div>
        <div class="form-group row">
          <label class="col-lg-2 col-form-label" for="led">LED:</label>
          <div class="col-lg-10">
          <input type="text" value="{{$pemeriksaan->led}}" name="led" class="form-control" id="led">
          </div>
        </div>
        <div class="form-group row">
          <label class="col-lg-2 col-form-label" for="diff">Diff:</label>
          <div class="col-lg-10">
          <input type="text" value="{{$pemeriksaan->diff}}" name="diff" class="form-control" id="diff">
          </div>
        </div>
        <div class="form-group row">
          <label class="col-lg-2 col-form-label" for="hbsag">HBSAG:</label>
          <div class="col-lg-10">
          <select class="form-control"  name="hbsag" id="hbsag">
                        <option value="{{$pemeriksaan->hbsag}}">{{isset($pemeriksaan->hbsag) ? $pemeriksaan->hbsag == "0" ? 'Negatif' : 'Positif' : 'HBSAG'}}</option>
                        <option value="0">Negatif</option>
                        <option value="1">Positif</option>
          </select>
          </div>
        </div>
        <div class="form-group row">
          <label class="col-lg-2 col-form-label" for="gula">Gula:</label>
          <div class="col-lg-10">
          <input type="number" step="1" value="{{$pemeriksaan->gula}}" name="gula" class="form-control" id="gula">
          </div>
        </div>
        <div class="form-group row">
          <div class="offset-2 col-lg-10">
          <input type="checkbox" name="gula_stick" class="" {{$pemeriksaan->gula_stick == "1" ? 'checked' : null}} id="gula_stick"> Stick
          </div>
        </div>
        <div class="form-group row">
          <label class="col-lg-2 col-form-label" for="chol">Kolesterol:</label>
          <div class="col-lg-10">
          <input type="number" step="1" value="{{$pemeriksaan->chol}}" name="chol" class="form-control" id="chol">
          </div>
        </div>
        <div class="form-group row">
          <div class="offset-2 col-lg-10">
          <input type="checkbox" name="chol_stick" class="" {{$pemeriksaan->chol_stick == "1" ? 'checked' : null}} id="chol_stick"> Stick
          </div>
        </div>
        <div class="form-group row">
          <label  class="col-lg-2 col-form-label" for="trig">Trig:</label>
          <div class="col-lg-10">
          <input type="number" step="1" value="{{$pemeriksaan->trig}}" name="trig" class="form-control" id="trig">
          </div>
        </div>
        <div class="form-group row">
          <label class="col-lg-2 col-form-label" for="ua">UA:</label>
          <div class="col-lg-10">
          <input type="number" step="0.1" value="{{$pemeriksaan->ua}}" name="ua" class="form-control" id="ua">
          </div>
        </div>
        <div class="form-group row">
          <div class="offset-2 col-lg-10">
          <input type="checkbox" name="ua_stick" class="" {{$pemeriksaan->ua_stick == "1" ? 'checked' : null}} id="ua_stick"> Stick
          </div>
        </div>
        <label class="col-form-label">Kehamilan:</label>
        <div class="form-group">
          <div class="form-group row">
            <label align="right" class="col-lg-2 col-form-label" for="hamil_protein">Protein:</label>
              <div class="col-lg-10">
                <select class="form-control"  name="hamil_protein" id="hamil_protein">
                              <option value="{{$pemeriksaan->hamil_protein}}">{{isset($pemeriksaan->hamil_protein) ? $pemeriksaan->hamil_protein == "0" ? 'Negatif' : 'Positif' : 'Protein'}}</option>
                              <option value="0">Negatif</option>
                              <option value="1">Positif</option>
                </select>
            </div>
          </div>
          <div class="form-group row">
            <label align="right" class="col-lg-2 col-form-label" for="hamil_reduksi">Reduksi:</label>
              <div class="col-lg-10">
                <select class="form-control"  name="hamil_reduksi" id="hamil_reduksi">
                              <option value="{{$pemeriksaan->hamil_reduksi}}">{{isset($pemeriksaan->hamil_reduksi) ? $pemeriksaan->hamil_reduksi == "0" ? 'Negatif' : $pemeriksaan->hamil_reduksi == "1" ? 'Positif 1' : 'Positif 2' : 'Reduksi'}}</option>
                              <option value="0">Negatif</option>
                              <option value="1">Positif 1</option>
                              <option value="2">Positif 2</option>
                </select>
            </div>
          </div>
        </div>
        <div class="form-group row">
          <label class="col-lg-2 col-form-label" for="bilirubin">Bilirubin:</label>
          <div class="col-lg-10">
          <input type="text" value="{{$pemeriksaan->bilirubin}}" name="bilirubin" class="form-control" id="bilirubin">
          </div>
        </div>
        <div class="form-group row">
          <label class="col-lg-2 col-form-label" for="gravindek">Gravindek:</label>
          <div class="col-lg-10">
          <select class="form-control"  name="gravindek" id="gravindek">
                        <option value="{{$pemeriksaan->gravindek}}">{{isset($pemeriksaan->gravindek) ? $pemeriksaan->gravindek == 1 ? "Positif" : "Negatif" : 'Gravindek'}}</option>
                        <option value="0">Negatif</option>
                        <option value="1">Positif</option>
          </select>
          </div>
        </div>
        <label class="col-form-label">Urinalisa:</label>
        <div class="form-group">
          <div class="form-group row">
            <label align="right" class="col-lg-2 col-form-label" for="urinalisa_protein">Protein:</label>
              <div class="col-lg-10">
                <select class="form-control"  name="urinalisa_protein" id="urinalisa_protein">
                              <option value="{{$pemeriksaan->urinalisa_protein}}">{{isset($pemeriksaan->urinalisa_protein) ? $pemeriksaan->urinalisa_protein == '0' ? "Rutin" : "Lengkap" : 'Urinalisa'}}</option>
                              <option value="0">Rutin</option>
                              <option value="1">Lengkap</option>
                </select>
            </div>
          </div>
          <div class="form-group row">
            <label align="right" class="col-lg-2 col-form-label" for="urinalisa_sedimen">Sedimen:</label>
              <div class="col-lg-10">
                <input type="text" class="form-control" value="{{$pemeriksaan->urinalisa_sedimen}}" name="urinalisa_sedimen" id="urinalisa_sedimen"></input>
            </div>
          </div>
        </div>
        <label class="col-form-label">HIV:</label>
        <div class="form-group">
          <div class="form-group row">
            <label align="right" class="col-lg-2 col-form-label" for="hiv_1">R1:</label>
              <div class="col-lg-10">
                <select class="form-control" name="hiv_1" id="hiv_1">
                              <option value="{{$pemeriksaan->hiv_1}}">{{$pemeriksaan->hiv_1}}</option>
                              <option value="non">Non Reaktif</option>
                              <option value="reaktif">Reaktif</option>
                              <option value="indeterminate">Indeterminate</option>
                </select>
            </div>
          </div>
          <div class="form-group row">
          <div class="offset-2 col-lg-10">
          <input type="checkbox" name="hiv_1_duplex" class="" {{$pemeriksaan->hiv_1_duplex == "1" ? 'checked' : null}} id="hiv_1_duplex"> Duplex
          </div>
        </div>
          <div class="form-group row">
            <label align="right" class="col-lg-2 col-form-label" for="hiv_2">R2:</label>
              <div class="col-lg-10">
                <select class="form-control" name="hiv_2" id="hiv_2">
                              <option value="{{$pemeriksaan->hiv_2}}">{{$pemeriksaan->hiv_2}}</option>
                              <option value="non">Non Reaktif</option>
                              <option value="reaktif">Reaktif</option>
                              <option value="indeterminate">Indeterminate</option>
                </select>
            </div>
          </div>
          <div class="form-group row">
          <div class="offset-2 col-lg-10">
          <input type="checkbox" name="hiv_2_duplex" class="" {{$pemeriksaan->hiv_2_duplex == "1" ? 'checked' : null}} id="hiv_2_duplex"> Duplex
          </div>
        </div>
          <div class="form-group row">
            <label align="right" class="col-lg-2 col-form-label" for="hiv_3">R3:</label>
              <div class="col-lg-10">
                <select class="form-control" name="hiv_3" id="hiv_3">
                              <option value="{{$pemeriksaan->hiv_3}}">{{$pemeriksaan->hiv_3}}</option>
                              <option value="non">Non Reaktif</option>
                              <option value="reaktif">Reaktif</option>
                              <option value="indeterminate">Indeterminate</option>
                </select>
            </div>
          </div>
          <div class="form-group row">
          <div class="offset-2 col-lg-10">
          <input type="checkbox" name="hiv_3_duplex" class="" {{$pemeriksaan->hiv_3_duplex == "1" ? 'checked' : null}} id="hiv_3_duplex"> Duplex
          </div>
        </div>
        </div>
        <div class="form-group row">
          <label class="col-lg-2 col-form-label" for="rpr">RPR:</label>
          <div class="col-lg-10">
          <select class="form-control" name="rpr" id="rpr">
             <option value="{{$pemeriksaan->rpr}}">{{$pemeriksaan->rpr}}</option>
             <option value="negatif">Negatif</option>
             <option value="pos 1/2">Positif 1/2</option>
             <option value="pos 1/4">Positif 1/4</option>
             <option value="pos 1/8">Positif 1/8</option>
             <option value="pos >1/8">Positif >1/8</option>
          </select>
          </div>
        </div>
         <div class="form-group row">
          <label class="col-lg-2 col-form-label" for="ims">IMS:</label>
          <div class="col-lg-10">
          <select class="form-control" name="ims" id="ims">
                        <option value="{{$pemeriksaan->ims}}">{{isset($pemeriksaan->ims) ? $pemeriksaan->ims == "0" ? "Negatif" : 'Positif' : 'IMS'}}</option>
                        <option value="0">Negatif</option>
                        <option value="1">Positif</option>
          </select>
          </div>
        </div>
        <label class="col-form-label">Mikrobiologi:</label>
        <div class="form-group">
          <div class="form-group row">
            <label align="right" class="col-lg-2 col-form-label" for="mikro_pengecatan">Pengecatan:</label>
              <div class="col-lg-10">
                <select class="form-control" name="mikro_pengecatan" id="mikro_pengecatan">
                              <option value="{{$pemeriksaan->mikro_pengecatan}}">{{$pemeriksaan->mikro_pengecatan}}</option>
                              <option value="bta">BTA</option>
                              <option value="gram">Gram</option>
                </select>
            </div>
          </div>
          <div class="form-group row">
            <label align="right" class="col-lg-2 col-form-label" for="mikro_hasil">Hasil:</label>
            <div class="col-lg-2">
                <select class="form-control" name="pmn" id="pmn">
                              <option value="{{json_decode($pemeriksaan->mikro_hasil)->pmn}}">{{ isset(json_decode($pemeriksaan->mikro_hasil)->pmn) ? json_decode($pemeriksaan->mikro_hasil)->pmn == '1' ? 'PMN +' : 'PMN -' : 'PMN'}}</option>
                              <option value="1">PMN +</option>
                              <option value="0">PMN -</option>
                </select>
            </div>
            <div class="col-lg-2">
                <select class="form-control" name="diplo" id="diplo">
                              <option value="{{json_decode($pemeriksaan->mikro_hasil)->diplo}}">{{ isset(json_decode($pemeriksaan->mikro_hasil)->diplo) ? json_decode($pemeriksaan->mikro_hasil)->diplo == '1' ? 'Diplo +' : 'Diplo -' : 'Diplo'}}</option>
                              <option value="1">Diplo +</option>
                              <option value="0">Diplo -</option>
                </select>
            </div>
            <div class="col-lg-2">
                <select class="form-control" name="candi" id="candi">
                              <option value="{{json_decode($pemeriksaan->mikro_hasil)->candi}}">{{ isset(json_decode($pemeriksaan->mikro_hasil)->candi) ? json_decode($pemeriksaan->mikro_hasil)->candi == '1' ? 'Candida +' : 'Candida -' : 'Candida'}}</option>
                              <option value="1">Candida +</option>
                              <option value="0">Candida -</option>
                </select>
            </div>
            <div class="col-lg-2">
                <select class="form-control" name="tric" id="tric">
                              <option value="{{json_decode($pemeriksaan->mikro_hasil)->tric}}">{{ isset(json_decode($pemeriksaan->mikro_hasil)->tric) ? json_decode($pemeriksaan->mikro_hasil)->tric == '1' ? 'Tricho +' : 'Tricho -' : 'Tricho'}}</option>
                              <option value="1">Tric +</option>
                              <option value="0">Tric -</option>
                </select>
            </div>
          </div>
        </div>
        <div class="form-group row">
          <label class="col-lg-2 col-form-label" for="harga">Biaya:</label>
          <div class="col-lg-10">
          <input type="number" value="{{$pemeriksaan->harga}}" name="harga" class="form-control" id="harga">
          </div>
        </div>
        <div class="form-group row">
          <label class="col-lg-2 col-form-label" for="waktu">Waktu:</label>
          <div class="col-lg-10">
          <input type="text" value="{{$pemeriksaan->waktu}}" name="waktu" class="form-control" id="waktu">
        </div>
        </div>

    </div>
  </div>
  <div class="col-md-12">
  <div class="row">
    <!-- <div class="offset-10 col-md-2">
      <a href="javascript:void(0);" class="add_button float-right btn btn-info btn-xs" title="Add field">Add items</a>
    </div> -->
  </div>
        <button type="submit" class="btn btn-primary">Submit</button>
      </form>
  </div>
  </div>
</div>
@endsection

@section('script')

<script>
$(document).ready(function(){
  var maxField = 999; //Input fields increment limitation
  var addButton = $('.add_button'); //Add button selector
  var wrapper = $('.field_wrapper'); //Input field wrapper
  var fieldHTML = '<div class="col-lg-12"><a href="javascript:void(0);" class="remove_button float-right btn btn-danger btn-xs"  title="Remove field">Remove<i class="fa fa-trash"></i></a><div class="form-group"><label align="right" class="col-lg-2 col-form-label" for="email">Email address:</label><input type="number" min="13" class="form-control" id="email"></div><div class="form-group"><label align="right" class="col-lg-2 col-form-label" for="pwd">Password:</label><input type="password" class="form-control" id="pwd"></div</div>'; //New input field html
  var x = 1; //Initial field counter is 1

  $(addButton).click(function(){ //Once add button is clicked
    if(x < maxField){ //Check maximum number of input fields
      x++; //Increment field counter
      $(wrapper).append(fieldHTML); // Add field html
    }
  });
  $(wrapper).on('click', '.remove_button', function(e){ //Once remove button is clicked
    e.preventDefault();
    $(this).parent('div').remove(); //Remove field html
    x--; //Decrement field counter
  });
});
</script>@endsection