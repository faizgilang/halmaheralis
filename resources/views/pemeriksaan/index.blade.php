@extends('layouts.theme')

@section('content')
<div class="card">
  <div class="card-header py-3">
    <h6 class="m-0 font-weight-bold text-primary">Kelola Data Pemeriksaan</h6>
  </div>
  <div class="card-body">
    <div class="table-responsive">
      <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <thead>
          <tr>
            <th>No</th>
            <th>No. CM</th>
            <th>Nama</th>
            <th>Debitur</th>
            <th>Tanggal</th>
            <th>Opsi</th>
          </tr>
        </thead>
        <tbody>
          @foreach($pemeriksaan as $index => $pemeriksaan)
          <tr>
            <td>{{$index+1}}</td>
            <td>{{$pemeriksaan->pasien->no_cm}}</td>
            <td>{{$pemeriksaan->pasien->nama}}</td>
            <td>{{strtoupper($pemeriksaan->debitur)}}</td>
            <td>{{$pemeriksaan->tanggal}}</td>
            <td>
              <button class="btn btn-sm"><a href="/edit_pemeriksaan/{{$pemeriksaan->id}}"><i class="fas fa-fw fa-edit"></i></a></button>
              <button class="btn btn-sm" onclick="generate({{$pemeriksaan->id}})"><i class="fas fa-fw fa-file-signature"></i></button>
              <button class="btn btn-sm"><a href="{{ url('delete_pemeriksaan/' . $pemeriksaan->id) }}" onclick="return confirm('Yakin Data ini akan dihapus?')">
                  <i class="fa fa-fw fa-trash"></i>
                </a>
              </button>
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
</div>
<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Hasil Pemeriksaan</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body row" id="modal_body">

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


@endsection
@section('script')
<script>
  function generate(id) {
    jQuery.ajax({
      url: "/detail_pemeriksaan",
      method: 'post',
      data: {
        "_token": "{{ csrf_token() }}",
        'id': id
      },
      success: function(response) {
        mappingFaktor(response);
        $('#modal').modal('show');
      }
    });
  };

  function mappingFaktor(response) {
    var obj = JSON.parse(response)
    $('#modal_body').empty();

    if (obj.detail.goldar != null) {
      $('#modal_body').append(
        '<div class="col-lg-5">Golongan Darah</div>' +
        '<div class="col-lg-1">:</div>' +
        '<div class="col-lg-3">' + obj.detail.goldar + '</div>'
      );
    }

    if (obj.detail.hb != null) {
      $('#modal_body').append(
        '<div class="col-lg-5">HB</div>' +
        '<div class="col-lg-1">:</div>' +
        '<div class="col-lg-3">' + obj.detail.hb + '</div>'
      );
    }

    if (obj.detail.hmt != null) {
      $('#modal_body').append(
        '<div class="col-lg-5">HMT</div>' +
        '<div class="col-lg-1">:</div>' +
        '<div class="col-lg-3">' + obj.detail.hmt + '</div>'
      );
    }

    if (obj.detail.leukosit != null) {
      $('#modal_body').append(
        '<div class="col-lg-5">Leukosit</div>' +
        '<div class="col-lg-1">:</div>' +
        '<div class="col-lg-3">' + obj.detail.leukosit + '</div>'
      );
    }

    if (obj.detail.plt != null) {
      $('#modal_body').append(
        '<div class="col-lg-5">PLT</div>' +
        '<div class="col-lg-1">:</div>' +
        '<div class="col-lg-3">' + obj.detail.plt + '</div>'
      );
    }

    if (obj.detail.widal_o != null) {
      $('#modal_body').append(
        '<div class="col-lg-5">Widal O</div>' +
        '<div class="col-lg-1">:</div>' +
        '<div class="col-lg-3">' + obj.detail.widal_o + '</div>'
      );
    }

    if (obj.detail.widal_h != null) {
      $('#modal_body').append(
        '<div class="col-lg-5">Widal H</div>' +
        '<div class="col-lg-1">:</div>' +
        '<div class="col-lg-3">' + obj.detail.widal_h + '</div>'
      );
    }

    if (obj.detail.led != null) {
      $('#modal_body').append(
        '<div class="col-lg-5">LED</div>' +
        '<div class="col-lg-1">:</div>' +
        '<div class="col-lg-3">' + obj.detail.led + '</div>'
      );
    }

    if (obj.detail.diff != null) {
      $('#modal_body').append(
        '<div class="col-lg-5">Diff</div>' +
        '<div class="col-lg-1">:</div>' +
        '<div class="col-lg-3">' + obj.detail.diff + '</div>'
      );
    }

    if (obj.detail.hbsag != null) {
      if (obj.detail.hbsag == "0") {
        var hbsag = "Negatif";
      } else {
        var hbsag = "Positif";
      }
      $('#modal_body').append(
        '<div class="col-lg-5">HBSAG</div>' +
        '<div class="col-lg-1">:</div>' +
        '<div class="col-lg-3">' + hbsag + '</div>'
      );
    }

    if (obj.detail.gula != null) {
      $('#modal_body').append(
        '<div class="col-lg-5">Gula</div>' +
        '<div class="col-lg-1">:</div>' +
        '<div class="col-lg-3">' + obj.detail.gula + '</div>'
      );
    }

    if (obj.detail.chol != null) {
      $('#modal_body').append(
        '<div class="col-lg-5">Kolesterol</div>' +
        '<div class="col-lg-1">:</div>' +
        '<div class="col-lg-3">' + obj.detail.chol + '</div>'
      );
    }

    if (obj.detail.trig != null) {
      $('#modal_body').append(
        '<div class="col-lg-5">Trig</div>' +
        '<div class="col-lg-1">:</div>' +
        '<div class="col-lg-3">' + obj.detail.trig + '</div>'
      );
    }

    if (obj.detail.ua != null) {
      $('#modal_body').append(
        '<div class="col-lg-5">UA</div>' +
        '<div class="col-lg-1">:</div>' +
        '<div class="col-lg-3">' + obj.detail.ua + '</div>'
      );
    }

    if (obj.detail.hamil_protein != null) {
      if (obj.detail.hamil_protein == "0") {
        var h_protein = "Negatif";
      } else {
        var h_protein = "Positif";
      }
      $('#modal_body').append(
        '<div class="col-lg-5">Hamil Potein</div>' +
        '<div class="col-lg-1">:</div>' +
        '<div class="col-lg-3">' + h_protein + '</div>'
      );
    }

    if (obj.detail.hamil_reduksi != null) {
      if (obj.detail.h_reduksi == "0") {
        var h_reduksi = "Negatif";
      } else if (obj.detail.hamil_reduksi == "1") {
        var h_reduksi = "Positif 1";
      } else {
        var h_reduksi = "Positif 2";
      }
      $('#modal_body').append(
        '<div class="col-lg-5">Hamil Reduksi</div>' +
        '<div class="col-lg-1">:</div>' +
        '<div class="col-lg-3">' + h_reduksi + '</div>'
      );
    }

    if (obj.detail.bilirubin != null) {
      $('#modal_body').append(
        '<div class="col-lg-5">Bilirubin</div>' +
        '<div class="col-lg-1">:</div>' +
        '<div class="col-lg-3">' + obj.detail.bilirubin + '</div>'
      );
    }

    if (obj.detail.gravindek != null) {
      if (obj.detail.gravindek == "0") {
        var gravindek = "Negatif";
      } else {
        var gravindek = "Positif";
      }
      $('#modal_body').append(
        '<div class="col-lg-5">Gravindek</div>' +
        '<div class="col-lg-1">:</div>' +
        '<div class="col-lg-3">' + gravindek + '</div>'
      );
    }

    if (obj.detail.urinalisa_protein != null) {
      if (obj.detail.urinalisa_protein == "0") {
        var u_protein = "Rutin";
      } else {
        var u_protein = "Lengkap";
      }
      $('#modal_body').append(
        '<div class="col-lg-5">Urinalisa Protein</div>' +
        '<div class="col-lg-1">:</div>' +
        '<div class="col-lg-3">' + u_protein + '</div>'
      );
    }

    if (obj.detail.urinalisa_sedimen != null) {
      $('#modal_body').append(
        '<div class="col-lg-5">Urinalisa Sedimen</div>' +
        '<div class="col-lg-1">:</div>' +
        '<div class="col-lg-3">' + obj.detail.urinalisa_sedimen + '</div>'
      );
    }

    if (obj.detail.hiv_1 != null) {
      if (obj.detail.hiv_1 == "non") {
        var hiv_1 = "Non Reaktif";
      } else if(obj.detail.hiv_1 == "reaktif") {
        var hiv_1 = "Reaktif";
      } else {
        var hiv_1 = "Indeterminate";
      }
      $('#modal_body').append(
        '<div class="col-lg-5">HIV R1</div>' +
        '<div class="col-lg-1">:</div>' +
        '<div class="col-lg-3">' + hiv_1 + '</div>'
      );
    }

    if (obj.detail.hiv_2 != null) {
      if (obj.detail.hiv_2 == "non") {
        var hiv_2 = "Non Reaktif";
      } else if(obj.detail.hiv_2 == "reaktif") {
        var hiv_2 = "Reaktif";
      } else {
        var hiv_2 = "Indeterminate";
      }
      $('#modal_body').append(
        '<div class="col-lg-5">HIV R2</div>' +
        '<div class="col-lg-1">:</div>' +
        '<div class="col-lg-3">' + hiv_2 + '</div>'
      );
    }

    if (obj.detail.hiv_3 != null) {
      if (obj.detail.hiv_3 == "non") {
        var hiv_3 = "Non Reaktif";
      } else if(obj.detail.hiv_3 == "reaktif") {
        var hiv_3 = "Reaktif";
      } else {
        var hiv_3 = "Indeterminate";
      }
      $('#modal_body').append(
        '<div class="col-lg-5">HIV R3</div>' +
        '<div class="col-lg-1">:</div>' +
        '<div class="col-lg-3">' + hiv_3 + '</div>'
      );
    }

    if (obj.detail.rpr != null) {
      $('#modal_body').append(
        '<div class="col-lg-5">RPR</div>' +
        '<div class="col-lg-1">:</div>' +
        '<div class="col-lg-3">' + obj.detail.rpr + '</div>'
      );
    }

    if (obj.detail.ims != null) {
      if (obj.detail.ims == "0") {
        var ims = "Negatif";
      } else {
        var ims = "Positif";
      }
      $('#modal_body').append(
        '<div class="col-lg-5">IMS</div>' +
        '<div class="col-lg-1">:</div>' +
        '<div class="col-lg-3">' + ims + '</div>'
      );
    }

    if (obj.detail.mikro_pengecatan != null) {
      $('#modal_body').append(
        '<div class="col-lg-5">Mikrobiologi Pengecatan</div>' +
        '<div class="col-lg-1">:</div>' +
        '<div class="col-lg-3">' + obj.detail.mikro_pengecatan + '</div>'
      );
    }

    if (JSON.parse(obj.detail.mikro_hasil).pmn != null || JSON.parse(obj.detail.mikro_hasil).diplo != null || JSON.parse(obj.detail.mikro_hasil).candi != null || JSON.parse(obj.detail.mikro_hasil).tric != null) {
      $('#modal_body').append(
        '<div class="col-lg-5">Mikrobiologi Hasil</div>' +
        '<div class="col-lg-1">:</div>'
      );
    }
    if (JSON.parse(obj.detail.mikro_hasil).pmn != null) {
      if (JSON.parse(obj.detail.mikro_hasil).pmn == "0") {
        var pmn = "PMN -";
      } else {
        var pmn = "PMN +";
      }
      $('#modal_body').append(
        '<div class="col-lg-3">' + pmn + '</div>'
      );
    }
    if (JSON.parse(obj.detail.mikro_hasil).diplo != null) {
      if (JSON.parse(obj.detail.mikro_hasil).diplo == "0") {
        var diplo = "Diplo -";
      } else {
        var diplo = "Diplo +";
      }
      $('#modal_body').append(
        '<div class="offset-6 col-lg-3">' + diplo + '</div>'
      );
    }
    if (JSON.parse(obj.detail.mikro_hasil).candi != null) {
      if (JSON.parse(obj.detail.mikro_hasil).candi == "0") {
        var candi = "Candida -";
      } else {
        var candi = "Candida +";
      }
      $('#modal_body').append(
        '<div class="offset-6 col-lg-3">' + candi + '</div>'
      );
    }
    if (JSON.parse(obj.detail.mikro_hasil).tric != null) {
      if (JSON.parse(obj.detail.mikro_hasil).tric == "0") {
        var tric = "Trich +";
      } else {
        var tric = "Trich -";
      }
      $('#modal_body').append(
        '<div class="offset-6 col-lg-3">' + tric + '</div>'
      );
    }

  }
</script>
@endsection