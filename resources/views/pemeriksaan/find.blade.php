@extends('layouts.theme')

@section('content')
<div class="card">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Cari Data Pasien</h6>
    </div>
  <div class="card-body">
     <form method="post" action="{{ url()->current() }}">
     {{ csrf_field() }}
  <div class=" field_wrapper">
    <div class="col-lg-12">
        <div class="form-group row">
          <label class="col-lg-2 col-form-label" for="no_cm">No CM:</label>
          <div class="col-lg-10">
          <select required class="form-control" name="no_cm" id="no_cm">
                        <option value="">Pilih Pasien</option>
                        @foreach($pasien as $pasien)
                        <option value="{{$pasien->no_cm}}">{{$pasien->no_cm.' - '.$pasien->nama}}</option>
                        @endforeach
          </select>
          </div>
        </div>
    </div>
  </div>
  <div class="col-md-12">
  <div class="row">
    <!-- <div class="offset-10 col-md-2">
      <a href="javascript:void(0);" class="add_button float-right btn btn-info btn-xs" title="Add field">Add items</a>
    </div> -->
  </div>
        <button type="submit" class="btn btn-primary">Submit</button>
      </form>
  </div>
  </div>
</div>
@endsection

@section('script')
<script>
$(document).ready(function() {
    $('#no_cm').select2({
      tags: true
    });
});
</script>
@endsection