<table>
  <tbody>
    <tr>
         <td>Nama Instansi</td>
         <td>Puskesmas Halmahera</td>
    </tr>
    <tr>
         <td>Bulan</td>
         <td>Puskesmas Halmahera</td>
    </tr>
    <tr>
         <td>Tahun</td>
         <td>Puskesmas Halmahera</td>
    </tr>
  </tbody>
</table>


                <table>
                <thead>
                    <tr>
                      <th rowspan="2">No</th>
                      <th rowspan="2">Tanggal</th>
                      <th rowspan="2">Nama</th>
                      <th rowspan="2">Umur</th>
                      <th rowspan="2">JK</th>
                      <th rowspan="2">Debitur</th>
                      <th rowspan="2">Alamat</th>
                      <th rowspan="2">No. Register</th>
                      <th rowspan="1">Hb</th>
                      <th rowspan="1">Hmt</th>
                      <th rowspan="1">Leukosit</th>
                      <th rowspan="1">Plt</th>
                      <th rowspan="1" colspan="2">Widal</th>
                      <th rowspan="2">LED</th>
                      <th rowspan="2">DIFF</th>
                      <th rowspan="2">HbsAg</th>
                      <th rowspan="2">Gula</th>
                      <th rowspan="2">Chol</th>
                      <th rowspan="2">Trig</th>
                      <th rowspan="2">Ua</th>
                      <th rowspan="2">Goldar</th>
                      <th rowspan="1" colspan="2">Ibu Hamil</th>
                      <th rowspan="1" colspan="2">Urinalisa</th>
                      <th rowspan="1" colspan="3">HIV</th>
                      <th rowspan="2">RPR</th>
                      <th rowspan="2">IMS</th>
                      <th rowspan="1" colspan="2">Mikrobiologi</th>
                      <th rowspan="2">Lain-lain</th>
                      <th rowspan="2">Harga</th>
                      <th rowspan="2">Waktu</th>
                    </tr>
                    <tr>
                      <th rowspan="1" >gr/dl</th>
                      <th rowspan="1" >%</th>
                      <th rowspan="1" >ribu/mm <sup>3</sup></th>
                      <th rowspan="1" >ribu/mm <sup>3</sup></th>
                      <th rowspan="1" >O</th>
                      <th rowspan="1" >H</th>
                      <th rowspan="1" >Protein</th>
                      <th rowspan="1" >Reduksi</th>
                      <th rowspan="1" >Protein</th>
                      <th rowspan="1" >Sedimen</th>
                      <th rowspan="1" >R1</th>
                      <th rowspan="1" >R2</th>
                      <th rowspan="1" >R3</th>
                      <th rowspan="1" >Pengecatan</th>
                      <th rowspan="1" >Hasil</th>
                    </tr>
                  </thead>
                  <tbody>
                  @foreach($pemeriksaan as $index => $pemeriksaan)
                  <tr>
                  <td>{{$index+1}}</td>
                  <td>{{$pemeriksaan->tanggal}}</td>
                  <td>{{$pemeriksaan->pasien->nama}}</td>
                  <td>{{date_diff(date_create($pemeriksaan->pasien->tanggal_lahir), date_create($pemeriksaan->tanggal))->format('%y Tahun %m Bulan %d Hari')}}</td>
                  <td>{{$pemeriksaan->pasien->jk == 1 ? "L" : "P"}}{{$pemeriksaan->is_hamil == 1 ? "-Hamil" : ""}}</td>
                  <td>{{strtoupper($pemeriksaan->debitur)}}</td>
                  <td>{{$pemeriksaan->pasien->alamat}}</td>
                  <td>{{$pemeriksaan->pasien->no_cm}}</td>
                  <td>{{$pemeriksaan->hb}}</td>
                  <td>{{$pemeriksaan->hmt}}</td>
                  <td>{{$pemeriksaan->leukosit}}</td>
                  <td>{{$pemeriksaan->plt}}</td>
                  <td>{{$pemeriksaan->widal_o}}</td>
                  <td>{{$pemeriksaan->widal_h}}</td>
                  <td>{{$pemeriksaan->led}}</td>
                  <td>{{$pemeriksaan->diff}}</td>
                  <td>{{isset($pemeriksaan->hbsag) ? $pemeriksaan->hbsag == 1 ? "Positif" : "Negatif" : ""}}</td>
                  <td>{{$pemeriksaan->gula}}</td>
                  <td>{{$pemeriksaan->chol}}</td>
                  <td>{{$pemeriksaan->trig}}</td>
                  <td>{{isset($pemeriksaan->ua) ? $pemeriksaan->ua == 1 ? "Positif" : "Negatif" : ""}}</td>
                  <td>{{$pemeriksaan->pasien->goldar}}</td>
                  <td>{{isset($pemeriksaan->hamil_protein) ?  $pemeriksaan->hamil_protein == 1 ? "Positif" : "Negatif" : ""}}</td>
                  <td>{{isset($pemeriksaan->hamil_reduksi) ? $pemeriksaan->hamil_reduksi == 1 ? "Positif 1" : $pemeriksaan->hamil_reduksi == 2 ? "Positif 2" : "Negatif" : ""}}</td>
                  <td>{{isset($pemeriksaan->urinalisa_protein) ?  $pemeriksaan->urinalisa_protein == 1 ? "Lengkap" : "Rutin" : ""}}</td>
                  <td>{{$pemeriksaan->urinalisa_sedimen}}</td>
                  <td>{{$pemeriksaan->hiv_1}}</td>
                  <td>{{$pemeriksaan->hiv_2}}</td>
                  <td>{{$pemeriksaan->hiv_3}}</td>
                  <td>{{$pemeriksaan->rpr}}</td>
                  <td>{{isset($pemeriksaan->ims) ? $pemeriksaan->ims == 1 ? "Positif" : "Negatif" : ""}}</td>
                  <td>{{$pemeriksaan->mikro_pengecatan}}</td>
                  <?php
                  if (isset(json_decode($pemeriksaan->mikro_hasil)->pmn)) {
                      if (isset(json_decode($pemeriksaan->mikro_hasil)->pmn) || isset(json_decode($pemeriksaan->mikro_hasil)->diplo) || isset(json_decode($pemeriksaan->mikro_hasil)->candi) || isset(json_decode($pemeriksaan->mikro_hasil)->tric)) {
                          $pmn = json_decode($pemeriksaan->mikro_hasil)->pmn == '1' ? 'PMN +,' : 'PMN -,';
                      } else {
                          $pmn = '';
                      }
                      if (isset(json_decode($pemeriksaan->mikro_hasil)->diplo)) {
                          $diplo = json_decode($pemeriksaan->mikro_hasil)->diplo == '1' ? 'Diplo +,' : 'Diplo -,';
                      } else {
                          $diplo = '';
                      }
                      if (isset(json_decode($pemeriksaan->mikro_hasil)->candi)) {
                          $candida = json_decode($pemeriksaan->mikro_hasil)->candi == '1' ? 'Candida +,' : 'Candida -,';
                      } else {
                          $candida = '';
                      }
                      if (isset(json_decode($pemeriksaan->mikro_hasil)->tric)) {
                          $tric = json_decode($pemeriksaan->mikro_hasil)->tric == '1' ? 'Trich +' : 'Trich -';
                      } else {
                          $tric = '';
                      }

                      echo '<td>'.$pmn.$diplo.$candida.$tric.'</td>';
                  } else {
                      echo '<td></td>';
                  }
                  ?>
                  <td>{{$pemeriksaan->is_first == 1 ? "Baru" : "Lama"}}</td>
                  <td>{{$pemeriksaan->harga}}</td>
                  <td>{{$pemeriksaan->waktu}}</td>
                  </tr>
                  @endforeach
                  </tbody>
                </table>
