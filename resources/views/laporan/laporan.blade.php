@extends('layouts.theme')

@section('content')
<div class="card">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Laporan</h6>
    </div>
  <div class="card-body">
     <form method="post" action="{{ url()->current() }}">
     {{ csrf_field() }}
  <div class=" field_wrapper">
    <div class="col-lg-12">
        <div class="form-group">
          <label for="laporan">Laporan:</label>
          <select required class="form-control" name="laporan" id="laporan">
                        <option value="">Pilih Laporan</option>
                        <option value="1">Register</option>
                        <option value="2">Laporan SP3</option>
                        <option value="3">Laporan Reagen</option>
          </select>
        </div>
    </div>
  </div>
  <div class="col-md-12">

        <button type="submit" class="btn btn-primary">Submit</button>
      </form>
  </div>
  </div>
</div>
@endsection

@section('script')

<script>
$('#laporan').change(function(){
  $('#sub').parent('div').remove()
  var wrapper = $('.field_wrapper'); //Input field wrapper
  var fieldHTML = '<div class="col-lg-12"><div class="form-group" id="sub"><label for="from">Dari:</label><input required type="date" name="from" class="form-control" id="from"></div><div class="form-group"><label for="to">Sampai:</label><input required type="date" name="to" class="form-control" id="to"></div></div>'; //New input field html

  $(wrapper).append(fieldHTML); // Add field html
});
</script>@endsection