
                <table>
                <thead>
                    <tr>
                      <th rowspan="2">PELAYANAN LABORATORIUM</th>
                      <th rowspan="2">HASIL</th>
                      <th rowspan="1" colspan="5">BARU</th>
                      <th rowspan="1" colspan="5">LAMA</th>
                      <th rowspan="2">TOTAL</th>
                    </tr>
                    <tr>
                      <th rowspan="1" >L</th>
                      <th rowspan="1" colspan="2" >P</th>
                      <th rowspan="1" ></th>
                      <th rowspan="1" >JUMLAH</th>
                      <th rowspan="1" >L</th>
                      <th rowspan="1" colspan="2" >P</th>
                      <th rowspan="1" ></th>
                      <th rowspan="1" >JUMLAH</th>
                    </tr>
                  </thead>
                  <tbody>
                  @foreach($pemeriksaan as $pemeriksaan)
                  <tr>
                  <td>{{$pemeriksaan['layanan']}}</td>
                  <td>{{$pemeriksaan['hasil']}}</td>
                  <td>{{$pemeriksaan['baru_l']}}</td>
                  <td>{{$pemeriksaan['baru_p1']}}</td>
                  <td>{{$pemeriksaan['baru_p2']}}</td>
                  <td>{{$pemeriksaan['baru']}}</td>
                  <td>{{$pemeriksaan['baru_jumlah']}}</td>
                  <td>{{$pemeriksaan['lama_l']}}</td>
                  <td>{{$pemeriksaan['lama_p1']}}</td>
                  <td>{{$pemeriksaan['lama_p2']}}</td>
                  <td>{{$pemeriksaan['lama']}}</td>
                  <td>{{$pemeriksaan['lama_jumlah']}}</td>
                  <td>{{$pemeriksaan['total']}}</td>
                  </tr>
                   @endforeach
                  </tbody>
                </table>
