
                <table>
                <thead>
                    <tr>
                      <th rowspan="2">PELAYANAN LABORATORIUM</th>
                      <th rowspan="2">TEST</th>
                      <th rowspan="2" colspan="2">HASIL</th>
                      <th rowspan="1" colspan="5">BARU</th>
                      <th rowspan="1" colspan="5">LAMA</th>
                      <th rowspan="2">TOTAL</th>
                    </tr>
                    <tr>
                      <th rowspan="1" >L</th>
                      <th rowspan="1" colspan="2" >P</th>
                      <th rowspan="1" ></th>
                      <th rowspan="1" >JUMLAH</th>
                      <th rowspan="1" >L</th>
                      <th rowspan="1" colspan="2" >P</th>
                      <th rowspan="1" ></th>
                      <th rowspan="1" >JUMLAH</th>
                    </tr>
                  </thead>
                  <tbody>
                  @foreach($pemeriksaan as $pemeriksaan)
                  <tr>
                  <td ></td>
                  <td>{{$pemeriksaan['test']}}</td>
                  <td>{{$pemeriksaan['hasil_1']}}</td>
                  <td>{{$pemeriksaan['hasil_2']}}</td>
                  <td>{{$pemeriksaan['baru_l']}}</td>
                  <td>{{$pemeriksaan['baru_p']}}</td>
                  <td>{{$pemeriksaan['baru_h']}}</td>
                  <td>{{$pemeriksaan['baru']}}</td>
                  <td>{{$pemeriksaan['baru_jumlah']}}</td>
                  <td>{{$pemeriksaan['lama_l']}}</td>
                  <td>{{$pemeriksaan['lama_p']}}</td>
                  <td>{{$pemeriksaan['lama_h']}}</td>
                  <td>{{$pemeriksaan['lama']}}</td>
                  <td>{{$pemeriksaan['lama_jumlah']}}</td>
                  <td>{{$pemeriksaan['total']}}</td>
                  </tr>
                   @endforeach
                  </tbody>
                </table>
