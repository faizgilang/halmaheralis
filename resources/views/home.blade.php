@extends('layouts.theme')

@section('content')
<div class="container">
    <div class="card justify-content-center">  
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Informasi Pemeriksaan</h6>
    </div>
    <div class="card-body">
        <div class="col-lg-12">
            <div class="card bg-primary text-white">        
                <div class="card-body">
                    <h6 class="card-title">Jumlah Pemeriksaan Hari ini</h6>
                </div>
                <div class="card-footer text-muted"><h5 class="card-title">{{$day}} Pemeriksaan</h5>
                </div>
            </div>
        </div>
        <br>
        <div class="col-lg-12">
            <div class="card bg-info text-white">        
                <div class="card-body">
                    <h6 class="card-title">Jumlah Pemeriksaan Bulan ini</h6>
                </div>
                <div class="card-footer text-muted"><h5 class="card-title">{{$month}} Pemeriksaan</h5>
                </div>
            </div>
        </div>
        <br>
        <div class="col-lg-12">
            <div class="card bg-success text-white">        
                <div class="card-body">
                    <h6 class="card-title">Jumlah Pemeriksaan Tahun ini</h6>
                </div>
                <div class="card-footer text-muted"><h5 class="card-title">{{$year}} Pemeriksaan</h5>
                </div>
            </div>
        </div>
        <br>
        
        
    </div>
    </div>
</div>
 
@endsection

@section('js')
@endsection